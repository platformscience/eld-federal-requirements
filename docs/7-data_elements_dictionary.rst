7. DATA ELEMENTS DICTIONARY
============================

.. _7.1:

7.1 24-Hour Period Starting Time
--------------------------------

:Description: This data element refers to the 24-hour period starting time specified by the motor carrier for driver’s home terminal.

:Purpose: Identifies the bookends of the work day for the driver; makes ELD records consistent with § 395.8 requirements, which require this information to be included on the form.

:Source: Motor carrier.

:Used in: ELD account profile; ELD outputs.

:Data Type: Programmed or populated on the ELD during account
    creation and maintained by the motor carrier to reflect true and
    accurate information for drivers.

:Data Range: 000000 to 235959; first two digits 00 to 23; middle
    two digits and last two digits 00 to 59.

:Data Length: 6 characters.

:Data Format: <HHMMSS> Military time format, where “HH” refers to
    hours,

    “MM” refers to minutes, and “SS” refers to seconds; designation for
    start time expressed in time standard in effect at the driver’s home
    terminal.

:Disposition: Mandatory.

:Examples: [060000], [073000], [180000].

.. _7.2:

7.2 Carrier Name
----------------

:Description: This data element refers to the motor carrier’s
    legal name for conducting commercial business.

:Purpose: Provides a recognizable identifier about the motor
    carrier on viewable ELD outputs; provides ability to cross check
    against USDOT number.

:Source: FMCSA’s Safety and Fitness Electronic Records (SAFER) System.

:Used in: ELD account profile.

:Data Type: Programmed on the ELD or entered once during the ELD
	account creation process.

:Data Range: Any alphanumeric combination.

:Data Length: Minimum: 4; Maximum: 120 characters.

:Data Format: <Carrier Name> as in <CCCC> to <CCCC CCCC>.

:Disposition: Mandatory.

:Example: [CONSOLIDATED TRUCKLOAD INC.].

.. _7.3:

7.3 Carrier’s USDOT Number
--------------------------

:Description: This data element refers to the motor carrier’s USDOT
	number.

:Purpose: Uniquely identifies the motor carrier employing the driver
	using the ELD.

:Source: FMCSA’s Safety and Fitness Electronic Records (SAFER) System.

:Used in: ELD account profiles; ELD event records; ELD output file.

:Data Type: Programmed on the ELD or entered once during the ELD
	account creation process.

:Data Range: An integer number of length 1-8 assigned to the motor
	carrier by FMCSA (9 position numbers reserved).

:Data Length: Minimum: 1; Maximum: 9 characters.

:Data Format: <Carrier’s USDOT Number> as in <C> to <CCCCCCCCC>.

:Disposition: Mandatory.

:Examples: [1], [1000003].


.. _7.4:

7.4 CMV Power Unit Number
-------------------------

:Description: This data element refers to the identifier the motor
    carrier uses for their CMVs in their normal course of business.

:Purpose: Identifies the vehicle a driver operates while a
    driver’s ELD records are recorded; Makes ELD records consistent with
    § 395.8 requirements, which require the truck or tractor number to
    be included on the form.

:Source: Unique CMV identifiers a motor carrier uses in its normal
    course of business and includes on dispatch documents, or the
    license number and the licensing State of the power unit.

:Used in: ELD event records; ELD output file.

:Data Type: Programmed on the ELD or populated by motor carrier’s
    extended ELD system or entered by the driver.

:Data Range: Any alphanumeric combination.

:Data Length: Minimum: 1; Maximum: 10 characters.

:Data Format: <CMV Power Unit Number> as in <C> to <CCCCCCCCCC>.

:Disposition: Mandatory for all CMVs operated while using an ELD.

:Examples: [123], [00123], [BLUEKW123], [TX12345].


.. _7.5:

7.5 CMV VIN
-----------

:Description: This data element refers to the
    manufacturer-assigned vehicle identification number (VIN) for the
    CMV powered unit.

:Purpose: Uniquely identifies the operated CMV not only within a
    motor carrier at a given time but across all CMVs sold within a
    30-year rolling period.

:Source: A robust unique CMV identifier standardized in North
    America.

:Used in: ELD event records; ELD output file.

:Data Type: Retrieved from the engine ECM via the vehicle databus.

:Data Range: Either blank or 17 characters long as specified by
    NHTSA in 49 CFR part 565, or 18 characters long with first character
    assigned as “-” (dash) followed by the 17 character long VIN. Check
    digit, i.e., VIN character position 9, as specified in 49 CFR part
    565 must imply a valid VIN.

:Data Length: Blank or 17-18 characters.

    **Data Format* : <CMV VIN> or <“-”><CMV VIN> or <{*blank**}> as in
    <CCCCCCCCCCCCCCCCC>, or <-CCCCCCCCCCCCCCCCC> or <>.

:Disposition: Mandatory for all ELDs linked to the engine ECM and
    when VIN is available from the engine ECM over the vehicle databus;
    otherwise optional. If optionally populated and source is not the
    engine ECM, precede VIN with the character “-” in records.

:Examples: [1FUJGHDV0CLBP8834], [-1FUJGHDV0CLBP8896], [].


.. _7.6:

7.6 Comment/Annotation
----------------------

:Description: This is a textual note related to a record, update,
    or edit capturing the comment or annotation a driver or authorized
    support personnel may input to the ELD.

:Purpose: Provides ability for a driver to offer explanations to
    records, selections, edits, or entries.

:Source: Driver or authorized support personnel.

:Used in: ELD events; ELD outputs.

:Data Type: Entered by the authenticated user via ELD’s interface.

:Data Range: Free form text of any alphanumeric combination.

:Data Length: 0-60 characters if optionally entered; 4-60
    characters if annotation is required and driver is prompted by the
    ELD.

:Data Format: <Comment/Annotation> as in <{blank}> or <C> to <CCC
    CCC >. **Disposition** : Optional in general; Mandatory if prompted by
    ELD.

:Examples: [], [Personal Conveyance. Driving to Restaurant in
    bobtail mode],

    [Forgot to switch to SB. Correcting here].


.. _7.7:

7.7 Data Diagnostic Event Indicator Status
------------------------------------------

:Description: This is a Boolean indicator identifying whether the
    used ELD unit has an active data diagnostic event set for the
    authenticated driver at the time of event recording.

:Purpose: Documents the snapshot of ELD’s data diagnostic status
    for the authenticated driver at the time of an event recording.

:Source: ELD internal monitoring functions.

:Used in: ELD events; ELD outputs.

:Data Type: Internally monitored and managed.

:Data Range: 0 (no active data diagnostic events for the driver)
    or 1 (at least one active data diagnostic event set for the driver).

:Data Length: 1 character.

:Data Format: <Data Diagnostic Event Indicator Status> as in <C >.

:Disposition: Mandatory.

:Examples: [0] or [1].


.. _7.8:

7.8 Date
--------

:Description: In combination with the variable “Time”, this
    parameter stamps records with a reference in time; even though date
    and time must be captured in UTC, event records must use date and
    time converted to the time zone in effect at the driver’s home
    terminal as specified in section :ref:`4.4.3`.

:Purpose: Provides ability to record the instance of recorded
    events.

:Source: ELD’s converted time measurement.

:Used in: ELD events; ELD outputs.

:Data Type: UTC date must be automatically captured by ELD; date
    in effect at the driver’s home terminal must be calculated as
    specified in section :ref:`4.4.3`.

:Data Range: Any valid date combination expressed in <MMDDYY>
    format where “MM” refers to months, “DD” refers to days of the month
    and “YY” refers to the last two digits of the calendar year.

:Data Length: 6 characters.

:Data Format: <MMDDYY> where <MM> must be between 01 and 12, <DD>
    must be between 01 and 31, and <YY> must be between 00 and 99.

:Disposition: Mandatory.

:Examples: [122815], [010114], [061228].


.. _7.9:

7.9 Distance Since Last Valid Coordinates
-----------------------------------------

:Description: Distance in whole miles traveled since the last
    valid latitude, longitude pair the ELD measured with the required
    accuracy.

:Purpose: Provides ability to keep track of location for recorded
    events in cases of temporary position measurement outage.

    Source : ELD internal calculations.

:Used in: ELD events; ELD outputs.

:Data Type: Kept track of by the ELD based on position measurement
    validity.

:Data Range: An integer value between 0 and 6; If the distance
    traveled since the last valid coordinate measurement exceeds 6
    miles, the ELD must enter the value as 6.

:Data Length: 1 character.

:Data Format: <Distance Since Last Valid Coordinates> as in <C>.

:Disposition: Mandatory.

    **Examples :** [0], [1], [5], [6].


.. _7.10:

7.10 Driver’s License Issuing State
-----------------------------------

:Description: This data element refers to the issuing State,
    Province or jurisdiction of the listed Driver’s License for the ELD
    account holder.

:Purpose: In combination with “Driver’s License Number”, it links
    the ELD driver account holder uniquely to an individual with driving
    credentials; ensures that only one driver account can be created per
    individual.

:Source: Driver’s license.

:Used in: ELD account profile(s); ELD output file.

:Data Type: Entered (during the creation of a new ELD account).

:Data Range: To character abbreviation listed on Table 5 of this
    appendix.

:Data Length: 2 characters.

:Data Format: <Driver’s License Issuing State> as in <CC>.

:Disposition: Mandatory for all driver accounts created on the
    ELD; optional for “non- driver” accounts.

:Example: [WA].

    

Table 5 - State and Province Abbreviation Codes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

+---------------+------------------+---------------+------------------+
| U.S.A         | STATE            | STATE CODE    | STATE            |
+---------------+------------------+---------------+------------------+
| STATE CODE    |                  |               |                  |
+---------------+------------------+---------------+------------------+
| AL            | ALABAMA          | MT            | MONTANA          |
+---------------+------------------+---------------+------------------+
| AK            | ALASKA           | NC            | NORTH CAROLINA   |
+---------------+------------------+---------------+------------------+
| AR            | ARKANSAS         | ND            | NORTH DAKOTA     |
+---------------+------------------+---------------+------------------+
| AZ            | ARIZONA          | NE            | NEBRASKA         |
+---------------+------------------+---------------+------------------+
| CA            | CALIFORNIA       | NH            | NEW HAMPSHIRE    |
+---------------+------------------+---------------+------------------+
| CO            | COLORADO         | NJ            | NEW JERSEY       |
+---------------+------------------+---------------+------------------+
| CT            | CONNECTICUT      | NM            | NEW MEXICO       |
+---------------+------------------+---------------+------------------+
| DC            | DIST of COL      | NV            | NEVADA           |
+---------------+------------------+---------------+------------------+
| DE            | DELAWARE         | NY            | NEW YORK         |
+---------------+------------------+---------------+------------------+
| FL            | FLORIDA          | OH            | OHIO             |
+---------------+------------------+---------------+------------------+
| GA            | GEORGIA          | OK            | OKLAHOMA         |
+---------------+------------------+---------------+------------------+
| HI            | HAWAII           | OR            | OREGON           |
+---------------+------------------+---------------+------------------+
| IA            | IOWA             | PA            | PENNSYLVANIA     |
+---------------+------------------+---------------+------------------+
| ID            | IDAHO            | RI            | RHODE ISLAND     |
+---------------+------------------+---------------+------------------+
| IL            | ILLINOIS         | SC            | SOUTH CAROLINA   |
+---------------+------------------+---------------+------------------+
| IN            | INDIANA          | SD            | SOUTH DAKOTA     |
+---------------+------------------+---------------+------------------+
| KS            | KANSAS           | TN            | TENNESSEE        |
+---------------+------------------+---------------+------------------+
| KY            | KENTUCKY         | TX            | TEXAS            |
+---------------+------------------+---------------+------------------+
| LA            | LOUISIANA        | UT            | UTAH             |
+---------------+------------------+---------------+------------------+
| MA            | MASSACHUSETTS    | VA            | VIRGINIA         |
+---------------+------------------+---------------+------------------+
| MD            | MARYLAND         | VT            | VERMONT          |
+---------------+------------------+---------------+------------------+
| ME            | MAINE            | WA            | WASHINGTON       |
+---------------+------------------+---------------+------------------+
| MI            | MICHIGAN         | WI            | WISCONSIN        |
+---------------+------------------+---------------+------------------+
| MN            | MINNESOTA        | WV            | WEST VIRGINIA    |
+---------------+------------------+---------------+------------------+
| MO            | MISSOURI         | WY            | WYOMING          |
+---------------+------------------+---------------+------------------+
| MS            | MISSISSIPPI      |               |                  |
+---------------+------------------+---------------+------------------+

AMERICAN POSSESSIONS OR PROTECTORATES
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

+--------------+---------------------+
| STATE CODE   | STATE               |
+--------------+---------------------+
| AS           | AMERICAN SAMOA      |
+--------------+---------------------+
| GU           | GUAM                |
+--------------+---------------------+
| MP           | NORTHERN MARIANAS   |
+--------------+---------------------+
| PR           | PUERTO RICO         |
+--------------+---------------------+
| VI           | VIRGIN ISLANDS      |
+--------------+---------------------+

CANADA
^^^^^^

+-----------------+-------------------------+
| PROVINCE CODE   | PROVINCE                |
+-----------------+-------------------------+
| AB              | ALBERTA                 |
+-----------------+-------------------------+
| BC              | BRITISH COLUMBIA        |
+-----------------+-------------------------+
| MB              | MANITOBA                |
+-----------------+-------------------------+
| NB              | NEW BRUNSWICK           |
+-----------------+-------------------------+
| NF              | NEWFOUNDLAND            |
+-----------------+-------------------------+
| NS              | NOVA SCOTIA             |
+-----------------+-------------------------+
| NT              | NORTHWEST TERRITORIES   |
+-----------------+-------------------------+
| ON              | ONTARIO                 |
+-----------------+-------------------------+
| PE              | PRINCE EDWARD ISLAND    |
+-----------------+-------------------------+
| QC              | QUEBEC                  |
+-----------------+-------------------------+
| SK              | SASKATCHEWAN            |
+-----------------+-------------------------+
| YT              | YUKON TERRITORY         |
+-----------------+-------------------------+

MEXICO
^^^^^^

+--------------+-------------------------+--------------+-------------------+
| STATE CODE   | STATE                   | STATE CODE   | STATE             |
+--------------+-------------------------+--------------+-------------------+
| AG           | AGUASCALIENTES          | MX           | MEXICO            |
+--------------+-------------------------+--------------+-------------------+
| BN           | BAJA CALIFORNIA NORTE   | NA           | NAYARIT           |
+--------------+-------------------------+--------------+-------------------+
| BS           | BAJA CALIFORNIA SUR     | NL           | NUEVO LEON        |
+--------------+-------------------------+--------------+-------------------+
| CH           | COAHUILA                | OA           | OAXACA            |
+--------------+-------------------------+--------------+-------------------+
| CI           | CHIHUAHUA               | PU           | PUEBLA            |
+--------------+-------------------------+--------------+-------------------+
| CL           | COLIMA                  | QE           | QUERETARO         |
+--------------+-------------------------+--------------+-------------------+
| CP           | CAMPECHE                | QI           | QUINTANA ROO      |
+--------------+-------------------------+--------------+-------------------+
| CS           | CHIAPAS                 | SI           | SINALOA           |
+--------------+-------------------------+--------------+-------------------+
| DF           | DISTRICTO FEDERAL       | SL           | SAN LUIS POTOSI   |
+--------------+-------------------------+--------------+-------------------+
| DG           | DURANGO                 | SO           | SONORA            |
+--------------+-------------------------+--------------+-------------------+
| GE           | GUERRERO                | TA           | TAMAULIPAS        |
+--------------+-------------------------+--------------+-------------------+
| GJ           | GUANAJUATO              | TB           | TABASCO           |
+--------------+-------------------------+--------------+-------------------+
| HD           | HIDALGO                 | TL           | TLAXCALA          |
+--------------+-------------------------+--------------+-------------------+
| JA           | JALISCO                 | VC           | VERACRUZ          |
+--------------+-------------------------+--------------+-------------------+
| MC           | MICHOACAN               | YU           | YUCATAN           |
+--------------+-------------------------+--------------+-------------------+
| MR           | MORELOS                 | ZA           | ZACATECAS         |
+--------------+-------------------------+--------------+-------------------+

OTHER
^^^^^

+-----------------+--------------------------------+
| PROVINCE CODE   | PROVINCE, STATE or COUNTRY     |
+-----------------+--------------------------------+
| OT              | ALL OTHERS NOT COVERED ABOVE   |
+-----------------+--------------------------------+


.. _7.11:

7.11 Driver’s License Number
----------------------------

**Description** : This data element refers to the unique Driver’s License
information required for each driver account on the ELD.

    **Purpose :** In combination with driver’s license issuing State, it
    links the ELD driver account holder to an individual with driving
    credentials; ensures that only one driver account can be created per
    individual.

:Source: Driver’s license.

:Used in: ELD account profile(s); ELD output file.

:Data Type: Entered (during the creation of a new ELD account).

:Data Range: Any alphanumeric combination.

:Data Length: Minimum: 1; Maximum: 20 characters.

:Data Format: <Driver’s License Number > as in <C> to
    <CCCCCCCCCCCCCCCCCCCC>. For ELD record keeping purposes, ELD must
    only retain characters in a Driver’s License Number entered during
    an account creation process that are a number between 0-9 or a
    character between A­Z (non-case sensitive).

:Disposition: Mandatory for all driver accounts created on the
    ELD; optional for “non­driver” accounts.

:Examples: [SAMPLMJ065LD], [D000368210361], [198],
    [N02632676353666].465


.. _7.12:

7.12 Driver’s Location Description
----------------------------------

:Description: This is a textual note related to the location of
    the CMV input by the driver upon ELD’s prompt.

:Purpose: Provides ability for a driver to enter location
    information related to entry of missing records; provides ability to
    accommodate temporary positioning service interruptions or outage
    without setting positioning malfunctions.

:Source: Driver, only when prompted by the ELD.

:Used in: ELD events; ELD outputs.

:Data Type: Entered by the authenticated driver when ELD solicits
    this information as specified in section :ref:`4.3.2.7`.

:Data Range: Free form text of any alphanumeric combination.

:Data Length: 5-60 characters.

:Data Format: <CCCCC> to <CCC CCC >.

:Disposition: Mandatory when prompted by ELD.

:Examples: [], [5 miles SW of Indianapolis, IN], [Reston, VA].


.. _7.13:

7.13 ELD Account Type
---------------------

:Description: An indicator designating whether an ELD account is a
    driver account or support personnel (non-driver) account.

:Purpose: Enables authorized safety officials to verify account
    type specific requirements set forth in this document.

:Source: ELD designated.

:Used in: ELD outputs.

:Data Type: Specified during the account creation process and
    recorded on ELD.

:Data Range: Character “D”, indicating account type “Driver”, or
    “S”, indicating account type “motor carrier’s support personnel”
    (i.e. non-driver); “Unidentified Driver” account must be designated
    with type “D”.

:Data Length: 1 character.

:Data Format: <C>.

:Disposition: Mandatory.

:Examples: [D], [S].


.. _7.14:

7.14 ELD Authentication Value
-----------------------------

:Description: An alphanumeric value that is unique to an ELD and
    verifies the authenticity of the given ELD.

:Purpose: Provides ability to cross-check the authenticity of an
    ELD used in the recording of a driver’s records during inspections.

:Source: ELD provider-assigned value; includes a certificate
    component and a hashed component; necessary information related to
    authentication keys and hash procedures disclosed by the registered
    ELD provider during the online ELD certification process for
    independent verification by FMCSA systems.

:Used in: ELD outputs.

:Data Type: Calculated from the authentication key and calculation
    procedure privately distributed by the ELD provider to FMCSA during
    the ELD registration process.

:Data Range: Alphanumeric combination.

:Data Length: 16-32 characters.

:Data Format: <CCCC CCCC>.

:Disposition: Mandatory.

:Example: [D3A4506EC8FF566B506EC8FF566BDFBB].


.. _7.15:

7.15 ELD Identifier
-------------------

:Description: An alphanumeric identifier assigned by the ELD
    provider to the ELD technology that is certified by the registered
    provider at FMCSA’s Web site.

:Purpose: Provides ability to cross-check that the ELD used in the
    recording of a driver’s records is certified through FMCSA’s
    registration and certification process as required.

:Source: Assigned and submitted by the ELD provider during the
    online certification of an ELD model and version.

:Used in: ELD outputs.

:Data Type: Coded on the ELD by the ELD provider and disclosed to
    FMCSA during the online certification process.

:Data Range: A six character alphanumeric identifier using
    characters A-Z and number 0-9.

:Data Length: 6 characters.

:Data Format: <ELD Identifier> as in <CCCCCC>.

:Disposition: Mandatory.

:Examples: [1001ZE], [GAM112], [02P3P1].


.. _7.16:

7.16 ELD Provider
-----------------

:Description: An alphanumeric company name of the technology
    provider as registered at the FMCSA’s Web site.

:Purpose: Provides ability to cross-check that the ELD used in the
    recording of a driver’s records is certified through FMCSA’s
    registration and certification process as required.

:Source: Assigned and submitted by the ELD provider during the
    online registration process.

:Used in: ELD outputs.

:Data Type: Coded on the ELD by the ELD provider and disclosed to
    FMCSA during the online registration process.

:Data Range: Any alphanumeric combination.

:Data Length: Minimum: 4; Maximum 120 characters.

:Data Format: <ELD Provider> as in <CCCC> to <CCCC CCCC>.

:Disposition: Mandatory.

:Examples: [ELD PROVIDER INC].


.. _7.17:

7.17 ELD Registration ID
------------------------

:Description: An alphanumeric registration identifier assigned to
    the ELD provider that is registered with FMCSA during the ELD
    registration process.

:Purpose: Provides ability to cross-check that the ELD provider
    has registered as required.

:Source: Received from FMCSA during online provider registration.

:Used in: ELD outputs.

:Data Type: Coded on the ELD by the provider.

:Data Range: A four character alphanumeric registration identifier
    using characters A- Z and numbers 0-9.

:Data Length: 4 characters.

:Data Format: <ELD Registration ID> as in <CCCC>.

:Disposition: Mandatory.

:Examples: [ZA10], [QA0C], [FAZ2].


.. _7.18:

7.18 ELD Username
-----------------

:Description: This data element refers to the unique user
    identifier assigned to the account holder on the ELD to authenticate
    the corresponding individual during an ELD login process; the
    individual may be a driver or a motor carrier’s support personnel.

:Purpose: Documents the user identifier assigned to the driver
    linked to the ELD account.

:Source: Assigned by the motor carrier during the creation of a
    new ELD account.

:Used in: ELD account profile; event records; ELD login process.

:Data Type: Entered (during account creation and user
    authentication).

:Data Range: Any alphanumeric combination.

:Data Length: Minimum: 4; Maximum: 60 characters.

:Data Format: <ELD Username> as in <CCCC> to <CCCC CCCC>.

:Disposition: Mandatory for all accounts created on the ELD.

:Examples: [smithj], [100384], [sj2345], [john.smith].


.. _7.19:

7.19 Engine Hours
-----------------

:Description: This data element refers to the time the CMV’s
    engine is powered in decimal hours with 0.1 hr (6-minute)
    resolution; this parameter is a placeholder for <{Total} Engine
    Hours>, which refers to the aggregated time of a vehicle’s engine’s
    operation since its inception, and used in recording “engine power
    on” and “engine shut down” events, and also for <{Elapsed} Engine
    Hours>, which refers to the elapsed time in the engine’s operation
    in the given ignition power on cycle, and used in the recording of
    all other events.

:Purpose: Provides ability to identify gaps in the operation of a
    CMV, when the vehicle’s engine may be powered but the ELD may not;
    provides ability to cross check integrity of recorded data elements
    in events and prevent gaps in the recording of ELD.

:Source: ELD measurement or sensing.

:Used in: ELD events; ELD outputs.

:Data Type: Acquired from the engine ECM or a comparable other
    source as allowed in section :ref:`4.3.1.4`.

:Data Range: For <{Total} Engine Hours>, range is between 0.0 and
    99,999.9; for <{Elapsed} Engine Hours>, range is between 0.0 and
    99,999.9.

:Data Length: 3-7 characters.

:Data Format: <Vehicle Miles> as in <C.C> to <CCCCC.C>.

:Disposition: Mandatory.

:Examples: [0.0], [9.9], [346.1], [2891.4].


.. _7.20:

7.20 Event Code
---------------

:Description: A dependent attribute on “Event Type” parameter that
    further specifies the nature of the change indicated in “Event
    Type”; t\`his parameter indicates the new status after the change.

:Purpose: Provides ability to code the specific nature of the
    change electronically.

:Source: ELD internal calculations.

:Used in: ELD event records; ELD outputs.

:Data Type: ELD recorded and maintained event attribute in
    accordance with the type of event and nature of the new status being
    recorded.

:Data Range: Dependent on the “Event Type” as indicated on Table 6 of
	this appendix.

:Data Length: 1 character.

:Data Format: <Event Type> as in <C>.

:Disposition: Mandatory.

:Examples: [0], [1], [4], [9].

.. Table_6:

Table 6 - “Event Type” Parameter Coding
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	.. figure:: media/image1.png
	   :alt: 
	   :width: 7.583in
	   :height: 6.167in


.. _7.21:

7.21 Event Data Check Value
---------------------------

:Description: A hexadecimal “check” value calculated in accordance
    with the procedure outlined in section :ref:`4.4.5.1` of this appendix and
    attached to each event record at the time of recording.

:Purpose: Provides ability to identify cases where an ELD event
    record may ‘have been inappropriately modified after its original
    recording.

:Source: ELD internal.

:Used in: ELD events; ELD output file.

:Data Type: Calculated by the ELD in accordance with section :ref:`4.4.5.1` of this appendix.

:Data Range: A number between hexadecimal 00 (decimal 0) and hexadecimal FF (decimal 255).

:Data Length: 2 characters.

:Data Format: <Event Data Check Value> as in <CC>.

:Disposition: Mandatory.

:Examples: [05], [CA], [F3].


.. _7.22:

7.22 Event Record Origin
------------------------

:Description: An attribute for the event record indicating whether
    it is automatically recorded, or edited, entered or accepted by the
    driver, requested by another authenticated user, or assumed from
    unidentified driver profile.

:Purpose: Provides ability to track origin of the records.

:Source: ELD internal calculations.

:Used in: ELD event records; ELD outputs.

:Data Type: ELD recorded and maintained event attribute in
    accordance with the procedures outlined in sections :ref:`4.4.4.2.2`,
    :ref:`4.4.4.2.3`, :ref:`4.4.4.2.4`, :ref:`4.4.4.2.5`, and :ref:`4.4.4.2.6` of this appendix.

:Data Range: 1, 2, 3 or 4 as described on Table 7 of this
    appendix.

:Data Length: 1 character.

:Data Format: <Event Record Origin> as in <C>.

:Disposition: Mandatory.

:Examples: [1], [2], [3], [4].

.. Table_7:

Table 7 - “Event Record Origin” Parameter Coding
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

+-----------------------------------------------------------------+----------------------------+
| Event Record Origin                                             | Event Record Origin Code   |
+-----------------------------------------------------------------+----------------------------+
| Automatically recorded by ELD                                   | 1                          |
+-----------------------------------------------------------------+----------------------------+
| Edited or entered by the Driver                                 | 2                          |
+-----------------------------------------------------------------+----------------------------+
| Edit requested by an Authenticated User other than the Driver   | 3                          |
+-----------------------------------------------------------------+----------------------------+
| Assumed from Unidentified Driver profile                        | 4                          |
+-----------------------------------------------------------------+----------------------------+


.. _7.23:

7.23 Event Record Status
------------------------

:Description: An attribute for the event record indicating whether
    an event is active or inactive and further, if inactive, whether it
    is due to a change or lack of confirmation by the driver or due to a
    driver’s rejection of change request.

:Purpose: Provides ability to keep track of edits and entries
    performed over ELD records while retaining original records.

:Source: ELD internal calculations.

:Used in: ELD event records; ELD outputs.

:Data Type: ELD recorded and maintained event attribute in
    accordance with the procedures outlined in sections :ref:`4.4.4.2.2`,
    :ref:`4.4.4.2.3`, :ref:`4.4.4.2.4`, :ref:`4.4.4.2.5`, and :ref:`4.4.4.2.6` of this appendix.

:Data Range: 1, 2, 3 or 4 as described on Table 8 of this
    appendix.

:Data Length: 1 character.

:Data Format: <Event Record Status> as in <C>.

:Disposition: Mandatory.

:Examples: [1], [2], [3], [4]

.. Table_8:

Table 8 - “Event Record Status” Parameter Coding
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

+-------------------------------+----------------------------+
| Event Record Status           | Event Record Status Code   |
+-------------------------------+----------------------------+
| Active                        | 1                          |
+-------------------------------+----------------------------+
| Inactive – Changed            | 2                          |
+-------------------------------+----------------------------+
| Inactive – Change Requested   | 3                          |
+-------------------------------+----------------------------+
| Inactive – Change Rejected    | 4                          |
+-------------------------------+----------------------------+


.. _7.24:

7.24 Event Sequence ID Number
-----------------------------

:Description: This data element refers to the serial identifier
    assigned to each required ELD event as described in section :ref:`4.5.1` of
    this appendix.

:Purpose: Provides ability to keep a continuous record, on a given
    ELD, across all users of that ELD.

:Source: ELD internal calculations.

:Used in: ELD event records; ELD outputs.

:Data Type: ELD maintained; incremented by 1 for each new record
    on the ELD; continuous for each new event the ELD records regardless
    of owner of the records.

:Data Range: 0 to FFFF; initial factory value must be 0; after
    FFFF hexadecimal (decimal 65535), the next Event Sequence ID number
    must be 0.

:Data Length: 1-4 characters.

:Data Format: <Event Sequence ID Number > as in <C> to <CCCC>.

:Disposition: Mandatory.

:Examples: [1], [1F2C], [2D3], [BB], [FFFE].


.. _7.25:

7.25 Event Type
---------------

:Description: An attribute specifying the type of the event
    record.

    **Purpose :** Provides ability to code the type of the recorded event
    in electronic format.

:Source: ELD internal calculations.

:Used in: ELD event records; ELD outputs.

:Data Type: ELD recorded and maintained event attribute in
    accordance with the type of event being recorded.

:Data Range: 1-7 as described on Table 9 of this appendix.

:Data Length: 1 character.

:Data Format: <Event Type> as in <C>.

:Disposition: Mandatory.

:Examples: [1], [5], [4], [7].

.. Table_9:

Table 9 - “Event Type” Parameter Coding
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

+-----------------------------------------------------------------------------------+-------------------+
| Event Type                                                                        | Event Type Code   |
+-----------------------------------------------------------------------------------+-------------------+
| A change in driver’s duty-status                                                  | 1                 |
+-----------------------------------------------------------------------------------+-------------------+
| An intermediate log                                                               | 2                 |
+-----------------------------------------------------------------------------------+-------------------+
| A change in driver’s indication of authorized personal use of CMV or yard moves   | 3                 |
+-----------------------------------------------------------------------------------+-------------------+
| A driver’s certification/re-certification of records                              | 4                 |
+-----------------------------------------------------------------------------------+-------------------+
| A driver’s login/logout activity                                                  | 5                 |
+-----------------------------------------------------------------------------------+-------------------+
| CMV’s engine power up / shut down activity                                        | 6                 |
+-----------------------------------------------------------------------------------+-------------------+
| A malfunction or data diagnostic detection occurrence                             | 7                 |
+-----------------------------------------------------------------------------------+-------------------+


.. _7.26:

7.26 Exempt Driver Configuration
--------------------------------

:Description: A parameter indicating whether the motor carrier
    configured a driver’s profile to claim exemption from ELD use.

    **Purpose :** Provides ability to code the motor carrier-indicated
    exemption for the driver electronically.

:Source: Motor carrier’s configuration for a given driver.

:Used in: ELD outputs.

:Data Type: Motor carrier configured and maintained parameter in
    accordance with the qualification requirements listed in § 395.1.

:Data Range: E (exempt) or 0 (number zero).

:Data Length: 1 character.

:Data Format: <Exempt Driver Configuration> as in <C>.

:Disposition: Mandatory.

:Examples: [E], [0].


.. _7.27:

7.27 File Data Check Value
--------------------------

:Description: A hexadecimal “check” value calculated in accordance
    with the procedure outlined in section :ref:`4.4.5.3` of this appendix and
    attached to each ELD output file.

    **Purpose :** Provides ability to identify cases where an ELD file may
    have been inappropriately modified after its original creation.

:Source: ELD internal.

:Used in: ELD output files.

:Data Type: Calculated by the ELD in accordance with section :ref:`4.4.5.3` of this appendix.

:Data Range: A number between hexadecimal 0000 (decimal 0) and hexadecimal FFFF (decimal 65535).

:Data Length: 4 characters.

:Data Format: <File Data Check Value> as in <CCCC>.

:Disposition: Mandatory.

:Examples: [F0B5], [00CA], [523E].


.. _7.28:

7.28 First Name
---------------

:Description: This data element refers to the given name of the
    individual holding an

    ELD account.

:Purpose: Links an individual to the associated ELD account.

:Source: Driver’s license for driver accounts; driver’s license or
    government-issued ID for support personnel accounts.

:Used in: ELD account profile(s); ELD outputs (display and file).

:Data Type: Entered (during the creation of a new ELD account).

:Data Range: Any alphanumeric combination.

:Data Length: Minimum: 2; Maximum: 30 characters.

:Data Format: <First Name> as in <CC> to <CC CC> where “C” denotes
    a character.

:Disposition: Mandatory for all accounts created on the ELD.

:Example: [John].


.. _7.29:

7.29 Geo-Location
-----------------

:Description: A descriptive indicator of the CMV position in terms
    of a distance and direction to a recognizable location derived from
    a GNIS database at a minimum containing all cities, towns and
    villages with a population of 5,000 or greater.

:Purpose: Provide recognizable location information on a display
    or printout to users of the ELD.

:Source: ELD internal calculations as specified in section :ref:`4.4.2`
    of this appendix.

:Used in: ELD display or printout.

:Data Type: Identified from the underlying latitude/longitude
    coordinates by the ELD. **Data Range** : Contains four segments in one
    text field; a recognizable location driven from GNIS database
    containing—at a minimum—all cities, towns and villages with a
    population of 5,000 in text format containing a location name and
    the State abbreviation, distance from this location and direction
    from this location.

:Data Length: Minimum: 5; Maximum: 60 characters.

    **Data Format* : <Distance from *{identified}** Geo-location> <’mi ‘>
    <Direction from **{identified}** Geo-location> <’ ‘> <State
    Abbreviation **{of identified}** Geo Location> <’ ‘> <Place name of
    **{identified}* Geo-location> where: <Distance from *{identified}**
    Geo-location> must either be <{blank}> or <C> or <CC> where the
    up-to two character number specifies absolute distance between
    identified geo-location and event location; <Direction from
    **{identified}** Geo-location> must either be <{blank}> or <C> or <CC>
    or <CCC> , must represent direction of event location with respect
    to the identified geo-location, and must take a value listed on
    Table 10 of this appendix; <State Abbreviation **{of identified}** Geo
    Location> must take values listed on Table 5; <Place name of
    **{identified}** Geo-location> must be the text description of the
    identified reference location; Overall length of the “Geo-location”
    parameter must not be longer than 60 characters long.

:Disposition: Mandatory.

:Examples: [2mi ESE IL Darien], [1mi SE TX Dallas], [11mi NNW IN
    West Lafayette].

.. Table_10:

Table 10 - Conventional Compass Rose Direction Coding To Be Used in the Geo-Location Parameter.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

+------------------------------------------------+------------------+
| Direction                                      | Direction Code   |
+------------------------------------------------+------------------+
| At indicated geo-location                      | {blank}          |
+------------------------------------------------+------------------+
| North of indicated geo-location                | N                |
+------------------------------------------------+------------------+
| North – North East of indicated geo-location   | NNE              |
+------------------------------------------------+------------------+
| North East of indicated geo-location           | NE               |
+------------------------------------------------+------------------+
| East – North East of indicated geo-location    | ENE              |
+------------------------------------------------+------------------+
| East of indicated geo-location                 | E                |
+------------------------------------------------+------------------+
| East – South East of indicated geo-location    | ESE              |
+------------------------------------------------+------------------+
| South East of indicated geo-location           | SE               |
+------------------------------------------------+------------------+
| South – South East of indicated geo-location   | SSE              |
+------------------------------------------------+------------------+
| South of indicated geo-location                | S                |
+------------------------------------------------+------------------+
| South – South West of indicated geo-location   | SSW              |
+------------------------------------------------+------------------+
| South West of indicated geo-location           | SW               |
+------------------------------------------------+------------------+
| West – South West of indicated geo-location    | WSW              |
+------------------------------------------------+------------------+
| West of indicated geo-location                 | W                |
+------------------------------------------------+------------------+
| West – North West of indicated geo-location    | WNW              |
+------------------------------------------------+------------------+
| North West of indicated geo-location           | NW               |
+------------------------------------------------+------------------+
| North– North West of indicated geo-location    | NNW              |
+------------------------------------------------+------------------+


.. _7.30:

7.30 Last Name
--------------

:Description: This data element refers to the last name of the
    individual holding an ELD account.

:Purpose: Links an individual to the associated ELD account.

:Source: Driver’s license for driver accounts; driver’s license or
    government-issued ID for support personnel accounts.

:Used in: ELD account profile(s); ELD outputs (display and file).

:Data Type: Entered (during the creation of a new ELD account).

:Data Range: Any alphanumeric combination.

:Data Length: Minimum: 2; Maximum: 30 characters

:Data Format: <Last Name> as in <CC> to <CC CC>.

:Disposition: Mandatory for all accounts created on the ELD.

:Example: [Smith].


.. _7.31:

7.31 Latitude
-------------

:Description: An angular distance in degrees north and south of
    the equator.

:Purpose: In combination with the variable “Longitude”, this
    parameter stamps records requiring a position attribute with a
    reference point on the face of the earth.

:Source: ELD’s position measurement.

:Used in: ELD events; ELD outputs.

:Data Type: Latitude and Longitude must be automatically captured
    by the ELD. **Data Range** : -90.00 to 90.00 in decimal degrees (two
    decimal point resolution) in records using conventional positioning
    precision; -90.0 to 90.0 in decimal degrees (single decimal point
    resolution) in records using reduced positioning precision when
    allowed; latitudes north of the equator must be specified by the
    absence of a minus sign (-) preceding the digits designating
    degrees; latitudes south of the Equator must be designated by a
    minus sign (-) preceding the digits designating degrees.

:Data Length: 3 to 6 characters.

:Data Format: First character: [<‘-’> or <{blank}>]; then [<C> or
    <CC>]; then <‘.’>; then [<C> or <CC>].

:Disposition: Mandatory.

:Examples: [-15.68], [38.89], [5.07], [-6.11], [-15.7], [38.9],
    [5.1], [-6.1].


.. _7.32:

7.32 Line Data Check Value
--------------------------

:Description: A hexadecimal “check” value calculated in accordance
    with procedure outlined in section :ref:`4.4.5.2` and attached to each line
    of output featuring data at the time of output file being generated.

:Purpose: Provides ability to identify cases where an ELD output
    file may have been inappropriately modified after its original
    generation.

:Source: ELD internal.

:Used in: ELD output file.

:Data Type: Calculated by the ELD in accordance with :ref:`4.4.5.2`.

:Data Range: A number between hexadecimal 00 (decimal 0) and
    hexadecimal FF (decimal 255) .

:Data Length: 2 characters.

:Data Format: <Line Data Check Value> as in <CC>.

:Disposition: Mandatory.

:Examples: [01], [A4], [CC].


.. _7.33:

7.33 Longitude
--------------

:Description: An angular distance in degrees measured on a circle
    of reference with respect to the zero (or prime) meridian; The prime
    meridian runs through Greenwich, England.

:Purpose: In combination with the variable “Latitude”, this
    parameter stamps records requiring a position attribute with a
    reference point on the face of the earth.

:Source: ELD’s position measurement.

:Used in: ELD events; ELD outputs.

:Data Type: Latitude and Longitude must be automatically captured
    by the ELD. **Data Range** : -179.99 to 180.00 in decimal degrees (two
    decimal point resolution) in records using conventional positioning
    precision; -179.9 to 180.0 in decimal degrees (single decimal point
    resolution) in records using reduced positioning precision when
    allowed; longitudes east of the prime meridian must be specified by
    the absence of a minus sign (-) preceding the digits designating
    degrees of longitude; longitudes west of the prime meridian must be
    designated by minus sign (-) preceding the digits designating
    degrees.

:Data Length: 3 to 7 characters.

:Data Format: First character: [<‘-’> or <{blank}>]; then [<C>,
    <CC> or <CCC>]; then <‘.’>; then [<C> or <CC>].

:Disposition: Mandatory.

:Examples: [-157.81], [-77.03], [9.05], [-0.15], [-157.8],
    [-77.0], [9.1], [-0.2].


.. _7.34:

7.34 Malfunction/Diagnostic Code
--------------------------------

:Description: A code that further specifies the underlying
    malfunction or data diagnostic event.

    **Purpose :** Enables coding the type of malfunction and data
    diagnostic event to cover the standardized set in Table 4 of this
    appendix.

:Source: ELD internal monitoring.

:Used in: ELD events; ELD outputs.

:Data Type: Recorded by ELD when malfunctions and data diagnostic
    events are set or reset.

:Data Range: As specified in Table 4 of this appendix.

:Data Length: 1 character.

:Data Format: <C>.

:Disposition \_: Mandatory.

:Examples: [1], [5], [P], [L].


.. _7.35:

7.35 Malfunction Indicator Status
---------------------------------

:Description: This is a Boolean indicator identifying whether the
    used ELD unit has an active malfunction set at the time of event
    recording.

:Purpose: Documents the snapshot of ELD’s malfunction status at
    the time of an event recording.

:Source: ELD internal monitoring functions.

:Used in: ELD events; ELD outputs.

:Data Type: Internally monitored and managed.

:Data Range: 0 (no active malfunction) or 1 (at least one active
    malfunction).

:Data Length: 1 character.

:Data Format: <Malfunction Indicator Status> as in <C>.

:Disposition: Mandatory.

:Examples: [0] or [1].


.. _7.36:

7.36 Multiday Basis Used
------------------------

:Description: This data element refers to the multiday basis (7 or
    8 days) used by the motor carrier to compute cumulative duty hours.

:Purpose: Provides ability to apply the HOS rules accordingly.

    Source : Motor carrier.

:Used in: ELD account profile; ELD outputs.

:Data Type: Entered by the motor carrier during account creation
    process.

:Data Range: 7 or 8.

:Data Length: 1 character.

:Data Format: <Multiday basis used> as in <C>.

:Disposition: Mandatory.

:Examples: [7], [8].


.. _7.37:

7.37 Order Number
-----------------

:Description: A continuous integer number assigned in the forming
    of a list, starting at 1 and incremented by 1 for each unique item
    on the list.

:Purpose: Allows for more compact report file output generation
    avoiding repetitious use of CMV identifiers and usernames affected
    in records.

:Source: ELD internal.

:Used in: ELD outputs, listing of users and CMVs referenced in ELD
    logs.

:Data Type: Managed by ELD.

:Data Range: Integer between 1 and 99.

:Data Length: 1-2 characters.

:Data Format: <Order Number> as in <C> or <CC>.

:Disposition: Mandatory.

:Examples: [1], [5], [11], [28].


.. _7.38:

7.38 Output File Comment
------------------------

:Description: A textual field that may be populated with
    information pertaining to the created ELD output file; An authorized
    safety official may provide a key phrase or code to be included in
    the output file comment, which may be used to link the requested
    data to an inspection, inquiry, or other enforcement action; if
    provided to the driver by an authorized safety official, it must be
    entered into the ELD and included in the exchanged dataset as
    specified.

:Purpose: The output file comment field provides an ability to
    link submitted data to an inspection, inquiry, or other enforcement
    action, if deemed necessary; further, it may also link a dataset to
    a vehicle, driver, carrier, and/or ELD that may participate in
    voluntary future programs that may involve exchange of ELD data.

:Source: Enforcement personnel or driver or motor carrier.

:Used in: ELD outputs.

:Data Type: If provided, output file comment is entered or
    appended to the ELD dataset prior to submission of ELD data to
    enforcement.

:Data Range: Blank or any alphanumeric combination specified and
    provided by an authorized safety official.

:Data Length: 0-60 characters.

    | **Data Format** : <{blank}>, or <C> thru <CCCC CCCC>.
    | **Disposition** : Mandatory.

:Examples: [], [3BHG701015], [113G1EFW02], [7353930].


.. _7.39:

7.39 Shipping Document Number
-----------------------------

:Description: Shipping document number the motor carrier uses in
    their system and dispatch documents.

:Purpose: Links ELD data to the shipping records; makes ELD
    dataset consistent with § 395.8 requirements.

:Source: Motor carrier.

:Used in: ELD outputs.

:Data Type: Entered in the ELD by the authenticated driver or
    motor carrier and verified by the driver.

:Data Range: Any alphanumeric combination.

:Data Length: 0-40 characters.

:Data Format: <{blank}>, or <C> thru <CCCC CCCC>.

:Disposition: Mandatory if a shipping number is used on motor
    carrier’s system.

:Examples: [], [B 75354], [FX334411707].


.. _7.40:

7.40 Time
---------

:Description: In combination with the variable “Date”, this
    parameter stamps records with a reference in time; even though date
    and time must be captured in UTC, event records must use date and
    time converted to the time zone in effect at the driver’s home
    terminal as specified in section :ref:`4.4.3` of this appendix.

:Purpose: Provides ability to record the instance of recorded
    events.

:Source: ELD’s converted time measurement.

:Used in: ELD events; ELD outputs.

:Data Type: UTC time must be automatically captured by ELD; time
    in effect at the driver’s home terminal must be calculated as
    specified in section :ref:`4.4.3` of this appendix.

:Data Range: Any valid date combination expressed in <HHMMSS>
    format where “HH” refers to hours of the day, “MM” refers to
    minutes, and “SS” refers to seconds.

:Data Length: 6 characters.

:Data Format: <HHMMSS> where <HH> must be between 00 and 23, <MM>
    and <SS> must be between 00 and 59.

:Disposition: Mandatory.

:Examples: [070111], [001259], [151522], [230945].


.. _7.41:

7.41 Time Zone Offset from UTC
------------------------------

:Description: This data element refers to the offset in time
    between UTC time and the time standard in effect at the driver’s
    home terminal.

:Purpose: Establishes the ability to link records stamped with
    local time to a universal reference.

:Source: Calculated from measured variable <{UTC} Time> and
    **<{Time Standard in Effect at driver’s home terminal}** Time>;
    Maintained together with “24-hour Period Starting Time” parameter by
    the motor carrier or tracked automatically by ELD.

:Used in: ELD account profile; ELD event: Driver’s certification
    of own records.

:Data Type: Programmed or populated on the ELD during account
    creation and maintained by the motor carrier or ELD to reflect true
    and accurate information for drivers. This parameter must adjust for
    Daylight Saving Time changes in effect at the driver’s home
    terminal.

:Data Range: 04 to 11; omit sign.

:Data Length: 2 characters.

:Data Format: <Time Zone Offset from UTC> as in <HH> where “HH”
    refer to hours in difference.

:Disposition: Mandatory.

:Examples: [04], [05], [10].


.. _7.42:

7.42 Trailer Number(s)
----------------------

:Description: This data element refers to the identifier(s) the
    motor carrier uses for the trailers in their normal course of
    business.

:Purpose: Identifies the trailer(s) a driver operates while a
    driver’s ELD records are recorded; makes ELD records consistent with
    § 395.8 which requires the trailer number(s) to be included on the
    form.

:Source: Unique trailer identifiers a motor carrier uses in their
    normal course of business and includes on dispatch documents, or the
    license number and licensing State of each towed unit; trailer
    number(s) must be updated each time hauled trailers change.

:Data Type: Automatically captured by the ELD or populated by
    motor carrier’s extended ELD system or entered by the driver; must
    be updated each time the hauled trailer(s) change.

:Data Range: Any alphanumeric combination.

:Data Length: Minimum: blank; Maximum: 32 characters (3 trailer
    numbers each maximum 10 characters long, separated by spaces).

:Data Format: Trailer numbers; separated by space in case of
    multiple trailers hauled at one time; field to be left blank” for
    non-combination vehicles (such as a straight truck or bobtail
    tractor).

    <Trailer Unit Number {#1}><’ ‘><Trailer Unit Number {#2}> <’
    ‘><Trailer Unit Number {#3}> as in <{blank}> to <CCCCCCCCCC
    CCCCCCCCCC CCCCCCCCCC>.

:Disposition: Mandatory when operating combination vehicles.

:Examples: [987], [00987 PP2345], [BX987 POP712 10567], [TX12345
    LA22A21], [].


.. _7.43:

7.43 Vehicle Miles
------------------

:Description: This data element refers to the distance traveled
    using the CMV in whole miles; this parameter is a placeholder for
    <{Total} Vehicle Miles>, which refers to the odometer reading and is
    used in recording “engine power on” and “engine shut down” events,
    and also for <{Accumulated} Vehicle Miles>, which refers to the
    accumulated miles in the given ignition power on cycle and is used
    in the recording of all other events.

:Purpose: Provides ability to track distance traveled while
    operating the CMV in each duty status. Total miles traveled within a
    24-hour period is a required field in § 395.8.

:Source: ELD measurement or sensing.

:Used in: ELD events; ELD outputs.

:Data Type: Acquired from the engine ECM or a comparable other
    source as allowed in section :ref:`4.3.1.3`.

:Data Range: For <{Total} Vehicle Miles>, range is between 0 and 9,999,999; for <{Accumulated} Vehicle Miles>, range is between 0 and 9,999.

:Data Length: 1-7 characters.

:Data Format: <Vehicle Miles> as in <C> to <CCCCCCC>.

:Disposition: Mandatory.

:Examples: [99], [1004566], [0], [422].
