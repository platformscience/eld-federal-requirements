Technical FAQ's
===============

This set of questions is from the `Eld Technical FAQs - CSA Compliance, Safety, Accountability`
https://csa.fmcsa.dot.gov/ELD/File/Index/b9d89f57-b7ae-da42-86fd-679779a6cbbd

This document was updated on 2018-02-22 with additions from the FMCSA Technical FAQ's September, 2017 revisions. 
https://csa.fmcsa.dot.gov/ELD/File/Index/400fd7dc-7169-724c-9de9-e7f52913a02d




ELD Systems
-----------

What is an ELD system? What is an ELD support system?
*****************************************************
For clarity, an ELD system will be referred to as an ELD. The ELD includes all components required to record, retain, and transfer data. The ELD must be capable of maintaining ELD records for at least six months from the date of receipt of the record, including edited and original records. Whereas the ELD Support System refers to the central support system through which carriers manage and store records separate from the device. The ELD support system is not a required system.

How can a driver edit their ELD data after leaving the vehicle?
***************************************************************
While not required, the motor carrier can include an ELD support system that allows drivers to make edits to ELD data when away from the ELD. Otherwise, the driver must return to the ELD and make edits.
Can an ELD system be mobile (e.g., an app on a smartphone or tablet)?
Yes, the rule does allow for the use of a mobile device (commonly called a portable or handheld unit). Apps are allowed to be ELDs as long as they have connectivity to the electronic control module (ECM) bus for the required data and meet all the technical specs.

Functional Specifications
-------------------------

User Accounts
-------------

Can a driver have more than one user account?
*********************************************
An ELD must not allow creation of more than one driver account associated with a driver’s license for a given motor carrier.

In the case of owner-operator, the same person may have two accounts: a driver account for logging hours of service and a supporting personnel account for managing the backend.

Are ELDs required to be interoperable?
**************************************
No, the ELD Rule does not require interoperability—one device does not need to have the capability to transfer data from one ELD to another. As a minimal requirement, the driver would need to have printed ELD records with them to reflect their previous seven days or manually add the hours of service records from the previous seven days.

Exempt Driving Status
---------------------

Is the ELD required to offer an exempt driver account?
******************************************************
There is no separate exempt driver account; rather, the ELD must allow for a driver account to be configured as exempt by the carrier. The ELD must prompt the carrier to add an annotation justifying the exemption. The default setting for a new driver account must be “no exemption.”

Can a driver select exempt driver status?
*****************************************
The ELD must allow the motor carrier the option to proactively configure a driver account as an exempt driver. The ELD must prompt the motor carrier to annotate the record and provide an explanation for the configuration of the exemption. If the motor carrier elects to create an exempt driver account, then the ELD must present the configured indication that is in effect for that driver during the ELD login and logout processes. The driver cannot change their status from exempt without the motor carrier changing the driver account.

How should the ELD function if the driver is operating under a situation that is exempt from the ELD rule?
**********************************************************************************************************
When the exempt driver status is in effect, the ELD must continue to record ELD driving time but suspend detection of missing required data elements data diagnostic event for the driver described in Section 4.6.1.5 of the appendix and data transfer compliance monitoring function described in Section 4.6.1.7 when such driver is authenticated on the ELD.


Unidentified Driver Account
---------------------------

Section 4.6.1.6(d) states that “An unidentified driving records data diagnostic event can be cleared by the ELD when driving time logged under the unidentified driver profile for the current 24‐hour period and the previous 7 consecutive days drops to 15 minutes or less.” Does this mean 15 minutes or less in each of the 7 previous days, or does it mean the sum of all unidentified driving time over the previous 7 days totals 15 minutes or less?
*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************
The aggregate – 15 minutes total. Note that only “unidentified driving records data diagnostic event” can be cleared (in other words, marked inactive). The underlying records that generated the event may not be deleted.




What data does the ELD need to log when there is an unidentified driver? (added Sept-2017)
******************************************************************************************
When a driver does not log into the ELD and does not respond to the ELD visual and audible prompts, the ELD must record accumulated driving and On-Duty Not-Driving time in accordance with the ELD defaults (see Section 4.4.1). When more than 30 minutes of driving in a 24-hour period accumulate in the unidentified driver profile, the ELD data diagnostic indicator must be turned on across all drivers logged into that ELD for the current day and the following 7 days. Other events that must be associated with the unidentified driver profile include the CMV engine power up and shut down and power compliance monitoring. The ELD must not allow entry of any information into the ELD other than a response to the login prompt.
The Event Log List for the Unidentified Driver Profile may be found in 4.8.2.1.10.

How can unidentified driving time be assigned? (added Sept-2017)
****************************************************************
Unidentified driving time can be assigned in two ways. The driver can claim the driving time when prompted by the ELD, or the motor carrier can later assign the unidentified driving time to the appropriate driver, which must be accepted by the driver.

If the unidentified driving time is accepted by the driver, will the ELD still show the driving time as unidentified? (added Sept-2017)
***************************************************************************************************************************************
The original records reflecting unidentified driving will remain, but with an inactive status. When unidentified records are assumed, a new event record(s) for the driver is created using elements of the unidentified driver log(s) and driver input to populate missing elements of the log originally recorded under the unidentified driver profile.

If the driver rejects unidentified driving, should that be displayed on the ELD for the roadside inspection view and output file view? (added Sept-2017)
********************************************************************************************************************************************************
The rejected unidentified driving events must remain available for review at roadside for eight days and should be included in the output file.

How should “odometer jumps,” caused by a driver unplugging the ELD from the ECM be captured? (added Sept-2017)
**************************************************************************************************************
If the driver unplugs an ELD from the ECM and later plugs the ELD back into the ECM, the ELD must identify any odometer jump in between as a malfunction (see Table 4 of Appendix A to Subpart B of Part 395). In addition, if the ECM is not disconnected, however connectivity is lost for more than 30 minutes in a 24-hour period, an engine synchronization malfunction must be recorded.

In the case of portable device platforms, if a driver forgets his or her phone or tablet, must unidentified driving time still be collected through an engine tied device or is it considered uninstalled? (added Sept-2017)
****************************************************************************************************************************************************************************************************************************
If more than one component is required to ensure the ELD is compliant with Subpart B and Appendix A of the ELD rule, all components must be present for the driver to be operating with an ELD when an ELD is required.
A manufacturer could choose, as a service to the carrier, to record time that was generated in the absence of one of the components from the black box as unidentified driving time.

Can ELD events remain unassigned after a trip is assigned to a driver? (added Sept-2017)
****************************************************************************************
Yes. Once the unassigned driving time has been reviewed and only certain records were attributable to and assumed by a driver, in accordance with 49 CFR 395.32, it is expected that some events will remain associated with the unidentified driving profile. These event types include intermediate logs, power- up/shut-down, and malfunction/diagnostic, which cannot be edited.



Powering On/Sensing
-------------------

Can an ELD provider pass the one-minute boot time rule by requiring the driver to power on the device (or an app) prior to starting the engine?
************************************************************************************************************************************************
Yes. If the ELD is an application on a separate device, the driver must understand that manual power must be on and the ELD application launched prior to starting the vehicle so that the device can recognize the engine start. Failure to start the application would result in the driver operating without an ELD, which would be in violation of the ELD rule.

Is it permissible to allow the ELD to power up, boot, and accept driver logins before the engine is powered?
************************************************************************************************************
Yes, this is an acceptable way for all ELDs to function. If the ELD is structured such that the device must be powered on and logged into before the engine is powered to meet the requirements of the rule, this should be made clear to the driver through the device manual and carriers should include this in their driver training.

Can a mobile app and the vehicle engine communicate over cellular?
******************************************************************
Yes, the rule does allow for this. However, manufacturers should keep in mind that in places without coverage, and without cellular communication, the device may not be able to record or display Records of Duty Status, which would leave the driver operating without logs—a violation of the hours of service rules.


What is a manufacturer required to do when required data is not available from the ECM?
***************************************************************************************
If the vehicle does not have an ECM or the ECM does not provide all the information required for, it must be acquired using an independent source apart from the positioning services described under Section 4.3.1.6 of the appendix and must meet accuracy levels laid out in the rule. Global Positioning Systems cannot be used to identify the vehicle’s motion status.

Please note that if there is a means for retrieving data from the ECM, the provider is obligated to use that means (for example, using a synthesized odometer or entering into an agreement with an OEM to access proprietary information). If FMCSA is made aware that a provider has chosen not to undertake the effort to secure data from the ECM that is, in fact, retrievable, the device would be considered non-compliant and removed from the self-certified and registered ELDs.

Is the ELD required to retrieve the CMV position from the device connected to the port, or can this be retrieved by using a smartphone or tablet?
***************************************************************************************************************************************************
Position information is not intended to be collected from the ECM. The ability to retrieve the CMV’s position should be provided by part of the ELD System in compliance with 4.6.1.4.

Can the partial VIN value obtained from the controller area network bus be used for reporting?
**********************************************************************************************
Partial VIN is not permitted. If the VIN cannot be fully obtained from the ECM, it must be entered manually; however, a partial VIN broadcast can be used to aid the driver in entering a manual VIN. Please note:

• Providers are encouraged to use the VIN check digit calculation to help prevent user entry errors.
• A VIN with any level of manual entry must be prefixed by a ”-“ character in the output file.
• Manual VIN entry cannot be used when the complete VIN can be obtained from the ECM. A device using manual entry when the complete VIN is in fact available from the ECM would not be in compliance with the rule and would be subject to removal from the list of self-certified and registered ELDs.

If the required ELD data from the ECM is private and requires an agreement with the original equipment manufacturer (OEM) to obtain said data, are ELD providers required to enter an agreement with the OEM?
*************************************************************************************************************************************************************************************************************************
The ELD provider is required to use all means necessary to obtain the required ELD data from the ECM; this may include entering into an agreement with the OEM or another third party. Using manual entry when the ECM is available would not be in compliance with the rule and the device would be subject to removal from the list of self-certified and registered ELDs.

How should the ELD handle the dashboard odometer display not matching the odometer value returned by the ECM? For instance, when the engine is replaced and the value is not synced.
*************************************************************************************************************************************************************************************
If the dashboard odometer display does not match the odometer value returned by the ECM, the ECM odometer value must be identified as the valid value.

Data Recording
--------------
How should the Time Zone Offset from UTC handle daylight savings time?
**********************************************************************
The rule calls for storing date/time information using UTC and transmitting data in the data file using the UTC offset in effect at the carrier’s home terminal. This means that when daylight saving’s time is in effect at the home terminal, it should be included in the UTC offset. It would not be included in the UTC offset if daylight savings time is not in effect at the home terminal.

Would server-based events (e.g., edits and assignment of unassigned events) generate a different sequence ID than that generated by the ELD?
*********************************************************************************************************************************************
The sequence ID must be continuous to its source device. In an ELD support system, that system may have its own sequence ID generator. Because events cannot be deleted, it should be possible to observe each component in the ELD system which generates sequence IDs and find a continuous list of events for each sequence number without any gaps.

In a portable device platform, should the event identifier sequence number be associated with the portable device or the vehicle’s black box?
**********************************************************************************************************************************************
The ELD Rule requires that the event identifier sequence number be consistently applied. It does not specify if the event identifier sequence number must be associated with the portable device or the vehicle’s black box, this is left to the discretion of the provider.

When an existing record becomes inactive and its replacement record becomes active or inactive-change-requested, is it acceptable that the replacement record has the same sequence ID as the record it replaces?
*******************************************************************************************************************************************************************************************************************
No, the replacement record must have its own sequence ID number.

When a driver edits an event or a motor carrier suggests an edit, the new event record should have a new sequence ID number.

When creating an ELD file as specified in Section 4.9.2, when a field specifies activities on a per-CMV basis (such as engine power-up and shut-down activities), which records should be included?
*******************************************************************************************************************************************************************************************************
The ELD file referenced in Section 4.9.2 is specific to the file that the motor carrier must provide to the safety official during an investigation. Therefore, any subset of drivers and vehicles, for a subset 6-months specified by an authorized safety official must be included.

Malfunctions
------------
In Section 4.6.1.7, the rule requires that “an ELD must verify [data transfer] functionality every seven days.” What does this mean?
***************************************************************************************************************************************
The ELD must verify that it continues to transfer data using the methods it is designed to support. This verification must happen every seven days, either automatically or requiring a manual step from the driver. The specific means of verification are left up to the provider. If the ELD has transferred data in this timeframe, that would be an acceptable means of verifying connectivity. FMCSA will also make available a verification function for the telematics option that may be used to verify connectivity without actually transferring an ELD file. Check the ELD Provider Website for more information.

How should an ELD respond to battery issues, such as an ELD battery dying due to high temperatures or when charge power runs out? (added Sept-2017)
***************************************************************************************************************************************************
In cases when the ELD’s battery dies or when the charge power runs out, the ELD must generate a malfunction event. The ELD must set a power compliance malfunction if the power data diagnostics event indicates an aggregated in motion driving time understatement of 30 minutes or more on the ELD over a 24-hour period across all driver profiles, including unidentified driver profile.

How will FMCSA ensure that the ELD data is transferred securely? (added Sept-2017)
**********************************************************************************
During data transfers, Appendix A requires additional security protocol through encryption, American National Standard for Information Technology, IEEE Standards Association, and others as incorporated by reference in Section 6.



Driver Edits
------------
Does the rule require that the driver have editing capabilities?
****************************************************************
Yes, the ELD must allow the driver to review, edit, and annotate their ELD records to account for errors and omissions, as specified in Section 4.3.2.8.

If a driver makes an error, can that record be deleted?
*******************************************************
Deleting records is not permitted. To correct errors, drivers must be able to edit, enter missing information into, and annotate the ELD records. The original record must be retained and receive an inactive status.

Can a carrier make edits to a driver’s logs?
********************************************
The rule does allow for carriers, using the support personnel account, to propose changes to a driver’s ELD data. To protect the driver’s logs from manipulation, edits requested by anyone or any system other than the driver must require the driver’s electronic confirmation or rejection.

Are there any edits that are not permitted?
*******************************************
Events of type 2 (intermediate log), 5 (login/logout), 6 (CMV power-up/shut-down) or 7 (malfunction/diagnostic) may not be edited in any way. This includes assumption of logs from the unidentified driving profile. If unidentified driving time gets assumed by a driver, the automatically generated change in duty status events would be associated with the driver, but any intermediate logs would not.

Edits which reduce the total amount of driving time recorded by the ELD are not allowed. Unidentified driving time may be transferred to a driver and driving time may be transferred between drivers in a team driving scenario, but driving time may not otherwise be re-assigned and may never be cumulatively changed. See section 4.3.2.8.2 for more information on editing limitations.

Will hours of service violations notification be eliminated upon an annotation being made by the driver or motor carrier? (added Sept-2017)
*******************************************************************************************************************************************
No, the annotations made by the driver or the motor carrier will be used by safety officials to help determine if an hours of service violation has occurred. Annotations cannot impact any automatically recorded driving time.


What is the Commercial Motor Vehicle Power Unit Number?
*******************************************************
The power unit number is the identifier the motor carrier uses to identify the power unit.

How should CMV Power units be listed if power units are used more than once by the driver within the period set requested?
*****************************************************************************************************************************
The User list will include all drivers and co-drivers. The CMV list will include multiple trucks in rank order of use, not just one entry per truck (see Sections 4.8.2.1.2 and 4.8.2.1.3).

Duty Status Categories
----------------------

How should the duty status categories be displayed? (added Sept-2017)
*********************************************************************
In displaying the duty status, the full name of the duty status should be used (i.e., Off Duty, Sleeper Berth, Driving, or On-Duty Not-Driving

Is the ELD required to automatically change duty status from Sleeper Berth to Driving upon sensing movement?
************************************************************************************************************
Yes, the ELD is required to automatically change a driver’s duty status to Driving when the vehicle reaches the 5 mph threshold or less after being in the Sleeper Berth duty status.

In Table 6, Event Type 3, it lists “Driver indication for PC, YM, and WT cleared.” What does WT refer to?
*********************************************************************************************************
“WT” refers to “waiting time.” This language is carried over from the draft rule and was included in the final version in error. There are no other references to WT found within the rule.

When is the ELD required to automatically change the driver’s duty status to driving? (added Sept-2017)
*******************************************************************************************************
The “driving” duty status must be automatically recorded by the ELD when the operated vehicle meets the configured threshold; not to exceed 5 mph. See Section 4.4.1.

Is the ELD required to automatically change duty status from Sleeper Berth to Driving upon sensing movement? (added Sept-2017)
******************************************************************************************************************************
Yes, the ELD is required to automatically change a driver’s duty status to Driving when the vehicle reaches the 5 mph threshold or less after being in the Sleeper Berth duty status.

If a driver forgets to indicate the start of Personal Conveyance (PC) or Yard Moves (YM), can they later edit that automatically generated driving event and change that Off Duty to PC or On Duty to YM? (added Sept-2017)
***************************************************************************************************************************************************************************************************************************
If a driver forgets to select special driving categories (Personal Conveyance or Yard Move) at the beginning and/or end of the special driving category, then the driver can make an annotation in the ELD record identifying the start and end of the special driving category. The ELD rule does allow driving time to be shortened for the purpose of correcting special driving category selection. 

Yard Moves
----------

How can yard moves be reflected on the ELD? (added Sept-2017)
*************************************************************
The ELD must provide an option to preconfigure drivers’ accounts with yard moves. Should the motor carrier opt to preconfigure a driver’s account with yard moves, the driver must select the beginning and end of the yard move period. The graph-grid must overlay periods of driver’s indications of yard moves using a different style line (such as dashed or dotted line) or shading. The appropriate abbreviation must also be indicated on the graph-grid.

What driver duty status should yard moves be reflected under? (added Sept-2017)
*******************************************************************************
Yard moves must be reflected as an On-Duty Not-Driving duty status.

Can an ELD use geofencing to automatically change a driver’s duty status to Yard Move?
**************************************************************************************
No. While this was permitted for AOBRDs, this is not permitted under the ELD rule. The device can use geofencing to prompt the driver to change their duty status, but it cannot change the duty status automatically.

Is there a requirement to indicate odometer or positioning at the beginning and end of a Yard Move category?
************************************************************************************************************
The position, engine hours, and vehicle miles should be recoded when the yard move starting and ending events are created as specified in Section 4.5.1.3 (Note: these elements are included by reference as Section 4.5.1.3 indicates all elements defined in Section 4.5.1.1 be included).

Can an ELD maintain a Yard Move status after the truck has been turned off?
***************************************************************************
No, once the vehicle has been turned off the yard move status is cleared. It is permissible—though not required—to prompt the driver to resume the previous duty status.


Can a carrier set parameters for yard moves via the technical specifications? (added Sept-2017)
***********************************************************************************************
Yes, while the yard move status must be selected by the driver, the ELD may allow the carrier to configure scenarios in which the driver can and cannot select the yard move.

Display and Print Requirement
-----------------------------

Does the rule require that an ELD be able to both display electronically and print?
***********************************************************************************
No. The display and print requirement serves as a backup to the data transfer methods. In the event that data transmission fails during a roadside inspection, the safety official must be able to view the required information without entering the cab of the truck. This can be accomplished via either the ELD screen or a printout from the ELD, whichever is supported by the device. If a provider selects the display requirement as the backup method, the device is not required to meet the print requirement and vice versa.

Would a PDF that can be transmitted wirelessly be acceptable in lieu of a print feature?
****************************************************************************************
No. A PDF print transmitted wirelessly would not satisfy the requirement, as print serves as a backup to data transfer.

In 4.8.1.2, the rule requires that an ELD without a printer be designed so that “the display may be reasonably viewed by an authorized safety official without entering the commercial motor vehicle.” Can you define “reasonable distance”?
************************************************************************************************************************************************************************************************************************************************
This distance is not specified in order to account for the variety in size of screens. The safety official must be able to read the screen display without having to physically enter the cab of the CMV.

The rule indicates that the graph-grid for each day’s records of duty status must be at least 6 inches by 1.5 inches in size. Can you clarify what this means for requirements for screen size?
****************************************************************************************************************************************************************************************************
This size requirement is for print display only, it does not apply to the ELD screen itself.

Is the display required to be handed to the inspector outside the truck?
************************************************************************
The display must be designed so that it can be reasonably viewed from outside the cab of the vehicle. That may require the device to be untethered from its mount or connected in a manner that would allow it to be passed outside of the vehicle for a reasonable distance.

Can the driver’s license information be omitted or redacted from the printed logs?
**********************************************************************************
The driver’s license information cannot be omitted or redacted. Section 395.25(c) lists the information the motor carrier must provide when creating a driver account and Section 4.8.1.3 lists the information that must be present on the ELD display screen or printout. This information includes the driver’s first and last name and driver’s license number and issuing state.

Section 4.8.1.3 indicates that location needs to be displayed for login/logout events. However, Section 4.5.1.5 lists required data elements for login/logout events and does not indicate that latitude and longitude need to be recorded. Which is accurate?
******************************************************************************************************************************************************************************************************************************************************************
Section 4.5.1.5 is correct, location is not required for display of login/logout events, and it is not included in the data file.

In the ELD header, should miles driven reflect miles in the current CMV or all CMVs that the driver has operated on the displayed date? What about Start End Odometer and Engine Hours?
*****************************************************************************************************************************************************************************************
The ELD header must reflect data for the current CMV. See Section 4.3.1.3 and Section 4.3.1.4.

Will ELD providers have access to the eRODS view of the extracted ELD data?
***************************************************************************
ELD providers will not have access to the eRODS view of the extracted ELD data.


When a driver makes an edit, should the original record be visible in the print or display option? (added Sept-2017)
********************************************************************************************************************
There should be an annotation on the new record indicating the edit occurred and the old record must be retained, accessible, and included in ELD outputs.

When is the ELD required to display data? (added Sept-2017)
***********************************************************
The ELD must be able to retrieve and display current data via display screen of the ELD or from a printout from the ELD at the request of a safety official.

What information must be displayed on the ELD or provided on the ELD printout? (added Sept-2017)
************************************************************************************************
The ELD must show the following information via the display of the ELD or printout:

	.. figure:: media/faq-2_displayPrintout.png

	.. figure:: media/faq-2_displayPrintout_tableview.png

What information should be displayed on the header for co-driver operations? (added Sept-2017)
**********************************************************************************************
The header should contain the information of the most recent co-driver. If there is no co-driver to report then this field should be left blank.

Data Transfer
-------------
Would it be acceptable for the data transfer to be initiated by the ELD but relayed through and communicated to FMCSA Service from the provider’s backend?
*************************************************************************************************************************************************************
Yes. As long as the file is being transferred to FMCSA through one of the acceptable data transfer methods and is being triggered by the action at roadside or a compliance investigation.

If a multi-function portable device is used for the front end of the ELD Device (i.e., user may be in a GPS application and need to switch back to the ELD application), does this count as a click in the single-step interface requirement?
************************************************************************************************************************************************************************************************************************************************************
The single-step requirement refers to compiling of the driver’s records and initiating the data transfer from within the ELD application. This step would take place once logged into the ELD application.

Will FMCSA collect State enforcement agency data transfer capabilities/preferences?
***********************************************************************************
The Federal Motor Carrier Safety Administration will not collect state enforcement agencies data transfer preferences.

Is it necessary for drivers to enter their user credentials before starting the data transfer process? (added Sept-2017)
************************************************************************************************************************
No, the data transfer process does not require a driver to enter user credentials.

ELD Output File
---------------
Section 4.8.2.1.1 specifies that the ELD output file must have one line for co-driver information and one line for CMV information. There may be multiple co-drivers and/or multiple CMVs. Should it contain the first co-driver and CMV in the day, or the last one, or is there any other option?
****************************************************************************************************************************************************************************************************************************************************************************************************
The co-driver and CMV at the time the file is generated must be reflected. The file contains sections for listing all user accounts and CMVs which are referenced during events transmitted as a part of the ELD file.

If a driver operates with multiple commercial motor vehicles (CMV), which CMV’s records must be included in the ELD file?
****************************************************************************************************************************
The file must contain all relevant records for all CMVs the driver operated within the selected reporting period.

Must the daily header display “ELD Registration ID” or “ELD Identifier”?
************************************************************************
The daily header must display the ELD Identifier.


During an investigation, how should the header section reflect ELD data?
************************************************************************
The header should be populated with the most recent CMV data and Co-Driver data (if applicable) within the report period. The actual date and location information (if available) must be reflected in the ELD data.

Is it permitted to leave certain fields blank (e.g., sequence id, record origin) for records that were originally obtained from an AOBRD (e.g., operating in a mixed fleet) or will these files be rejected?
*************************************************************************************************************************************************************************************************************
ELD output files with blank sequence ids, or record origins will be rejected. Automatic syncing of AOBRD records into an ELD system should appear as manual entries made by the driver. It is permissible for the edits to be created automatically by the system, but they must be approved by the driver.

For ELD Event Annotations or Comments records in Section 4.8.2.1.5 (ELD Data File), what do the <{Event} Date> and <{Event} Time> refer to?
*******************************************************************************************************************************************
The <{Event} Date> and <{Event} Time> refers to the date and time of the event in the ELD Event List corresponding with the annotation or comment.

Regarding Section 4.8.2.1.9, which Power Up and Power Down events should be included?
*************************************************************************************
All Power Up and Power Down events for all CMVs used by the driver within the time period that is being requested should be included, including those events that belong to another driver (e.g., if the driver was not using that CMV on that day).

Data Transfer Methods
---------------------
The rule states an ELD must use option 1 or 2 for data transfer. Can an ELD offer Web Services and USB?
*******************************************************************************************************
The manufacturer must select at least one complete option, either telematics (Email and Web Services) or local transfer (USB 2.0 and Bluetooth). So long as one option, which includes both methods, is met, the ELD is compliant. Manufacturers may offer additional methods if they choose.

For telematics data transfer, which email address should be used in the recipient field?
************************************************************ g ****************************
During Phase 1, for any ELDs self-certified and registered, it was at the inspector’s discretion to provide their email and have data sent to them directly, electronically. Starting December 18, 2017, the ELD must use the email address provided to the provider during the ELD registration process. The provided email address will be the same address provided to all providers.

Why is there a reference to the internet in Bluetooth transfer when it is part of the local transfer option?
************************************************************************************************************
“Local” in this option is referring to the ELD device, which does not need its own connection to the internet. When using Bluetooth, the inspector will share an internet connection that will be used by the ELD device to submit the output file via Web Services.

Note: The Bluetooth connection can only be guaranteed to offer connectivity to the FMCSA Web Service, so the connection in this case must come from the device itself as it may not be possible to connect to a back-office system using this connection.

Is the ELD required to encrypt files during a USB electronic data transfer?
***************************************************************************
The USB device will be self-encrypting, and will not rely on the ELD device for encryption.

Data Transfer/Connectivity Issues
---------------------------------
What if there is no internet connectivity to perform telematics data transfer between the ELD and the safety official? (added Sept-2017)
****************************************************************************************************************************************
If both the safety official and the driver are experiencing internet connectivity issues, then the safety official will use the ELD display screen or a printout from the ELD to review the ELD data. However, if the internet connectivity issue is only being experienced by the ELD, the device may be identified as a noncompliant device and the use of the display screen or printout to review the ELD data will be left to the discretion of the safety official. For web services and email, the device must be capable of independently connecting to the internet in a reliable manner whenever a roadside inspection occurs. An implementation that depends on Wi-Fi being present at the roadside or that only offers an extremely limited coverage area may not meet the requirements of an ELD.

Can an ELD connect to the ECM in a manner that relies on internet connectivity? (added Sept-2017)
*************************************************************************************************
Yes, however, the driver’s records of duty status must always be available. Therefore, if the driver is unable to access their records of duty status for a period of time due to operating in an area that has limited coverage, the driver would be considered operating without logs for that time.

What is the resolution process if the FMCSA data transfer mechanism incorrectly rejects a data file during an electronic data transfer? (added Sept-2017)
*********************************************************************************************************************************************************
If a data transfer method fails and another is available, the safety official can elect to try the alternate transfer method, or review the ELD data via display screen or printout. If validation fails and provides an error code, the provider should have a process in place that allows its customers to report the error. FMCSA will have a process in place for diagnosing and communicating any issues that arise as a result of technical difficulties experienced by the Agency. 


Registration and Self-Certification
-----------------------------------

Is the data submission and test certification a one-time effort?
****************************************************************
After the initial self-certification, providers will only need to update FMCSA with any major changes to the device.

What is the purpose of the authentication value?
************************************************
The purpose of the field is to confirm that a specific file came from a specific ELD in a specific vehicle.

Can the required ELD user manual be in digital format?
******************************************************
Yes, the required ELD manuals listed in 395.22(h) can be in digital format.

To ensure that the list of self-certified ELDs is current, ELD providers must notify FMCSA of any major changes to their device. What constitutes a major change to an ELD? (added Sept-2017)
*********************************************************************************************************************************************************************************************
If the provider makes a change that would cause any of the registered ELD values to change, an update to the ELD registration must be made; updated information will be vetted by FMCSA. The provider can also determine the best route for making this change—either updating a current listing, or registering a new ELD.

*Note:*  Any change that results in an update to the ELD Identifier or provider public/private key pair must be reflected on the ELD registration website. 

Will ELD providers be able to provide an updated public key in the event of a security breach? (added Sept-2017)
****************************************************************************************************************
Yes. The public key is required to be kept updated as part of the self-certification process. Public key updates will be vetted by FMCSA. Additional time will be needed to propagate the update into FMCSA’s systems.

Must a provider self-certify ELD software and compatible devices, i.e., smartphone, tablet? (added Sept-2017)
*************************************************************************************************************
The ELD solution must be certified and registered with FMCSA. For example, if the ELD solution is using a bring-your-own device platform or a portable device, then the software provider must self-certify and register the software and list the compatible devices.

If you are creating an application to be used on a smartphone or tablet, do you have to verify and list all the devices that the application works on? (added Sept-2017)
************************************************************************************************************************************************************************
No. For an app, it is not required that the vendor verify every single device the app will work on, but the registration must include the entire platform (i.e., Android or iPhone). The device that allows connection to the ECM, the phone/tablet OS with version range, and the app together can be certified.

If the ELD only works on certain models, the provider will need to specify (e.g., a 64-bit iPhone model with iOS 7 and above).

*Note:* There should be separate registrations for each platform (i.e., “ELD for Apple” and “ELD for
Android” would be registered separately). 

If a manufacturer offers a white-label device for sale to others with their logo on it, must all "providers" self-certify so that their own offering with branding is listed on the self-certified list? (added Sept-2017)
**************************************************************************************************************************************************************************************************************************
As part of the registration process, three pieces of information are necessary to identify a device: ELD
Identifier, ELD public key (both from registering company), and ELD Registration ID (from FMCSA). It is
allowable to register multiple devices with the same ELD Identifier and public key, but each registered
device will have its own unique ELD Registration ID.
The rule does not require that certification be done by the company manufacturing the device. Branding
partners can use the certification performed by the white label company.

In what format should I submit my public key to FMCSA? (added Sept-2017)
************************************************************************
Public keys should be submitted as a public key certificate. This certificate may be self-signed. The
certificate must conform to the specifications outlined in Section 2.2.1 of the ELD Interface Control
Document and Web Services Development Handbook.

What is an authentication value? (added Sept-2017)
**************************************************
The purpose of the authentication value is to provide a mechanism for cross-checking a transmitted ELD
file’s authenticity. Below is one example of a pseudo-algorithm that could serve as the basis for
generating an ELD authentication value:

	#. Extract some subject of the final ELD Output file. This could be as little as a few fields or as much as the entire file (minus the ELD Authentication Value). Including the ELD Registration ID would satisfy the requirement that the ELD Authentication Value verify the ELD that generated the value.
	#. Use a standard signing algorithm. Generate a signature for this content using one of the certificates submitted during ELD registration.
	#. Convert this binary signature to a string using a string encoding algorithm that does not use any of the ELD reserved characters (comma—ASCII 44 and carriage return—ASCII 13).

How many characters should my authentication value be? (added Sept-2017)
************************************************************************
An authentication value must be greater than or equal to 16 characters. For more information on this update to the technical specifications, see the ELD Interface Control Document and Web Services Development Handbook.

Removal Process
---------------
When will FMCSA start testing ELDs that have been self-certified? (added Sept-2017)
***********************************************************************************
While FMCSA will not be testing ELDs that are currently listed on the ELD registration list or during the registration process, ELD providers have the option of using the file validator available on the ELD  registration page to test that their ELD produces a valid file. The ELD registration page will soon be updated with a data transfer testing environment for providers to test their ELD solution date transfer capabilities.

Will FMCSA notify the industry when an ELD is under an investigation? (added Sept-2017)
***************************************************************************************
No. Consistent with its approach to other investigations, FMCSA will not publicize ELD investigations. Should an ELD investigation result in a device being deemed a noncompliant device, then that device will be listed on the ELD revoked list.