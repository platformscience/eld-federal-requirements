4. FUNCTIONAL REQUIREMENTS
===========================

.. _4.1:

4.1 ELD User Accounts
---------------------

.. _4.1.1:

4.1.1 Account Types
~~~~~~~~~~~~~~~~~~~

An ELD must support a user account structure that separates drivers and motor carrier’s support personnel (i.e. non-drivers).

4.1.2 Account Creation
~~~~~~~~~~~~~~~~~~~~~~

1. Each user of the ELD must have a valid active account on the ELD with a unique identifier assigned by the motor carrier. 

2. Each driver account must require the entry of the driver’s license number and the State or jurisdiction that issued the driver’s license into the ELD during the account creation process. The driver account must securely store this information on the ELD. 

3. An ELD must not allow creation of more than one driver account associated with a driver’s license for a given motor carrier. 

4. A driver account must not have administrative rights to create new accounts on the ELD. 

5. A support personnel account must not allow recording of ELD data for its account holder. 

6. An ELD must reserve a unique driver account for recording events during nonauthenticated operation of a CMV. This appendix will refer to this account as the “unidentified driver account.”

.. _4.1.3:

4.1.3 Account Security
~~~~~~~~~~~~~~~~~~~~~~

1. An ELD must provide secure access to data recorded and stored on the
   system by requiring user authentication during system login.
2. Driver accounts must only have access to data associated with that
   driver, protecting the authenticity and confidentiality of the
   collected information.

.. _4.1.4:

4.1.4 Account Management
~~~~~~~~~~~~~~~~~~~~~~~~

1. An ELD must be capable of separately recording and retaining ELD data
   for each individual driver using the ELD.
2. An ELD must provide for and require **concurrent authentication for
   team drivers**
3. If more than one ELD unit is used to record a driver’s electronic
   records within a motor carrier’s operation, the ELD in the vehicle
   the driver is operating most recently must be able to produce a
   complete ELD report for that driver, on demand, for the current
   24-hour period and the previous 7 consecutive days.

.. _4.1.5:

4.1.5 Non-Authenticated Operation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. An ELD must associate all non-authenticated operation of a CMV with a
   **single ELD account labeled unidentified driver**.
2. If a driver does not log onto the ELD, as soon as the vehicle is in
   motion, the ELD must:

   -  Provide a **visual or visual and audible warning** reminding the
      driver to stop and log in to the ELD;
   -  Record accumulated driving and on-duty, not-driving, time in
      accordance with the ELD defaults described in section
      :ref:`4.4.1` of this appendix under the unidentified
      driver profile; and
   -  Not allow entry of any information into the ELD other than a
      response to the login prompt.

--------------

.. _4.2:

4.2 ELD-Vehicle Interface
-------------------------

1. An ELD must be integrally synchronized with the engine of the CMV.
   Engine synchronization for purposes of ELD compliance means the
   monitoring of the vehicle’s engine operation to automatically capture
   the engine’s power status, vehicle’s motion status, miles driven
   value, and engine hours value when the CMV’s engine is powered.

2. An ELD used while operating a CMV that is a model year 2000 or later
   model year, as indicated by the vehicle identification number (VIN),
   that has an engine electronic control module (ECM) must establish a
   link to the engine ECM when the CMV’s engine is powered and receive
   automatically the engine’s power status, vehicle’s motion status,
   miles driven value, and engine hours value through the serial or
   Control Area Network communication protocols supported by the
   vehicle’s engine ECM. If the vehicle does not have an ECM, an ELD may
   use alternative sources to obtain or estimate these vehicle
   parameters with the listed accuracy requirements under section
   :ref:`4.3.1` of this appendix.

--------------

.. _4.3:

4.3 ELD Inputs
--------------

.. _4.3.1:

4.3.1 ELD Sensing
~~~~~~~~~~~~~~~~~

.. _4.3.1.1:

4.3.1.1 Engine Power Status
^^^^^^^^^^^^^^^^^^^^^^^^^^^

An ELD must be powered and become fully functional within 1 minute of the vehicle’s engine receiving power and must remain powered for as long as the vehicle’s engine stays powered.

.. _4.3.1.2:

4.3.1.2 Vehicle Motion Status
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. An ELD must automatically determine whether a CMV is in motion or
   stopped by comparing the vehicle speed information with respect to a
   set speed threshold as follows:

   -  Once the **vehicle speed exceeds the set speed threshold, it must
      be considered in motion**.
   -  Once in motion, the vehicle must be considered in motion until its
      **speed falls to 0 miles per hour and stays at 0 miles per hour
      for 3 consecutive seconds. Then, the vehicle will be considered
      stopped**.
   -  An ELD’s set speed threshold for determination of the in-motion
      state for the purpose of this section must **not be configurable
      to greater than 5 miles per hour**.

2. If an ELD is required to have a link to the vehicle’s engine ECM,
   vehicle speed information must be acquired from the engine ECM.
   Otherwise, vehicle speed information must be acquired using an
   independent source apart from the positioning services described
   under section :ref:`4.3.1.6` of this appendix and must be
   accurate within ±3 miles per hour of the CMV’s true ground speed for
   purposes of determining the in motion state for the CMV.

.. _4.3.1.3:

4.3.1.3 Vehicle Miles
^^^^^^^^^^^^^^^^^^^^^

1. An ELD must monitor vehicle miles as accumulated by a CMV over the
   course of an ignition power on cycle (accumulated vehicle miles) and
   over the course of CMV’s operation (total vehicle miles). Vehicle
   miles information must use or must be converted to units of **whole
   miles**.
2. If the ELD is required to have a link to the vehicle’s engine ECM as
   specified in section :ref:`4.2` of this appendix:

   -  The ELD must monitor the engine ECM’s odometer message broadcast
      and use it to log total vehicle miles information; and
   -  The ELD must use the odometer message to determine accumulated
      vehicle miles since engine’s last power on instance.

3. If the ELD is not required to have a link to the vehicle’s engine ECM
   as specified in section :ref:`4.2` of this appendix, the
   accumulated vehicle miles indication must be obtained or estimated
   from a source that is accurate to within ±10% of miles accumulated by
   the CMV over a 24-hour period as indicated on the vehicle’s odometer
   display.

.. _4.3.1.4:

4.3.1.4 Engine Hours
^^^^^^^^^^^^^^^^^^^^

1. An ELD must monitor engine hours of the CMV over the course of an
   ignition power on cycle (elapsed engine hours) and over the course of
   the total engine hours of the CMV’s operation. Engine hours must use
   or must be converted to **hours in intervals of a tenth of an hour**.
2. If an ELD is required to have a link to the vehicle’s engine ECM, the
   ELD must monitor the engine ECM’s total engine hours message
   broadcast and use it to log total engine hours information.
   Otherwise, engine hours must be obtained or estimated from a source
   that monitors the ignition power of the CMV and must be accurate
   within ±0.1 hour of the engine’s total operation within a given
   ignition power on cycle.

.. _4.3.1.5:


4.3.1.5 Date and Time
^^^^^^^^^^^^^^^^^^^^^

1. The ELD must obtain and record the date and time information
   automatically without allowing any external input or interference
   from a motor carrier, driver, or any other person.
2. The ELD time must be synchronized to Coordinated Universal Time (UTC)
   and the absolute deviation from UTC must not exceed 10 minutes at any
   point in time.

.. _4.3.1.6:

4.3.1.6 CMV Position
^^^^^^^^^^^^^^^^^^^^

1. An ELD must determine automatically the position of the CMV in
   standard latitude/longitude coordinates with the accuracy and
   availability requirements of this section.
2. The ELD must obtain and record this information **without allowing
   any external input or interference from a motor carrier, driver, or
   any other person**.
3. CMV position measurement must be accurate to ±0.5 mile of absolute
   position of the CMV when an ELD measures a valid latitude/longitude
   coordinate value.
4. Position information must be obtained in or converted to standard
   signed latitude and longitude values and must be expressed as decimal
   degrees to hundreds of a degree precision (i.e., a decimal point and
   two decimal places).
5. Measurement accuracy combined with the reporting precision
   requirement implies that position reporting accuracy will be on the
   order of ±1mile of absolute position of the CMV during the course of
   a CMV’s commercial operation.
6. During periods of a **driver’s indication of personal use of the CMV,
   the measurement reporting precision requirement is reduced to tenths
   of a degree** (i.e., a decimal point and single decimal place) as
   further specified in section :ref:`4.7.3` of this appendix.
7. An ELD must be able to acquire a valid position measurement **at
   least once every 5 miles of driving**; however, the ELD records CMV
   location information only during ELD events as specified in section
   :ref:`4.5.1` of this appendix.

.. _4.3.1.7:

4.3.1.7 CMV VIN
^^^^^^^^^^^^^^^

The vehicle identification number (VIN) for the power unit of a CMV must automatically obtained from the engine ECM and recorded if it is on the vehicle data bus.

.. _4.3.2:

4.3.2 Driver’s Manual Entries
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. An ELD must prompt the driver to input information into the ELD only
   when the CMV is stationary and driver’s duty status is not on-duty
   driving, except for the condition specified in section
   :ref:`4.4.1.2` of this appendix.
2. If the driver’s duty status is driving, an ELD must only allow the
   driver who is operating the CMV to change the driver’s duty status to
   another duty status.
3. A stopped vehicle must maintain zero (0) miles per hour speed to be
   considered stationary for purposes of information entry into an ELD.
4. An ELD must allow an authenticated co-driver who is not driving, but
   who has logged into the ELD prior to the vehicle being in motion, to
   make entries over his or her own records when the vehicle is in
   motion. The **ELD must not allow co-drivers to switch driving roles
   when the vehicle is in motion**.

.. _4.3.2.1:

4.3.2.1 Driver’s Entry of Required Event Data Fields
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. An ELD must provide a means for a driver to enter information pertaining to the driver’s ELD records manually, e.g., CMV power unit number, as specified in section :ref:`7.4` of this appendix; trailer number(s), as specified in section :ref:`7.42` ; and shipping document number, as specified in section :ref:`7.39`.

2. If the motor carrier populates these fields automatically, the ELD must provide means for the driver to review such information and make corrections as necessary.

.. _4.3.2.2:

4.3.2.2 Driver’s Status Inputs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _4.3.2.2.1:

4.3.2.2.1 Driver’s Indication of Duty Status
''''''''''''''''''''''''''''''''''''''''''''

1. An ELD must provide a means for the authenticated driver to select a
   driver’s duty status.
2. The ELD must use the ELD duty status categories listed in **`Table 1` of this appendix.**

.. _`Table 1`:

   .. rubric:: Table 1 Duty Status Categories
      :name: table-1-duty-status-categories

   +-----------------------+----------------+---------------+
   | Duty Status           | Abbreviation   | Data Coding   |
   +=======================+================+===============+
   | Off Duty              | OFF            | 1             |
   +-----------------------+----------------+---------------+
   | Sleeper Berth         | SB             | 2             |
   +-----------------------+----------------+---------------+
   | Driving               | D              | 3             |
   +-----------------------+----------------+---------------+
   | On-duty Not Driving   | ON             | 4             |
   +-----------------------+----------------+---------------+

.. _4.3.2.2.2:

4.3.2.2.2 Driver’s Indication of Situations Impacting Driving Time Recording
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

1. An ELD must provide the means for a driver to indicate the beginning
   and end of a period when the driver may use the CMV for **authorized
   personal use or for performing yard moves**. The ELD must acquire
   this status in a standard format from the category list in `Table
   2`_ of this appendix. This list must be supported
   independent of the duty status categories described in section
   :ref:`4.3.2.2.1` of this appendix.

   .. rubric:: `Table 2`_ Categories for Driver’s Indication of Situations
      Impacting Driving Time Recording
      :name: table-2-categories-for-drivers-indication-of-situations-impacting-driving-time-recording

.. _Table 2:

   +----------------------------------+----------------+---------------+
   | Category                         | Abbreviation   | Data Coding   |
   +==================================+================+===============+
   | Authorized Personal Use of CMV   | PC             | 1             |
   +----------------------------------+----------------+---------------+
   | Yard Moves                       | YM             | 2             |
   +----------------------------------+----------------+---------------+
   | Default: None                    | --             | 0             |
   +----------------------------------+----------------+---------------+

2. An ELD must allow a driver to select only categories that a **motor carrier enables by configuration** for that driver, as described in section :ref:`4.3.3.1.1` of this appendix.
3. An ELD must only allow **one category to be selected at any given time** and use the latest selection by the driver.
4. The ELD **must prompt the driver to enter an annotation upon
   selection of a category** from `Table 2`_ of this
   appendix and record the driver’s entry.
5. A driver’s indication of special driving situation must **reset to
   none if the ELD or CMV’s engine goes through a power off cycle (ELD
   or CMV’s engine turns off and then on) except if the driver has
   indicated authorized personal use of CMV. If the driver has indicated
   authorized personal use of the CMV, the ELD must require confirmation
   of continuation of the authorized personal use of CMV condition by530
   the driver. If not confirmed by the driver and the vehicle is in
   motion, the ELD must default to none**.

.. _4.3.2.3:

4.3.2.3 Driver’s Certification of Records
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. An ELD must include a function whereby a driver can certify the
   driver’s records at the end of a 24-hour period.

   -  This function, when selected, must display a statement that reads
      “I hereby certify that my data entries and my record of duty
      status for this 24-hour period are true and correct.”
   -  An ELD must prompt the driver to select “Agree” or “Not ready.” An
      ELD must record the driver’s affirmative selection of “Agree” as
      an event.

2. An ELD must only allow the authenticated driver to certify records
   associated with that driver.
3. If any edits are necessary after the driver certifies the records for
   a given 24- hour period, the ELD must require and prompt the driver
   to re-certify the updated records.
4. If there are any past records on the ELD (excluding the current
   24-hour period) that require certification or re-certification by the
   driver, the ELD must indicate the required driver action on the ELD’s
   display and prompt the driver to take the necessary action during the
   login and logout processes.

.. _4.3.2.4:

4.3.2.4 Driver’s Data Transfer Initiation Input
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. An ELD must provide a standardized single-step driver interface for
   compilation of driver’s ELD records and initiation of the data
   transfer to authorized safety officials when requested during a
   roadside inspection.
2. The ELD must input the data transfer request from the driver, require
   confirmation, present and request selection of the supported data
   transfer options by the ELD, and prompt for entry of the output file
   comment as specified in section :ref:`4.3.2.5` of this
   appendix. Upon confirmation, the ELD must generate the compliant
   output file and perform the data transfer.
3. The supported single-step data transfer initiation mechanism (such as
   a switch or an icon on a touch-screen display) must be clearly marked
   and visible to the driver when the vehicle is stopped.

.. _4.3.2.5:

4.3.2.5 Driver’s Entry of an Output File Comment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An ELD must accommodate the entry of an output file comment up to 60 long. If an authorized safety official provides a key phrase code during an inspection to be included in the output file comment, must be entered and embedded in the electronic ELD records in the dataset as specified in section :ref:`4.8.2.1.1` this appendix. The default value for the output file comment must be
blank. This output file comment must be used only for the creation of related data files for the intended time, place, and ELD user.

.. _4.3.2.6:

4.3.2.6 Driver’s Annotation of Records
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. An ELD must allow a driver to add annotations in text format to
   recorded, entered, or edited ELD events.
2. The ELD must require annotations to be **4 characters or longer**,
   including embedded spaces if driver annotation is required and driver
   is prompted by the ELD.

.. _4.3.2.7:

4.3.2.7 Driver’s Entry of Location Information
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. An ELD must allow manual entry of a CMV’s location by the driver in
   text format in support of the driver edit requirements described in
   section :ref:`4.3.2.8` of this appendix.
2. The driver’s manual location entry must be available as an option to
   a driver only when prompted by the ELD under allowed conditions as
   described in section :ref:`4.6.1.4` of this appendix.
3. A manual location entry must show “M” in the latitude/longitude
   coordinates fields in ELD records.

.. _4.3.2.8:

4.3.2.8 Driver’s Record Entry/Edit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. An ELD must provide a mechanism for a driver to review, edit, and
   annotate the driver’s ELD records when a notation of errors or
   omissions is necessary or enter the driver’s missing ELD records
   subject to the requirements specified in this section.
2. An ELD must not permit alteration or erasure of the original
   information collected concerning the driver’s ELD records or
   alteration of the source data streams used to provide that
   information.

.. _4.3.2.8.1:

4.3.2.8.1 Mechanism for Driver Edits and Annotations
''''''''''''''''''''''''''''''''''''''''''''''''''''

1. If a driver edits or annotates an ELD record or enters missing
   information, the act must not overwrite the original record.
2. The ELD must use the process outlined in section
   :ref:`4.4.4.2` of this appendix to configure required
   event attributes to track the edit history of records.
3. Driver edits must be accompanied by an annotation. The ELD must
   prompt the driver to annotate edits.

.. _4.3.2.8.2:

4.3.2.8.2 Driver Edit Limitations
'''''''''''''''''''''''''''''''''

1. An ELD must not allow or require the editing or manual entry of
   records with the following event types, as described in section
   :ref:`7.25` of this appendix:

   +--------------+-----------------------------------------------+
   | Event Type   | Description                                   |
   +==============+===============================================+
   | 2            | An intermediate log,                          |
   +--------------+-----------------------------------------------+
   | 5            | A driver’s login/logout activity,             |
   +--------------+-----------------------------------------------+
   | 6            | CMV’s engine power up / shut down, or         |
   +--------------+-----------------------------------------------+
   | 7            | ELD malfunctions and data diagnostic events   |
   +--------------+-----------------------------------------------+

2. An ELD must not allow automatically recorded driving time to be
   shortened or the ELD username associated with an ELD record to be
   edited or reassigned, except under the following circumstances:

   -  Assignment of Unidentified Driver records. ELD events recorded
      under the “Unidentified Driver” profile may be edited and assigned
      to the driver associated with the record; and
   -  Correction of errors with team drivers. In the case of team
      drivers, the driver account associated with the driving time
      records may be edited and reassigned between the team drivers if
      there was a mistake resulting in a mismatch between the actual
      driver and the driver recorded by the ELD and if both team drivers
      were respectively indicated in each other’s records as a
      co-driver. The ELD must require each co-driver to confirm the
      change for the corrective action to take effect.

.. _4.3.3:

4.3.3 Motor Carrier’s Manual Entries
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An ELD must restrict availability of motor carrier entries outlined in
this section only to authenticated “support personnel” account holders.

.. _4.3.3.1:

4.3.3.1 ELD Configuration 
^^^^^^^^^^^^^^^^^^^^^^^^^

If an ELD or a technology that includes an ELD function offers configuration options to the motor carrier or the
driver that are not otherwise addressed or prohibited in this appendix,
the configuration options must not affect the ELD’s compliance with the
requirements of this rule for each configuration setting of the ELD.

.. _4.3.3.1.1:

4.3.3.1.1 Configuration of Available Categories Impacting Driving
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Time Recording 1. An ELD must allow a motor carrier to unilaterally
configure the availability of each of the three categories listed on
`Table 2`_ of this appendix that the motor carrier
chooses to authorize for each of its drivers. By default, none of these
categories must be available to a new driver account without the motor
carrier proactively configuring their availability. 2. A motor carrier
may change the configuration for the availability of each category for
each of its drivers. Changes to the configuration setting must be
recorded on the ELD and communicated to the applicable authenticated
driver during the ELD login process.

.. _4.3.3.1.2:

4.3.3.1.2 Configuration of Using ELDs
'''''''''''''''''''''''''''''''''''''

1. An ELD must provide the motor carrier the ability to configure a
   driver account exempt from use of an ELD.
2. The ELD must default the setting of this configuration option for
   each new driver account created on an ELD to “no exemption.”
3. An exemption must be proactively configured for an applicable driver
   account by the motor carrier. The ELD must prompt the motor carrier
   to annotate the record and provide an explanation for the
   configuration of exemption.
4. If a motor carrier configures a driver account as exempt

   -  The ELD must present the configured indication that is in effect
      for that driver during the ELD login and logout processes.
   -  The ELD must continue to record ELD driving time but suspend
      detection of missing data elements data diagnostic event for the
      driver described in section :ref:`4.6.1.5` of this
      appendix and data transfer compliance monitoring function
      described in section :ref:`4.6.1.7` when such driver
      is authenticated on the ELD.

.. _4.3.3.1.3:

4.3.3.1.3 Motor Carrier’s Post-Review Electronic Edit Requests
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

1. An ELD may allow the motor carrier (via a monitoring algorithm or
   support personnel) to screen, review, and request corrective edits to
   the driver’s certified (as described in section
   :ref:`4.3.2.3` of this appendix) and submitted records
   through the ELD system electronically. If this function is
   implemented by the ELD, the ELD must also support functions for the
   driver to see and review the requested edits.
2. Edits requested by anyone or any system other than the driver must
   require the driver’s electronic confirmation or rejection.


------------

.. _4.4:

4.4 ELD Processing and Calculations
-----------------------------------

.. _4.4.1:

4.4.1 Conditions for Automatic Setting of Duty Status
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _4.4.1.1:

4.4.1.1 Automatic Setting of Duty Status to Driving
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An ELD must automatically record driving time when the vehicle is in
motion by setting duty status to driving for the driver unless, before
the vehicle is in motion, the driver:

1. Sets the duty status to **off-duty and indicates personal use of
   CMV**, in which case duty status must remain off-duty until
   **driver’s indication of the driving condition ends**; or
2. Sets the duty status to **on-duty not driving and indicates yard
   moves**, in which case duty status must remain on-duty not driving
   until **driver’s indication of the driving condition ends**.

.. _4.4.1.2:

4.4.1.2 Automatic Setting of Duty Status to On-Duty Not Driving
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When the duty status is set to driving, and the CMV has not been
in-motion for 5 consecutive minutes, the ELD must prompt the driver to
confirm continued driving status or enter the proper duty status. If the
driver does not respond to the ELD prompt within 1-minute after
receiving the prompt, the ELD must automatically switch the duty status
to on-duty not driving. The time thresholds for purposes of this section
must not be configurable.

.. _4.4.1.3:

4.4.1.3 Other Automatic Duty-Status Setting Actions Prohibited
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An ELD must not feature any other automatic records of duty setting
mechanism than those described in sections `4.4.1.1`_
and :ref:`4.4.1.2` of this appendix. Duty status changes
that are not initiated by the driver, including duty status alteration
recommendations by motor carrier support personnel or a software
algorithm, are subject to motor carrier edit requirements in section
:ref:`4.3.3.1.3`.

.. _4.4.2:

4.4.2 Geo-Location Conversions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. For each change in duty status, the ELD must convert automatically
   captured vehicle position in latitude/longitude coordinates into
   geo-location information, indicating approximate distance and
   direction to an identifiable location corresponding to the name of a
   nearby city, town, or village, with a State abbreviation.
2. Geo-location information must be derived from a database that
   contains all cities, towns, and villages with a population of 5,000
   or greater and listed in ANSI INCITS 446-2008 (R2013) (incorporated
   by reference, see § 395.38).
3. An ELD’s viewable outputs (such as printouts or display) must feature
   geolocation information as place names in text format.

.. _4.4.3:

4.4.3 Date and Time Conversions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. An ELD must have the capability to convert and track date and time
   captured in UTC standard to the time standard in effect at driver’s
   home terminal, taking the daylight savings time changes into account
   by using the parameter “Time Zone Offset from UTC” as specified in
   section :ref:`7.41` of this appendix.
2. An ELD must record the driver’s record of duty status using the time
   standard in effect at the driver’s home terminal for a 24-hour period
   beginning with the time specified by the motor carrier for that
   driver’s home terminal.
3. The data element “Time Zone Offset from UTC” must be included in the
   “Driver’s Certification of Own Records” events as specified in
   section :ref:`4.5.1.4` of this appendix.

.. _4.4.4:

4.4.4 Setting of Event Parameters in Records, Edits, and Entries
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section describes the security measures for configuring and
tracking event attributes for ELD records, edits, and entries in a
standardized manner. 

4.4.4.1 Event Sequence Identifier (ID) number
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Each ELD event must feature an event sequence ID number. \* The event
sequence ID number for each ELD event must use continuous numbering
across all users of that ELD and across engine and ELD power on and off
cycles. \* An ELD must use the next available event sequence ID number
(incremented by one) each time a new event log is recorded. \* The event
sequence ID number must track at least the last 65,536 unique events
recorded on the ELD. 2. The continuous event sequence ID numbering
structure used by the ELD must be mapped into a continuous hexadecimal
number between 0000 (Decimal 0) and FFFF (Decimal 65535).

.. _4.4.4.2:

4.4.4.2 Event Record Status, Event Record Origin, Event Type Setting
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. An ELD must retain the original records even when allowed edits and entries are made over a driver’s ELD records.

2. An ELD must keep track of all event record history, and the process used by the ELD must produce the event record status, event record origin, and event type for the ELD records in the standard categories specified in sections :ref:`7.23`, :ref:`7.22`, and :ref:`7.25` of this appendix, respectively for each record as a standard security measure. For example, an ELD may use the process outlined in sections :ref:`4.4.4.2.1` :ref:`4.4.4.2.6` to meet the requirements of this section.

.. _4.4.4.2.1:

4.4.4.2.1 Records Automatically Logged by ELD
'''''''''''''''''''''''''''''''''''''''''''''

At the instance an ELD creates a record automatically, the ELD must:

1. Set the “Event Record Status” to “1” (active); and
2. Set the “Event Record Origin” to “1” (automatically recorded by ELD).

.. _4.4.4.2.2:

4.4.4.2.2 Driver Edits
''''''''''''''''''''''

At the instance of a driver editing existing record(s), the ELD must:

1. Identify the ELD record(s) being modified for which the “Event Record
   Status” is currently set to “1” (active);
2. Acquire driver input for the intended edit and construct the ELD
   record(s) that will replace the record(s) identified in paragraph
   **:ref:`4.4.4.2.2` (1)** of this appendix;
3. Set the “Event Record Status” of the ELD record(s) identified in
   paragraph **:ref:`4.4.4.2.2` (1)** of this appendix, which
   is being modified, to “2” (inactive-changed);
4. Set the “Event Record Status” of the ELD record(s) constructed in
   paragraph **:ref:`4.4.4.2.2` (2)** of this appendix to “1”
   (active); and
5. Set the “Event Record Origin” of the ELD record(s) constructed in
   paragraph **:ref:`4.4.4.2.2` (2)** of this appendix to “2”
   (edited or entered by the driver).

.. _4.4.4.2.3:

4.4.4.2.3 Driver Entries
''''''''''''''''''''''''

When a driver enters missing record(s), the ELD must:

1. Acquire driver input for the missing entries being implemented and
   construct the new ELD record(s) that will represent the driver
   entries;
2. Set the “event record status” of the ELD record(s) constructed in
   paragraph **:ref:`4.4.4.2.3` (1)** of this appendix to “1”
   (active); and
3. Set the “event record origin” of the ELD record(s) constructed in
   paragraph **:ref:`4.4.4.2.3` (1)** of this appendix to “2”
   (edited or entered by the driver).

.. _4.4.4.2.4:

4.4.4.2.4 Driver’s Assumption of Unidentified Driver Logs
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''

When a driver reviews and assumes ELD record(s) logged under the
unidentified driver profile, the ELD must:

1. Identify the ELD record(s) logged under the unidentified driver
   profile that will be reassigned to the driver;
2. Use elements of the unidentified driver log(s) from paragraph
   **:ref:`4.4.4.2.4` (1)** of this appendix and acquire
   driver input to populate missing elements of the log originally
   recorded under the unidentified driver profile, and construct the new
   event record(s) for the driver;
3. Set the event record status of the ELD record(s) identified in
   paragraph **:ref:`4.4.4.2.4` (1)** of this appendix, which
   is being modified, to “2” (inactive–changed);
4. Set the event record status of the ELD record(s) constructed in
   paragraph **:ref:`4.4.4.2.4` (2)** of this appendix to “1”
   (active); and
5. Set the event record origin of the ELD record(s) constructed in
   paragraph **:ref:`4.4.4.2.4` (2)** of this appendix to “4”
   (assumed from unidentified driver profile).

.. _4.4.4.2.5:

4.4.4.2.5 Motor Carrier Edit Suggestions
''''''''''''''''''''''''''''''''''''''''

If a motor carrier requests an edit on a driver’s records
electronically, the ELD must:

1. Identify the ELD record(s) the motor carrier requests to be modified
   for which the “event record status” is currently set to “1” (active);
2. Acquire motor carrier input for the intended edit and construct the
   ELD record(s) that will replace the record identified in paragraph
   **:ref:`4.4.4.2.5` (1)** of this appendix—if approved by
   the driver;
3. Set the event record status of the ELD record(s) in paragraph
   **:ref:`4.4.4.2.5` (2)** of this appendix to “3”
   (inactive–change requested); and
4. Set the event record origin of the ELD record constructed in
   paragraph **:ref:`4.4.4.2.5` (2)** of this appendix to “3”
   (edit requested by an authenticated user other than the driver).

.. _4.4.4.2.6:

4.4.4.2.6 Driver’s Actions Over Motor Carrier Edit Suggestions
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

1. If edits are requested by the motor carrier, the ELD must allow the
   driver to review the requested edits and indicate on the ELD whether
   the driver confirms or rejects the requested edit(s).
2. If the driver approves the motor carrier’s edit suggestion the ELD
   must:

   -  Set the event record status of the ELD record(s) identified under
      paragraph **:ref:`4.4.4.2.5` (1)** of this appendix
      being modified, to “2” (inactive–changed); and
   -  Set the “event record status” of the ELD record(s) constructed in
      paragraph **:ref:`4.4.4.2.5` (2)** of this appendix to
      “1” (active).

3. If the driver disapproves the motor carrier’s edit(s) suggestion, the
   ELD must set the “event record status” of the ELD record(s)
   identified in paragraph **:ref:`4.4.4.2.5` (2)** of this
   appendix to “4” (inactive–change rejected).

.. _4.4.5:

4.4.5 Data Integrity Check Functions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. An ELD must support standard security measures that require the
   calculation and recording of standard data check values for each ELD
   event recorded, for each line of the output file, and for the entire
   data file to be generated for transmission to an authorized safety
   official or the motor carrier.
2. For purposes of implementing data check calculations, the
   alphanumeric-tonumeric mapping provided in `Table 3`_
   of this appendix must be used.
3. Each ELD event record type specified in sections :ref:`4.5.1.1` and :ref:`4.5.1.3` of this appendix must include an event data check value, which must be calculated as specified in section :ref:`4.4.5.1`. An event data check value must be calculated at the time of the following instances and must accompany that event record thereafter:

   -  When an event record is automatically created by the ELD;
   -  When an authorized edit is performed by the driver on the ELD;
   -  When an electronic edit proposal is created by the motor carrier
      through the ELD system.
   -  Each line of the ELD output file must include a line data check
      value, which must be calculated as specified in section :ref:`4.4.5.2` of
      this appendix.
   -  Each ELD report must also include a file data check value, which
      must be calculated as specified in section
      :ref:`4.4.5.3` of this appendix.

.. _4.4.5.1:

4.4.5.1 Event Data Check
^^^^^^^^^^^^^^^^^^^^^^^^

The event data check value must be calculated as follows. 

.. _4.4.5.1.1:

4.4.5.1.1 Event Checksum Calculation 
''''''''''''''''''''''''''''''''''''''

1. A checksum calculation includes the summation of numeric values or mappings of a specified group of alphanumeric data elements. The ELD must calculate an event checksum value associated with each ELD event at the instance of the event record being created. 

2. The event record elements that must be included in the
checksum calculation are the following: \* <Event Type>, \* <Event
Code>, \* <Event Date>, \* <Event Time>, \* <Vehicle Miles>, \* <Engine
Hours>, \* <Event Latitude>, \* <Event Longitude>, \* <CMV number>, and
\* <ELD username>. 3. The ELD must sum the numeric values of all
individual characters making up the listed data elements using the
character to decimal value coding specified in **`Table
3 <#Table-3>`__** of this appendix, and use the 8-bit lower byte of the
hexadecimal representation of the summed total as the event checksum
value for that event.

.. _4.4.5.1.2:

4.4.5.1.2 Event Data Check Calculation
''''''''''''''''''''''''''''''''''''''

The event data check value must be the hexadecimal representation of the
output 8-bit byte, after the below bitwise operations are performed on
the binary representation of the event checksum value, as set forth
below:

1. Three consecutive circular shift left (rotate no carry -left)
   operations; and
2. A bitwise exclusive OR (XOR) operation with the hexadecimal value C3
   (decimal 195; binary 11000011).

.. _4.4.5.2:

4.4.5.2 Line Data Check
^^^^^^^^^^^^^^^^^^^^^^^

A line data check value must be calculated at the time of the generation
of the ELD output file, to transfer data to authorized safety officials
or to catalogue drivers’ ELD records at a motor carrier’s facility. A
line data check value must be calculated as follows. 

4.4.5.2.1 Line Checksum Calculation
'''''''''''''''''''''''''''''''''''

1. The ELD must calculate a line checksum value
associated with each line of ELD output file at the instance when an ELD
output file is generated. 2. The data elements that must be included in
the line checksum calculation vary as per the output data file specified
in section :ref:`4.8.2.1` of this appendix. 3. The ELD must
convert each character featured in a line of output using the character
to decimal value coding specified on `Table 3`_ of this
appendix and sum the converted numeric values of each character listed
on a given ELD output line item (excluding the line data check value
being calculated), and use the 8-bit lower byte value of the hexadecimal
representation of the summed total as the line checksum value for that
line of output.

.. _4.4.5.2.2:

4.4.5.2.2 Line Data Check Calculation.
''''''''''''''''''''''''''''''''''''''

The line data check value must be calculated by performing the following
operations on the binary representation of the line checksum value as
follows: 1. Three consecutive circular shift left (rotate no carry
-left) operations on the line checksum value; and 2. A bitwise XOR
operation with the hexadecimal value 96 (decimal 150; binary 10010110).

.. _4.4.5.2.3:

4.4.5.2.3 Line Data Check Value Inclusion in Output File
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

The calculated line data check value must be appended as the last line
item of each of the individual line items of the ELD output file as
specified in the output file format in section
:ref:`4.8.2.1` of this appendix.

.. _4.4.5.3:

4.4.5.3 File Data Check
^^^^^^^^^^^^^^^^^^^^^^^

A file data check value must also be calculated at the time of the
creation of an ELD output file. A file data check value must be
calculated as follows. 

.. _4.4.5.3.1:

4.4.5.3.1 File Checksum Calculation. 
''''''''''''''''''''''''''''''''''''
1. The ELD must calculate a single 16-bit file checksum value associated with
an ELD output file at the instance when an ELD output file is generated.

2. The file data check value calculation must include all individual
line data check values contained in that file. 3. The ELD must sum all
individual line data check values contained in a data file output
created, and use the lower two 8-bit byte values of the hexadecimal
representation of the summed total as the “file checksum” value.

.. _4.4.5.3.2:

4.4.5.3.2 File Data Check Value Calculation.
''''''''''''''''''''''''''''''''''''''''''''

1. The file data check value must be calculated by performing the
   following operations on the binary representation of the file
   checksum value:

   -  Three consecutive circular shift left (aka rotate no carry -left)
      operations on each 8-bit bytes of the value; and
   -  A bitwise XOR operation with the hexadecimal value 969C (decimal
      38556; binary 1001011010011100).

2. The file data check value must be the 16-bit output obtained from the
   above process.

.. _4.4.5.3.3:

4.4.5.3.3 File Data Check Value Inclusion in Output File.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''

The calculated 16-bit file data check value must be converted to
hexadecimal 8- bit bytes and must be appended as the last line item of
the ELD output file as specified in the output file format in section
:ref:`4.8.2.1.11` of this appendix.

.. _Table 3:

Table 3 Character to Decimal Value Mapping for Checksum Calculations 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

+-----------------------------------------------+-----------------------------------------------------------------+
| Character                                     | Decimal mapping {ASCII (“Character”) (decimal)– 48 (decimal)}   |
+===============================================+=================================================================+
| "1"                                           | 1                                                               |
+-----------------------------------------------+-----------------------------------------------------------------+
| "2"                                           | 2                                                               |
+-----------------------------------------------+-----------------------------------------------------------------+
| "3"                                           | 3                                                               |
+-----------------------------------------------+-----------------------------------------------------------------+
| "4"                                           | 4                                                               |
+-----------------------------------------------+-----------------------------------------------------------------+
| "5"                                           | 5                                                               |
+-----------------------------------------------+-----------------------------------------------------------------+
| "6"                                           | 6                                                               |
+-----------------------------------------------+-----------------------------------------------------------------+
| "7"                                           | 7                                                               |
+-----------------------------------------------+-----------------------------------------------------------------+
| "8"                                           | 8                                                               |
+-----------------------------------------------+-----------------------------------------------------------------+
| "9"                                           | 9                                                               |
+-----------------------------------------------+-----------------------------------------------------------------+
| "A"                                           | 17                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "B"                                           | 18                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "C"                                           | 19                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "D"                                           | 20                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "E"                                           | 21                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "F"                                           | 22                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "G"                                           | 23                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "H"                                           | 24                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "I"                                           | 25                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "J"                                           | 26                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "K"                                           | 27                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "L"                                           | 28                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "M"                                           | 29                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "N"                                           | 30                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "O"                                           | 31                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "P"                                           | 32                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "Q"                                           | 33                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "R"                                           | 34                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "S"                                           | 35                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "T"                                           | 36                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "U"                                           | 37                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "V"                                           | 38                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "W"                                           | 39                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "X"                                           | 40                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "Y"                                           | 41                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "Z"                                           | 42                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "a"                                           | 49                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "b"                                           | 50                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "c"                                           | 51                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "d"                                           | 52                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "e"                                           | 53                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "f"                                           | 54                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "g"                                           | 55                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "h"                                           | 56                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "i"                                           | 57                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "j"                                           | 58                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "k"                                           | 59                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "l"                                           | 60                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "m"                                           | 61                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "n"                                           | 62                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "o"                                           | 63                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "p"                                           | 64                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "q"                                           | 65                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "r"                                           | 66                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "s"                                           | 67                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "t"                                           | 68                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "u"                                           | 69                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "v"                                           | 70                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "w"                                           | 71                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "x"                                           | 72                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "y"                                           | 73                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| "z"                                           | 74                                                              |
+-----------------------------------------------+-----------------------------------------------------------------+
| All other characters including blank spaces   | 0                                                               |
+-----------------------------------------------+-----------------------------------------------------------------+

--------------

.. _4.5:

4.5 ELD Recording
-----------------

.. _4.5.1:

4.5.1 Events and Data to Record
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An ELD must record data at the following discrete events: 

.. _4.5.1.1:

4.5.1.1 Event: Change in Driver’s Duty Status 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When a driver’s duty status changes, the ELD must associate the record with the driver, the record
originator—if created during an edit or entry—the vehicle, the motor
carrier, and the shipping document number and must include the following
data elements:

1.   as described in section :ref:`7.24` of this appendix;
2.   as described in section :ref:`7.23`;
3.   as described in section :ref:`7.22`;
4.   as described in section :ref:`7.25`;
5.   as described in section :ref:`7.20`;
6.  <{Event} Date> as described in section :ref:`7.8`;
7.  <{Event} Time> as described in section :ref:`7.40`;
8.  <{Accumulated} Vehicle Miles> as described in section
    :ref:`7.43`;
9.  <{Elapsed} Engine Hours> as described in section
    :ref:`7.19`;
10. <{Event} Latitude> as described in section :ref:`7.31`;
11. <{Event} Longitude> as described in section :ref:`7.33`;
12.  as described in section :ref:`7.9`;
13.  as described in section :ref:`7.35`;
14.  as described in section :ref:`7.7`;
15. <{Event} Comment /Annotation> as described in section
    :ref:`7.6`;
16.  as described in section :ref:`7.12`; and
17.  as described in section :ref:`7.21`.

.. _4.5.1.2:

4.5.1.2 Event: Intermediate Logs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. When a CMV is in motion, as described in section
   :ref:`4.3.1.2` of this appendix, and there has not been a
   duty status change event or another intermediate log event recorded
   in the previous 1-hour period, the ELD must record a new intermediate
   log event.
2. The ELD must associate the record to the driver, the vehicle, the
   motor carrier, and the shipping document number, and must include the
   same data elements outlined in section :ref:`4.5.1.1` of
   this appendix except for item (p) in section
   `4.5.1.1`_.

.. _4.5.1.3:

4.5.1.3 Event: Change in Driver’s Indication of Allowed Conditions that Impact Driving Time Recording
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. At each instance when the status of a driver’s indication of personal
   use of CMV or yard moves changes, the ELD must record a new event.
2. The ELD must associate the record with the driver, the vehicle, the
   motor carrier, and the shipping document number, and must include the
   same data elements outlined in section :ref:`4.5.1.1` of
   this appendix.

.. _4.5.1.4:

4.5.1.4 Event: Driver’s Certification of Own Records
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. At each instance when a driver certifies or re-certifies that the
   driver’s records for a given 24-hour period are true and correct, the
   ELD must record the event.
2. The ELD must associate the record with the driver, the vehicle, the
   motor carrier, and the shipping document number and must include the
   following data elements:

   -   as described in section :ref:`7.24` of this appendix;
   -   as described in section :ref:`7.25`;
   -   as described in section :ref:`7.20`;
   -   as described in section :ref:`7.41`.
   -  <{Event} Date>and as described in section :ref:`7.8`; and
   -  <{Event} Time> as described in section :ref:`7.40`.

.. _4.5.1.5:

4.5.1.5 Event: Driver’s Login/Logout Activity
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. At each instance when an authorized user logs in and out of the ELD,
   the ELD must record the event.
2. The ELD must associate the record with the driver, the vehicle, the
   motor carrier, and the shipping document number, and must include the
   following data elements:

   -   as described in section :ref:`7.24` of this appendix;
   -   as described in section :ref:`7.25`;
   -   as described in section :ref:`7.20`;
   -  <{Event} Date> as described in section :ref:`7.8`;
   -  <{Event} Time> as described in section :ref:`7.40`;
   -  <{Total} Vehicle Miles> as described in section
      :ref:`7.43`; and
   -  <{Total} Engine Hours> as described in section
      :ref:`7.19`.

.. _4.5.1.6:

4.5.1.6 Event: CMV’s Engine Power Up and Shut Down Activity
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. When a CMV’s engine is powered up or shut down, an ELD must record
   the event within 1 minute of occurrence and retain the earliest shut
   down and latest power-up event if the CMV has not moved since the
   last ignition power on cycle.
2. The ELD must associate the record with the driver or the unidentified
   driver profile, the vehicle, the motor carrier, and the shipping
   document number, and must include the following data elements:

   -   as described in section :ref:`7.24` of this appendix;
   -   as described in section :ref:`7.25`;
   -   as described in section :ref:`7.20`;
   -  <{Event} Date> as described in section :ref:`7.8`;
   -  <{Event} Time> as described in section :ref:`7.40`;
   -  <{Total} Vehicle Miles> as described in section
      :ref:`7.43`;
   -  <{Total} Engine Hours> as described in section
      :ref:`7.19`;
   -  <{Event} Latitude> as described in section :ref:`7.31`;
   -  <{Event} Longitude> as described in section :ref:`7.33`;
      and
   -   as described in section :ref:`7.9`.

.. _4.5.1.7:

4.5.1.7 Event: ELD Malfunction and Data Diagnostics Occurrence
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. At each instance when an ELD malfunction or data diagnostic event is
   detected or cleared by the ELD, the ELD must record the event.
2. The ELD must associate the record with the driver, the vehicle, the
   motor carrier, and the shipping document number, and must include the
   following data elements:

   -   as described in section :ref:`7.24` of this appendix;
   -   as described in section :ref:`7.25`;
   -   as described in section :ref:`7.20`;
   -  <Malfunction/Diagnostic Code> as described in section
      :ref:`7.34`;
   -  <{Event} Date> as described in section :ref:`7.8`;
   -  <{Event} Time> as described in section :ref:`7.40`;
   -  <{Total} Vehicle Miles> as described in section
      :ref:`7.43`; and
   -  <{Total} Engine Hours> as described in section
      :ref:`7.19`.


.. _4.6:

4.6 ELD’s Self-Monitoring of Required Functions
-----------------------------------------------

An ELD must have the capability to monitor its compliance with the
technical requirements of this section for the detectable malfunctions
and data inconsistencies listed in `Table 4`_ of this
appendix and must keep records of its malfunction and data diagnostic
event detection.

.. _Table 4:

Table 4 Standard Coding for Required Compliance Malfunction and Data Diagnostic Event Detection 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+-------------------------------+---------------------------------------------------+
| Malfunction/Diagnostic Code   | Malfunction Description                           |
+===============================+===================================================+
| P                             | “Power compliance” malfunction                    |
+-------------------------------+---------------------------------------------------+
| E                             | “Engine synchronization compliance” malfunction   |
+-------------------------------+---------------------------------------------------+
| T                             | “Timing compliance” malfunction                   |
+-------------------------------+---------------------------------------------------+
| L                             | “Positioning compliance” malfunction              |
+-------------------------------+---------------------------------------------------+
| R                             | “Data recording compliance” malfunction           |
+-------------------------------+---------------------------------------------------+
| S                             | “Data transfer compliance” malfunction            |
+-------------------------------+---------------------------------------------------+
| O                             | “Other” ELD detected malfunction                  |
+-------------------------------+---------------------------------------------------+

+-------------------------------+----------------------------------------------------------+
| Malfunction/Diagnostic Code   | Data Diagnostic Event                                    |
+===============================+==========================================================+
| 1                             | “Power data diagnostic” event                            |
+-------------------------------+----------------------------------------------------------+
| 2                             | “Engine synchronization data diagnostic” event           |
+-------------------------------+----------------------------------------------------------+
| 3                             | “Missing required data elements data diagnostic” event   |
+-------------------------------+----------------------------------------------------------+
| 4                             | “Data transfer data diagnostic” event                    |
+-------------------------------+----------------------------------------------------------+
| 5                             | “Unidentified driving records data diagnostic” event     |
+-------------------------------+----------------------------------------------------------+
| 6                             | “Other” ELD identified diagnostic event                  |
+-------------------------------+----------------------------------------------------------+

.. _4.6.1:

4.6.1 Compliance Self-Monitoring, Malfunctions and Data Diagnostic Events
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _4.6.1.1:

4.6.1.1 Power Compliance Monitoring 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. An ELD must monitor data it receives from the engine ECM or alternative sources as
allowed in sections :ref:`4.3.1.1` –
:ref:`4.3.1.4` of this appendix, its onboard sensors, and
data record history to identify instances when it may not have complied
with the power requirements specified in section
`4.3.1.1`_, in which case, the ELD must record a power
data diagnostics event for the corresponding driver(s), or under the
unidentified driver profile if no drivers were authenticated at the time
of detection. 2. An ELD must set a power compliance malfunction if the
power data diagnostics event described in paragraph
`4.6.1.1`_\ (1) of this appendix indicates an aggregated
in-motion driving time understatement of 30 minutes or more on the ELD
over a 24-hour period across all driver profiles, including the
unidentified driver profile.

.. _4.6.1.2:

4.6.1.2 Engine Synchronization Compliance Monitoring
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. An ELD must monitor the data it receives from the engine ECM or
   alternative sources as allowed in sections `4.3.1.1`_
   – :ref:`4.3.1.4` of this appendix, its onboard sensors,
   and data record history to identify instances and durations of its
   non-compliance with the ELD engine synchronization requirement
   specified in section :ref:`4.2`.
2. An ELD required to establish a link to the engine ECM as described in
   section :ref:`4.2` must monitor its connectivity to the
   engine ECM and its ability to retrieve the vehicle parameters
   described under section :ref:`4.3.1` of this appendix and
   must record an enginesynchronization data diagnostics event when it
   no longer can acquire updated values for the ELD parameters required
   for records within 5 seconds of the need.
3. An ELD must set an engine synchronization compliance malfunction if
   connectivity to any of the required data sources specified in section
   :ref:`4.3.1` of this appendix is lost for more than 30
   minutes during a 24-hour period aggregated across all driver
   profiles, including the unidentified driver profile.

.. _4.6.1.3:

4.6.1.3 Timing Compliance Monitoring
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ELD must periodically cross-check its compliance with the
requirement specified in section :ref:`4.3.1.5` of this
appendix with respect to an accurate external UTC source and must record
a timing compliance malfunction when it can no longer meet the
underlying compliance requirement.

.. _4.6.1.4:

4.6.1.4 Positioning Compliance Monitoring
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. An ELD must continually monitor the availability of valid position
   measurements meeting the listed accuracy requirements in section
   :ref:`4.3.1.6` of this appendix and must track the
   distance and elapsed time from the last valid measurement point.
2. ELD records requiring location information must use the last valid
   position measurement and include the latitude/longitude coordinates
   and distance traveled, in miles, since the last valid position
   measurement.
3. An ELD must monitor elapsed time during periods when the ELD fails to
   acquire a valid position measurement within 5 miles of the CMV’s
   movement. When such elapsed time exceeds a cumulative 60 minutes over
   a 24 hour period, the ELD must set and record a positioning
   compliance malfunction.
4. If a new ELD event must be recorded at an instance when the ELD had
   failed to acquire a valid position measurement within the most recent
   elapsed 5 miles of driving, but the ELD has not yet set a positioning
   compliance malfunction, the ELD must record the character “X” in both
   the latitude and longitude fields, unless location is entered
   manually by the driver, in which case it must log the character “M”
   instead. Under the circumstances listed in this paragraph, if the ELD
   event is due to a change in duty status for the driver, the ELD must
   prompt the driver to enter location manually in accordance with
   section :ref:`4.3.2.7` of this appendix. If the driver
   does not enter the location information and the vehicle is in motion,
   the ELD must record a missing required data element data diagnostic
   event for the driver.
5. If a new ELD event must be recorded at an instance when the ELD has
   set a positioning compliance malfunction, the ELD must record the
   character “E” in both the latitude and longitude fields regardless of
   whether the driver is prompted and manually enters location
   information.

.. _4.6.1.5:

4.6.1.5 Data Recording Compliance Monitoring
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. An ELD must monitor its storage capacity and integrity and must
   detect a data recording compliance malfunction if it can no longer
   record or retain required events or retrieve recorded logs that are
   not otherwise catalogued remotely by the motor carrier.
2. An ELD must monitor the completeness of the ELD event record
   information in relation to the required data elements for each event
   type and must record a missing data elements data diagnostics event
   for the driver if any required field is missing at the time of
   recording.

.. _4.6.1.6:

4.6.1.6 Monitoring Records Logged under the Unidentified Driver Profile
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. When there are ELD records involving driving time logged on an ELD
   under the unidentified driver profile, the ELD must prompt the
   driver(s) logging in with a warning indicating the existence of new
   unassigned driving time.
2. The ELD must provide a mechanism for the driver to review and either
   acknowledge the assignment of one or more of the unidentified driver
   records attributable to the driver under the authenticated driver’s
   profile as described in paragraph :ref:`4.3.2.8.2`
   (2)(1) of this appendix or indicate that these records are not
   attributable to the driver.
3. If more than 30 minutes of driving in a 24-hour period show
   unidentified driver on the ELD, the ELD must detect and record an
   unidentified driving records data diagnostic event and the data
   diagnostic indicator must be turned on for all drivers logged in to
   that ELD for the current 24-hour period and the following 7 days.
4. An unidentified driving records data diagnostic event can be cleared
   by the ELD when driving time logged under the unidentified driver
   profile for the current 24-hour period and the previous 7 consecutive
   days drops to 15 minutes or less.

.. _4.6.1.7:

4.6.1.7 Data Transfer Compliance Monitoring
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. An ELD must implement in-service monitoring functions to verify that
   the data transfer mechanism(s) described in section
   :ref:`4.9.1` of this appendix are continuing to function
   properly. An ELD must verify this functionality at least once every 7
   days. These monitoring functions may be automatic or may involve
   manual steps for a driver.
2. If the monitoring mechanism fails to confirm proper in-service
   operation of the data transfer mechanism(s), an ELD must record a
   data transfer data diagnostic event and enter an unconfirmed data
   transfer mode.
3. After an ELD records a data transfer data diagnostic event, the ELD
   must increase the frequency of the monitoring function to check at
   least once every 24-hour period. If the ELD stays in the unconfirmed
   data transfer mode following the next three consecutive monitoring
   checks, the ELD must detect a data transfer compliance malfunction.

.. _4.6.1.8:

4.6.1.8 Other Technology-Specific Operational Health Monitoring
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In addition to the required monitoring schemes described in sections
:ref:`4.6.1.1` – :ref:`4.6.1.7` of this
appendix, the ELD provider may implement additional, technology specific
malfunction and data diagnostic detection schemes and may use the ELD’s
malfunction status indicator and data diagnostic status indicator
(described in sections :ref:`4.6.2.1` and :ref:`4.6.3.1`) to communicate the ELD’s malfunction or
non-compliant state to the operator(s) of the ELD.

.. _4.6.2:

4.6.2 ELD Malfunction Status Indicator
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ELD malfunctions affect the integrity of the device and its compliance;
therefore, active malfunctions must be indicated to all drivers who may
use that ELD. An ELD must provide a recognizable visual indicator, and
may provide an audible signal, to the operator as to its malfunction
status.

.. _4.6.2.1:

4.6.2.1 Visual Malfunction Indicator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. An ELD must display a single visual malfunction indicator for all drivers using the ELD on the ELD’s display or on a stand-alone indicator. The visual signal must be visible to the driver when the driver is seated in the normal driving position. 2. The ELD malfunction indicator must be clearly illuminated when there is an active malfunction on the ELD. 3. The malfunction status must be continuously communicated to the driver when the ELD is powered.

.. _4.6.3:

4.6.3 ELD Data Diagnostic Status Indicator
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ELD data diagnostic status affects only the authenticated user;
therefore, an ELD must only indicate the active data diagnostics status
applicable to the driver logged into the ELD. An ELD must provide a
recognizable visual indicator, and may provide an audible signal, to the
driver as to its data diagnostics status.

.. _4.6.3.1:

4.6.3.1 Visual Data Diagnostics Indicator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. An ELD must display a single visual data diagnostics indicator, apart
   from the visual malfunction indicator described in section
   :ref:`4.6.2.1` of this appendix, to communicate visually
   the existence of active data diagnostics events for the applicable
   driver.
2. The visual signal must be visible to the driver when the driver is
   seated in the normal driving position.


.. _4.7:

4.7 Special Purpose ELD Functions
---------------------------------

.. _4.7.1:

4.7.1 Driver’s ELD Volume Control
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. If a driver selects the sleeper-berth state for the driver’s record
   of duty status and no co-driver has logged into the ELD as on-duty
   driving, and if the ELD outputs audible signals, the ELD must either:

   -  Allow the driver to mute the ELD’s volume or turn off the ELD’s
      audible output, or
   -  Automatically mute the ELD’s volume or turn off the ELD’s audible
      output.

2. For purposes of this section, if an ELD operates in combination with
   another device or other hardware or software technology that is not
   separate from the ELD, the volume controls required herein apply to
   the combined device or technology.

.. _4.7.2:

4.7.2 Driver’s Access to Own ELD Records
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ELD must provide a mechanism for a driver to obtain a copy of the
driver’s own ELD records on demand, in either an electronic or printout
format compliant with inspection standards outlined in section
:ref:`4.8.2.1` of this appendix.

1. The process must not require a driver to go through the motor carrier
   to obtain copies of the driver’s own ELD records if driver’s records
   reside on or are accessible directly by the ELD unit used by the
   driver.
2. If an ELD meets the requirements of this section by making data files
   available to the driver, it must also provide a utility function for
   the driver to display the data on a computer, at a minimum, as
   specified in § 395.8(g).

.. _4.7.3:

4.7.3 Privacy Preserving Provision for Use During Personal Uses of a CMV
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. An ELD must record the events listed in section
   :ref:`4.5.1` of this appendix under all circumstances.
   However, when a driver indicates that the driver is temporarily using
   the CMV for an authorized personal purpose, a subset of the recorded
   elements must either be omitted in the records or recorded at a lower
   precision level, as described in further detail below. The driver
   indicates this intent by setting the driver’s duty status to
   off-duty, as described in section :ref:`4.3.2.2.1`, and
   indicating authorized personal use of CMV as described in section
   :ref:`4.3.2.2.2`.
2. During a period when a driver indicates authorized personal use of
   CMV, the ELD must:

   -  Record all new ELD events with latitude/longitude coordinates
      information rounded to a single decimal place resolution; and
   -  Omit recording vehicle miles and engine hours fields in new ELD
      logs by leaving them blank, except for events corresponding to a
      CMV’s engine power-up and shut-down activity as described in
      section :ref:`4.5.1.6` of this appendix.

3. A driver’s indication that the CMV is being operated for authorized
   personal purposes may span more than one CMV ignition on cycle if the
   driver proactively confirms continuation of the personal use
   condition prior to placing the vehicle in motion when the ELD prompts
   the driver at the beginning of the new ignition power on cycle.


.. _4.8:

4.8 Printout or Display
-----------------------

The ELD must be able to generate a compliant report as specified in this
section, either as a printout or on a display.

.. _4.8.1.1:

4.8.1.1 Print Paper Requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Print paper must be able to accommodate the graph grid specifications as
listed in section :ref:`4.8.1.3` of this appendix.

.. _4.8.1.2:

4.8.1.2 Display Requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  (a) This section does not apply if an ELD produces a printout for use
       at a roadside inspection.

- (b) An ELD must be designed so that its display may be reasonably
       viewed by an authorized safety official without entering the
       commercial motor vehicle. For example, the display may be
       untethered from its mount or connected in a manner that would
       allow it to be passed outside of the vehicle for a reasonable
       distance.

.. _4.8.1.3:

4.8.1.3 Information To Be Shown on the Printout and Display at Roadside
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  (a)The printout and display must show reports for the inspected
   driver’s profile and the unidentified driver profile separately. If
   there are no unidentified driver records existing on the ELD for the
   current 24-hour period and for any of the previous 7 98 consecutive
   days, an ELD does not need to print or display unidentified driver
   records for the authorized safety official. Otherwise, both reports
   must be printed or displayed and provided to the authorized safety
   official.

-  (b)The printout and display must show the following information for
   the current 24-hour period and each of the previous 7 consecutive
   days: (Items in < . > are data elements.) 


:Date: <Date {of Record}>
:24-hour: Starting Time, Time Zone Offset from UTC: <24-Hour Period Starting Time>, <Time Zone Offset from UTC>
:Carrier: <Carrier's USDOT number>,<Carrier Name>
:Driver Name: <{Driver} Last Name>, <{Driver} First Name>
:Driver ID: <ELD username{for the driver}>
:Driver License State: <{Driver} Driver License Issuing State>
:Driver License Number: <{Driver} Driver License Number>
:Co-Driver: <{Co-Driver’s} Last Name>, <{Co-Driver’s} First Name>
:Co-Driver ID: < ELD username{for the co-driver}>
:Current Odometer: <{Current}{Total} Vehicle Miles>
:Current Engine Hours: <{Current}{Total} Engine Hours>
:ELD ID: <ELD Registration ID>
:ELD Provider: <Provider>
:Truck Tractor ID: <CMV Power Unit Number>
:Truck Tractor VIN: <CMV VIN>
:Shipping ID: <Shipping Document Number>
:Current Location: <{Current} Geo-location>
:Unidentified Driving Records: <{Current} Data Diagnostic Event Indicator Status {for “Unidentified driving records data diagnostic” event}>
:Exempt Driver Status: <Exempt Driver Configuration {for the Driver}>
:ELD Malfunction Indicators: <Malfunction Indicator Status {and Malfunction Description} {for ELD}>
:Driver’s Data Diagnostic Status: <Data Diagnostic Event Status {and Diagnostic Description}{for Driver}>
:Date: <Date {of Printout or Display}>

**Change of Duty Status, Intervening Interval Records and Change in Driver’s Indication of Special Driving Conditions:**

	<Event Record Status>,<Event Record Origin>,<Event Type>,<{Event} Date>,
	<{Event} Time>,<{Accumulated} Vehicle Miles>,<{Elapsed} Engine Hours>,<Geo-
	Location>#,<{Event} Comment/Annotation>

	<Event Sequence ID Number>,<Event Record Status>,<Event Record Origin>,<Event
	Type>,<Event Code>,<{Event} Date>,<{Event} Time>,<{Accumulated} Vehicle
	Miles>,<{Elapsed} Engine Hours>,<Geo-Location>#,<{Event} Comment/Annotation>
	# “<Geo-location> must be substituted with “<Driver’s Location Description>” field for
	manual entries and with “<{blank}>” field for intervening logs.


**Example of Print/Display Daily Header**

	.. figure:: media/image2.png
	   :alt:


**24 Hours [Print/Display Graph Grid]**

:Total hours: <Total Hours {in working day so far}>
:Off duty: <Total Hours {logged in Off-duty status}>
:Sleeper Berth: <Total Hours {logged in Sleeper berth status}>
:Driving: <Total Hours {logged in Driving status}>
:On duty not driving: <Total Hours {logged in on-duty not driving status}>
:Miles Today: < Vehicle Miles {Driven Today}>	   


**Example of Print/Display 24 Hours Duty Status Grid**

	.. figure:: media/image3.png
	   :alt:

[For Each Row of Driver’s Record Certification Events]
Time: <{Event} Time>
Location: <Geo-Location>#
101
Odometer: <{Total} Vehicle Miles>
Engine Hours: <{Total} Engine Hours>
Event:<Date {of the certified record}>
Origin: Driver
Comment: <{Event} Comment/Annotation>

[For Each Row of Malfunctions and Data Diagnostic Events]
Time: <{Event} Time>
Location: <Geo-Location>#
Odometer: < {Total}Vehicle Miles>
Engine Hours: <{Total}Engine Hours>
Event:<Event Type>
Origin: <Event Record Origin>
Comment: <{Event} Comment/Annotation>

[For Each Row of ELD Login/Logout Events]
Time: <{Event} Time>
Location: <Geo-Location>#
Odometer: < {Total}Vehicle Miles>
Engine Hours: <{Total}Engine Hours>
Event:<Event Type>
Origin: <ELD username>
Comment: <{Event} Comment/Annotation>

[For Each Row of CMV Engine Power up / Shut Down Events]
Time: <{Event} Time> (24 hours)
Location: <Geo-Location>#
Odometer: < {Total}Vehicle Miles>
Engine Hours: <{Total}Engine Hours>
Event:<Event Type>
Origin: Auto
Comment/Annotation>
1Printout report must only list up to 10 most recent ELD malfunctions and up to 10 most
recent data diagnostics events within the time period for which the report is generated.


**Example of Print/Display detail log data**

	.. figure:: media/image4.png
	   :alt:


**Example of Full Day ELD Record:**

	.. figure:: media/image5.png
	   :alt:

(c) The printout and display must show a graph-grid consistent with § 395.8(g)
showing each change of duty status.

	(1) On the printout, the graph-grid for each day’s RODS must be at least 6 inches
	by 1.5 inches in size.

	(2) The graph-grid must overlay periods of driver’s indications of authorized
	personal use of CMV and yard moves using a different style line (such as dashed or dotted
	line) or shading. The appropriate abbreviation must also be indicated on the graph-grid.

.. _4.8.2:

4.8.2 ELD Data File
~~~~~~~~~~~~~~~~~~~

An ELD must have the capability to generate a consistent electronic file
output compliant with the format described herein to facilitate the
transfer, processing, and standardized display of ELD data sets on the
authorized safety officials’ computing environments.


.. _4.8.2.1:

4.8.2.1 ELD Output File Standard
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  

   (a) Regardless of the particular database architecture used for
       recording the ELD events in electronic format, the ELD must
       produce a standard ELD data output file for transfer purposes,
       which must be generated according to the standard specified in
       this section.

-  

   (b) Data output must be provided in a single comma-delimited file
       outlined in this section using American National Standard Code
       for Information Exchange (ASCII) character sets meeting the
       standards of ANSI INCITS 4-1986 (R2012) (incorporated by
       reference, see § 395.38). It must include:

(1) A header segment, which specifies current or non-varying elements of
    an ELD file; and
(2) Variable length comma-delimited segments for the drivers, vehicles,
    ELD events, ELD malfunction and data diagnostics records, ELD login
    and logout activity, and unidentified driver records.
(3) Any field value that may contain a comma (“,”) or a carriage return
    () must be replaced with a semicolon (‘;’) before generating the
    compliant CSV output file 105

.. _4.8.2.1.1:

4.8.2.1.1 Header Segment
''''''''''''''''''''''''

This segment must include the following data elements and format: ELD
File Header Segment: <{Driver’s} Last Name>,<{Driver’s} First Name>,<
ELD username{for the driver} >,< {Driver’s} Driver's License Issuing
State>,<{Driver’s} Driver's License Number>, <{Co-Driver’s} Last
Name>,<{Co-Driver’s} First Name>,, ,,, ,,,<24-Hour Period Starting
Time>,, ,, <{Current} Date>,< {Current} Time>,< {Current}
Latitude>,<{Current} Longitude>,< {Current} {Total} Vehicle Miles>,<
{Current} {Total} Engine Hours>, ,,,

.. raw:: html

   <Output File
   Comment>

,

.. _4.8.2.1.2:

4.8.2.1.2 User List
'''''''''''''''''''

This segment must list all drivers and co-drivers with driving time
records on the most recent CMV operated by the inspected driver and
motor carrier’s support personnel who requested edits within the time
period for which this file is generated. The list must 106 be in
chronological order with most recent user of the ELD on top, and include
the driver being inspected, the co-driver, and the unidentified driver
profile. This segment has a variable number of rows depending on the
number of profiles with activity over the time period for which this
file is generated. This section must start with the following title:
User List: Each subsequent row must have the following data elements:
<{Assigned User} Order Number>,<{User’s} ELD Account Type>,<{User’s}
Last Name>,<{User’s} First Name>,

.. _4.8.2.1.3:

4.8.2.1.3 CMV List
''''''''''''''''''

This segment must list each CMV that the current driver operated and
that has been recorded on the driver’s ELD records within the time
period for which this file is generated. The list must be rank ordered
in accordance with the time of CMV operation with the most recent CMV
being on top. This segment has a variable number of rows depending on
the number of CMVs operated by the driver over the time period for which
this file is generated. This section must start with the following
title: CMV List: Each subsequent row must have the following data
elements: <{Assigned CMV} Order Number>,,,

.. _4.8.2.1.4:

4.8.2.1.4 ELD Event List for Driver’s Record of Duty Status
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

This segment must list ELD event records tagged with event types 1 (a
change in duty status as described in section :ref:`4.5.1.1` of this appendix),
2 (an intermediate log as described in section 4.5.1.2), and 3 (a change
in driver’s indication of conditions 107 impacting driving time
recording as described in section 4.5.1.3). The segment must list all
event record status types and all event record origins for the driver,
rank ordered with the most current log on top in accordance with the
date and time fields of the record. This segment has a variable number
of rows depending on the number of ELD events recorded for the driver
over the time period for which this file is generated. This section must
start with the following title: ELD Event List: Each subsequent row must
have the following data elements: ,,,, ,<{Event} Date>,<{Event} Time>,<
{Accumulated} Vehicle Miles>,< {Elapsed} Engine Hours>,<{Event}
Latitude>,<{Event} Longitude>,, <{Corresponding CMV} Order Number>,<{
User} Order Number {for Record Originator}>,,,,

.. _4.8.2.1.5:

4.8.2.1.5 Event Annotations, Comments, and Driver’s Location
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Description This segment must list only the elements of the ELD event
list created in section :ref:`4.8.2.1.4` of this appendix that have an annotation, comment, or a manual entry of location description by the driver. This segment has a variable number of rows depending on the number of ELD events under section :ref:`4.8.2.1.4` that feature a comment, annotation, or manual location entry by the driver. This section must start with the following title: ELD Event Annotations or Comments: Each subsequent row must have the following data elements: 108 ,< ELD username {of the Record Originator} >,<{Event} Comment Text or Annotation>,<{Event} Date>,<{Event} Time>, ,

.. _4.8.2.1.6:

4.8.2.1.6 ELD Event List for Driver’s Certification of Own Records
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

This segment must list ELD event records with event type 4 (driver’s
certification of own records as described in section :ref:`4.5.1.4` of this
appendix) for the inspected driver for the time period for which this
file is generated. It must be rank ordered with the most current record
on top. This segment has a variable number of rows depending on the
number of certification and re-certification actions the authenticated
driver may have executed on the ELD over the time period for which this
file is generated. This section must start with the following title:
Driver’s Certification/Recertification Actions: Each subsequent row must
have the following data elements: ,,<{Event} Date>,<{Event}
Time>,,<{Corresponding CMV} Order Number>,

.. _4.8.2.1.7:

4.8.2.1.7 Malfunction and Diagnostic Event Records
''''''''''''''''''''''''''''''''''''''''''''''''''

This segment must list all malfunctions that have occurred on this ELD
during the time period for which this file is generated. It must list
diagnostic event records related to the driver being inspected, rank
ordered with the most current record on top. This segment has a variable
number of rows depending on the number of ELD malfunctions and ELD
diagnostic event records recorded and relevant to the inspected driver
over the time period for which this file is generated. This section must
start with the following title: 109 Malfunctions and Data Diagnostic
Events: Each subsequent row must have the following data elements:
,,<Malfunction/Diagnostic Code>,<{Event} Date>,<{Event} Time>,<{Total}
Vehicle Miles>,<{Total} Engine Hours> ,<{Corresponding CMV} Order
Number>,

.. _4.8.2.1.8:

4.8.2.1.8 ELD Login/Logout Report
'''''''''''''''''''''''''''''''''

This segment must list the login and logout activity on the ELD (ELD
events with event type 5 (A driver’s login/logout activity)) for the
inspected driver for the time period for which this file is generated.
It must be rank ordered with the most recent activity on top. This
section must start with the following title: ELD Login/Logout Report:
Each subsequent row must have the following data elements: ,,,<{Event}
Date>,<{Event} Time>,<{Total} Vehicle Miles>,<{Total} Engine Hours>,

.. _4.8.2.1.9:

4.8.2.1.9 CMV’s Engine Power-Up and Shut Down Activity
''''''''''''''''''''''''''''''''''''''''''''''''''''''

This segment must list the logs created when a CMV’s engine is powered
up and shut down (ELD events with event type 6 (CMV’s engine power
up/shut down)) for the time period for which this file is generated. It
must be rank ordered with the latest activity on top. This section must
start with the following title: CMV Engine Power-Up and Shut Down
Activity: Each subsequent row must have the following data elements:
,,<{Event} Date>,<{Event} 110 Time>,<{Total} Vehicle Miles>,<{Total}
Engine Hours>,<{Event} Latitude>,<{Event} Longitude>,,,,,

.. _4.8.2.1.10:

4.8.2.1.10 ELD Event Log List for the Unidentified Driver Profile
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

This segment must list the ELD event records for the Unidentified Driver
profile, rank ordered with most current log on top in accordance with
the date and time fields of the logs. This segment has a variable number
of rows depending on the number of Unidentified Driver ELD records
recorded over the time period for which this file is generated. This
section must start with the following title: Unidentified Driver Profile
Records: Each subsequent row must have the following data elements:
,,,,,<{Event} Date>,<{Event} Time>,< {Accumulated} Vehicle Miles>,<
{Elapsed} Engine Hours>,<{Event} Latitude>,<{Event} Longitude>,,
<{Corresponding CMV} Order Number>,,,

.. _4.8.2.1.11:

4.8.2.1.11 File Data Check Value
''''''''''''''''''''''''''''''''

This segment lists the file data check value as specified in section :ref:`4.4.5.3` of this appendix. This part includes a single line as follows: End of File: 111

.. _4.8.2.2:

4.8.2.2 ELD Output File Name Standard
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If the ELD output is saved in a file for transfer or maintenance
purposes, it must follow the 25 character-long filename standard below:

-  (a)The first five position characters of the filename must correspond
   to the first five letters of the last name of the driver for whom the
   file is compiled. If the last name of the driver is shorter than five
   characters, remaining positions must use the character “\ *”
   [underscore] as a substitute character. For example, if the last name
   of the driver is “Lee”, the first five characters of the output file
   must feature “Lee* \_”.

-  (b)The sixth and seventh position characters of the filename must
   correspond to the last two digits of the driver’s license number for
   the driver for whom the file is compiled.

-  (c)The eighth and ninth position characters of the filename must
   correspond to the sum of all individual numeric digits in the
   driver’s license number for the driver for whom the file is compiled.
   The result must be represented in two-digit format. If the sum value
   exceeds 99, use the last two digits of the result. For example, if
   the result equals “113”, use “13”. If the result is less than 10, use
   0 as the first digit. For example, if the result equals “5”, use
   “05”.

-  (d)The tenth through fifteenth position characters of the filename
   must correspond to the date the file is created. The result must be
   represented in six digit format “MMDDYY” where “MM” represents the
   month, “”DD” represents the day, and “YY” represents the last two
   digits of the year. For example, February 5, 2013, must be
   represented as “020513”.

-  (e)The sixteenth position character of the filename must be a hyphen
   “-”. 112

-  

   (f) The seventeenth through twenty-fifth position characters of the
       filename must, by default, be “000000000” but each of these nine
       digits can be freely configured by the motor carrier or the ELD
       provider to be a number between 0 and 9 or a character between A
       and Z to be able to produce distinct files—if or when
       necessary—that may otherwise be identical in filename as per the
       convention proposed in this section. ELD providers or motor
       carriers do not need to disclose details of conventions they may
       use for configuring the seventeenth through twenty-fifth digits
       of the filename.

.. _4.9:

4.9 Data Transfer Capability Requirements
-----------------------------------------

An ELD must be able to present the captured ELD records of a driver in
the standard electronic format as described below, and transfer the data
file to an authorized safety official, on demand, for inspection
purposes.

.. _4.9.1:

4.9.1 Data Transfer During Roadside Safety Inspections
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  (a)On demand during a roadside safety inspection, an ELD must produce
   ELD records for the current 24-hour period and the previous 7
   consecutive days in electronic format, in the standard data format
   described in section :ref:`4.8.2.1` of this appendix.

-  (b)When a driver uses the single-step driver interface, as described
   in section :ref:`4.3.2.4` of this appendix, to indicate that the ELD compile and transfer the driver’s ELD records to authorized safety officials, the ELD must transfer the generated ELD data output to the computing environment used by authorized safety officials via the standards referenced in this section. To meet roadside electronic data transfer requirements, an ELD must do at least one of the following: (1) Option 1—Telematics transfer methods. Transfer the electronic data using both:

-- (i) Wireless Web services, and 113 (ii) Email, or

-- (2) Option 2—Local transfer methods. Transfer the electronic data using
    both:

--  (i) USB2 (incorporated by reference, see § 395.38), and

-- (ii) Bluetooth (incorporated by reference, see § 395.38).

-  (c)The ELD must provide an ELD record for the current 24-hour period
   and the previous 7 consecutive days as described in section 4.8.1.3
   either on a display or on a printout.

-  (d)An ELD must support one of the two options for roadside data
   transfer in paragraph
-  (b) of this section, and must certify proper operation of each element under that option. An authorized safety official will specify which transfer mechanism the official will use within the certified transfer mechanisms of an ELD.

.. _4.9.2:

4.9.2 Motor Carrier Data Reporting
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  (a)An ELD must be capable of retaining copies of electronic ELD
   records for a period of at least 6 months from the date of receipt.

-  (b)An ELD must produce, on demand, a data file or a series of data
   files of ELD records for a subset of its drivers, a subset of its
   vehicles, and for a subset of the 6- month record retention period,
   to be specified by an authorized safety official, in an electronic
   format standard described in section :ref:`4.8.2.1` of this appendix or, if
   the motor carrier has multiple offices or terminals, within the time
   permitted under § 390.29.

(1) At a minimum, an ELD must be able to transfer the ELD records
    electronically by one of the following transfer mechanisms:
(2) Web Services as specified in section :ref:`4.10.1.1` of this appendix (but not necessarily wirelessly), and E-mail as specified :ref:`4.10.1.2` (but not necessarily wirelessly); or 114 (3)USB 2.0 as specified in section :ref:`4.10.1.3` of this appendix and Bluetooth, as specified in section :ref:`4.10.1.4` (both incorporated by reference, see § 395.38).

.. _4.10:

4.10 Communications Standards for the Transmittal of Data Files
---------------------------------------------------------------

from ELDs ELDs must transmit ELD records electronically in accordance
with the file format specified in section :ref:`4.8.2.1` of this appendix and
must be capable of a one-way transfer of these records to authorized
safety officials upon request as specified in section 4.9.

.. _4.10.1:

4.10.1 Data Transfer Mechanisms
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For each type of data transfer mechanism, an ELD must follow the
specifications in this section.

.. _4.10.1.1:

4.10.1.1 Wireless Data Transfer via Web Services
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

(a) Transfer of ELD data to FMCSA via Web Services must follow the following standards: (1)Web Services Description Language (WSDL) 1.1 (2)Simple Object Access Protocol (SOAP) 1.2 (incorporated by reference, see § 395.38) (3)Extensible Markup Language (XML) 1.0 5th Edition

(b) If an ELD provider plans to use Web Services, upon ELD provider    registration as described in section 5.1 of this appendix,

(1) FMCSA will provide formatting files necessary to convert the ELD file into an XML format and upload the data to the FMCSA servers. These files include FMCSA’s Rules of Behavior, XML Schema, WSDL file, Interface Control Document (ICD), and the ELD Web Services Development Handbook, and 115 (2)ELD Providers must obtain a Public/Private Key pair compliant with the NIST SP 800-32, Introduction to Public Key Technology and the Federal PKI Infrastructure, (incorporated by reference, see § 395.38), and submit the public key with their registration. (3)ELD Providers will be required to complete a test procedure to ensure their data is properly formatted before they can begin submitting driver’s ELD data to the FMCSA server.

(c)ELD data transmission must be accomplished in a way that protects
   the privacy of the driver(s).

(d)At roadside, if both the vehicle operator and law enforcement have
   an available data connection, the vehicle operator will initiate the
   transfer of ELD data to an authorized safety official. In some cases,
   an ELD may be capable of converting the ELD file to an XML format
   using an FMCSA-provided schema and upload it using information
   provided in the WSDL file using SOAP via RFC 7230, RFC 7231, and RFC
   5246, Transport Layer Security (TLS) Protocol Version 1.2
   (incorporated by reference, see § 395.38).

.. _4.10.1.2:

4.10.1.2 Wireless Data Transfer Through E-Mail
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  (a) The ELD must attach a file to an email message to be sent using    RFC 5321 Simple Mail Transfer Protocol (SMTP) (incorporated by    reference, see § 395.38), to a specific email address, which will    be shared with the ELD providers during the technology    registration process.

-  (b)The file must have the format described in section :ref:`4.8.2.1` of this
   appendix and must be encrypted using the Secure/Multipurpose Internet
   Mail Extensions as described in RFC 5751 (incorporated by reference,
   see § 395.38), and the RSA algorithm as 116 described in RFC 4056
   (incorporated by reference, see § 395.38), with the FMCSA public key
   compliant with NIST SP 800-32 (incorporated by reference, see §
   395.38) to be provided to the ELD provider at the time of
   registration. The content must be encrypted using AES in FIPS
   Publication 197 (incorporated by reference, see § 395.38), and RFC
   3565 (incorporated by reference, see § 395.38).

-  (c)The email must be formatted using the RFC 5322 Internet Message
   Format (incorporated by reference, see § 395.38), as follows: 

	.. figure:: media/image6.png
	   :alt:

-  (d)A message confirming receipt of the ELD file will be sent to the
   address specified in the email. The filename must follow the
   convention specified in section :ref:`4.8.2.2` of this appendix.

.. _4.10.1.3:

4.10.1.3 Data Transfer via USB 2.0
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  (a) ELDs certified for the USB data transfer mechanism must be
       capable of transferring ELD records using the Universal Serial
       Bus Specification (Revision 2.0) (incorporated by reference, see
       § 395.38).

-  (b)Each ELD technology must implement a single USB-compliant
   interface with the necessary adaptors for a Type A connector. The USB
   interface must implement the 117 Mass Storage class (08h) for
   driverless operation, to comply with IEEE standard 1667- 2009,
   (incorporated by reference, see § 395.38).

-  (c)The ELD must be capable of providing power to a standard
   USB-compatible drive.

-  (d)An ELD must re-authenticate the driver prior to saving the
   driver’s ELD file to an external device.

-  (e)On initiation by an authenticated driver, an ELD must be capable
   of saving ELD file (s) to USB-compatible drives (AES, in FIPS Publication 197,
   incorporated by reference, see § 395.38) that are provided by
   authorized safety officials during an inspection. Prior to
   initiating this action, ELDs must be capable of reading a text
   file from an authorized safety officials’ drive and verifying it
   against a file provided to ELD providers who have registered
   their technologies as described in section 5.1 of this appendix.


.. _4.10.1.4:

4.10.1.4 Data Transfer via Bluetooth®
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  (a)Bluetooth SIG Specification of the Bluetooth System covering core
   package version 2.1 + EDR (incorporated by reference, see § 395.38)
   must be followed. ELDs using this standard must be capable of
   displaying a Personal Identification Number generated by the
   Bluetooth application profile for bonding with other devices
   (incorporated by reference, see § 395.38).

-  (b)Upon request of an authorized official, the ELD must become
   discoverable by the authorized safety officials’ Bluetooth-enabled
   computing platform, and generate a random code, which the driver must
   share with the official (incorporated by reference, see § 395.38).
   118

-  

   (c) The ELD must connect to the roadside authorized safety officials’
       technology via wireless personal area network and transmit the
       required data via Web Services as described in section 4.10.1.1
       of this appendix.

.. _4.10.2:

4.10.2 Motor Carrier Data Transmission
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Regardless of the roadside transmission option supported by an ELD, ELD
records are to be retained and must be able to transmit
enforcement-specified historical data for their drivers using one of the
methods specified under section 4.9.2 of this appendix.

-  (a)Web services option must follow the specifications described under
   section :ref:`4.10.1.1` of this appendix.

-  (b)The email option must follow the specifications described under
   section :ref:`4.10.1.2` of this appendix.

-  (c)The USB option must follow the specifications of Universal Serial
   Bus Specification, revision 2.0 (incorporated by reference, see §
   395.38) and described in section :ref:`4.10.1.3` of this appendix.

-  (d)Bluetooth must follow the specifications incorporated by reference
   (see § 395.38) and described in section :ref:`4.10.1.4` of this appendix.