5. ELD REGISTRATION AND CERTIFICATION
=====================================

As described in § 395.22(a) of this part, motor carriers must only use ELDs
that are listed on the FMCSA Web site. An ELD provider must register with FMCSA
and certify each ELD model and version for that ELD to be listed on this Web site.
Appendix C- ELD Functional Requirements (Sections 5-7)
APPENDIX TO SUBPART B OF PART 395—FUNCTIONAL SPECIFICATIONS FOR ALL
ELECTRONIC LOGGING DEVICES (ELDS)

5.1. ELD Provider’s Registration
--------------------------------

5.1.1. Registering Online
^^^^^^^^^^^^^^^^^^^^^^^^^

(a)An ELD provider developing an ELD technology must register online at a
secure FMCSA Web site where the ELD provider can securely certify that its ELD
is compliant with this appendix.
(b)Provider’s registration must include the following information:
(1)Company name of the technology provider/manufacturer.
(2)Name of an individual authorized by the provider to verify that the ELD is
compliant with this appendix and to certify it under section 5.2 of this appendix.
(3)Address of the registrant.
(4)E-mail address of the registrant.
(5)Telephone number of the registrant.

5.1.2 Keeping Information Current
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ELD provider must keep the information in section 5.1.1(b) of this appendix
current through FMCSA’s Web site. 

5.1.3 Authentication Information Distribution
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
FMCSA will provide a unique ELD registration ID, authentication key(s),
authentication file(s), and formatting and configuration details required in this
appendix to registered providers during the registration process.

5.2 Certification of Conformity with FMCSA Standards
--------------------------------------------------------

A registered ELD provider must certify that each ELD model and version has
been sufficiently tested to meet the functional requirements included in this
appendix under the conditions in which the ELD would be used.

5.2.1 Online Certification
^^^^^^^^^^^^^^^^^^^^^^^^^^

(a) An ELD provider registered online as described in section 5.1.1 of this
appendix must disclose the information in paragraph (b) of this section about each ELD
model and version and certify that the particular ELD is compliant with the requirements
of this appendix.
(b) The online process will only allow a provider to complete certification if
the provider successfully discloses all of the following required information:

	(1) Name of the product.
	(2) Model number of the product.
	(3) Software version of the product.
	(4) An ELD identifier, uniquely identifying the certified model and version of the ELD, assigned by the ELD provider in accordance with section 7.15 of this appendix.
	(5) Picture and/or screen shot of the product.
	(6) User’s manual describing how to operate the ELD.
	(7) Description of the supported and certified data transfer mechanisms and stepby-step instructions for a driver to produce and transfer the ELD records to an authorized safety official.
	(8) Summary description of ELD malfunctions.
	(9) Procedure to validate an ELD authentication value as described in section 7.14 of this appendix.
	(10) Certifying statement describing how the product was tested to comply with FMCSA regulations.

5.2.2 Procedure to Validate an ELD’s Authenticity
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Paragraph 5.2.1(b)(9) of this appendix requires that the ELD provider identify its
authentication process and disclose necessary details for FMCSA systems to
independently verify the ELD authentication values included in the dataset of inspected
ELD outputs. The authentication value must include a hash component that only uses
data elements included in the ELD dataset and data file. ELD authentication value must
meet the requirements specified in section 7.14 of this appendix.

5.3 Publicly Available Information
---------------------------------------

Except for the information listed under paragraphs 5.1.1(b)(2), (4), and (5) and
5.2.1(b)(9) of this appendix, FMCSA will make the information in sections 5.1.1 and
5.2.1 for each certified ELD publicly available on a Web site to allow motor carriers to
determine which products have been properly registered and certified as ELDs compliant
with this appendix.

5.4 Removal of Listed Certification
--------------------------------------


5.4.1 Removal Process
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

FMCSA may remove an ELD model or version from the list of ELDs on the
FMCSA Website in accordance with this section. 


5.4.2 Notice
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

FMCSA shall initiate the removal of an ELD model or version from the list of
ELDs on the FMCSA Web site by providing the ELD provider written notice stating:
(a) The reasons FMCSA proposes to remove the model or version from the
FMCSA list; and
(b) Any corrective action that the ELD provider must take for the ELD model or
version to remain on the list.

5.4.3 Response
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An ELD provider that receives notice under section 5.4.2 of this appendix may
submit a response to the Director, Office of Carrier Driver, and Vehicle Safety Standards
no later than 30 days after issuance of the notice of proposed removal, explaining:

	(a) The reasons why the ELD provider believes the facts relied on by the Agency,
	in proposing removal, are wrong; or
	(b) The action the ELD provider will take to correct the deficiencies that FMCSA
	identified.

5.4.4 Agency Action
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

(a) If the ELD provider fails to respond within 30 days of the date of the notice
issued under section 5.4.2 of this appendix, the ELD model or version shall be
removed from the FMCSA list.
(b) If the ELD provider submits a timely response, the Director, Office of Carrier,
Driver, and Vehicle Safety Standards, shall review the response and withdraw the notice
of proposed removal, modify the notice of proposed removal, or affirm the notice of
proposed removal, and notify the ELD provider in writing of the determination.
(c) Within 60 days of the determination, the ELD provider shall take any action required to comply. If the Director determines that the ELD provider failed to timely take the required action within the 60 day period, the ELD model or version shall be removed from the FMCSA list.
(d) The Director, Office of Carrier, Driver, and Vehicle Safety Standards may request from the ELD provider any information that the Director considers necessary to make a determination under this section.

5.4.5 Administrative Review
---------------------------
	(a) Within 30 days of removal of an ELD model or version from the FMCSA list
	of certified ELDs under section 5.4.4 of this appendix, the ELD provider may request
	administrative review.
	(b) A request for administrative review must be submitted in writing to the
	FMCSA Associate Administrator for Policy. The request must explain the error
	committed in removing the ELD model or version from the FMCSA list, identify all
	factual, legal, and procedural issues in dispute, and include any supporting information or
	documents.
	(c) The Associate Administrator may ask the ELD provider to submit additional
	information or attend a conference to discuss the removal. If the ELD provider does not
	submit the requested information or attend the scheduled conference, the Associate
	Administrator may dismiss the request for administrative review.
	(d) The Associate Administrator will complete the administrative review
	and notify the ELD provider of the decision in writing. The decision constitutes a
	final Agency action. 