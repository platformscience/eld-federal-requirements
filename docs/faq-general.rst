General FAQ's - 2017 DECEMBER
=============================

Content Pulled from: https://www.fmcsa.dot.gov/sites/fmcsa.dot.gov/files/docs/regulations/hours-service/elds/74541/eldrulefaqs-dec2017.pdf

General Information about the ELD Rule
--------------------------------------

1.   What are the key requirements of the Electronic Logging Device (ELD) rule?
*******************************************************************************
The ELD rule:
* Requires ELD use by commercial drivers who are required to prepare hours-of-service (HOS) records
of duty status (RODS).
* Sets ELD performance and design standards, and requires ELDs to be certified and registered
with FMCSA.
* Establishes what supporting documents drivers and carriers are required to keep.
* Prohibits harassment of drivers based on ELD data or connected technology (such as fleet
management system). The rule also provides recourse for drivers who believe they have
been harassed.


2.   What is the mandate in the Moving Ahead for Progress in the 21st Century Act (MAP-21) for the Electronic Logging Device (ELD) rule?
********************************************************************************************************************************************
Section 32301(b) of the Commercial Motor Vehicle Safety Enhancement Act, enacted as part of MAP-21,
(Pub. L. 112-141, 126 Stat. 405, 786-788, July 6, 2012), mandates the ELD rule. It calls for the Secretary
of Transportation to adopt regulations requiring ELD use in commercial motor vehicles (CMVs) involved in
interstate commerce, when operated by drivers who are required to keep records of duty status (RODS).

3.  Who must comply with the electronic logging device (ELD) rule?
******************************************************************
The ELD applies to most motor carriers and drivers who are currently required to maintain records of duty
status (RODS) per Part 395, 49 CFR 395.8(a). The rule applies to commercial buses as well as trucks, and to
Canada- and Mexico-domiciled drivers.
The ELD rule allows limited exceptions to the ELD mandate, including:
* Drivers who operate under the short-haul exceptions may continue using timecards; they are not
required to keep RODS and will not be required to use ELDs.
* Drivers who use paper RODS for not more than 8 days out of every 30-day period.
* Drivers who conduct drive-away-tow-away operations, in which the vehicle being driven is the
commodity being delivered.
* Drivers of vehicles manufactured before 2000.

4.  What electronic logging device (ELD) user documentation must be onboard a driver’s commercial motor vehicle?
****************************************************************************************************************
Beginning on December 18, 2017, a driver using an ELD must have an ELD information packet onboard the
commercial motor vehicle (CMV) containing the following items:

	1. A user’s manual for the driver describing how to operate the ELD;

	2. An instruction sheet describing the data transfer mechanisms supported by the ELD and step-by-step instructions to produce and transfer the driver’s hours-of-service records to an
	authorized safety official;

	3. An instruction sheet for the driver describing ELD malfunction reporting requirements and recordkeeping procedures during ELD malfunctions; and

	4. A supply of blank driver’s records of duty status (RODS) graph-grids sufficient to record the driver’s duty status and other related information for a minimum of 8 days.

Prior to December 18, 2017, FMCSA recommends that drivers have the user’s manual, malfunction instruction sheet, and graph-grids.

5.  Can the ELD information packet be in electronic form?
*********************************************************
Yes. The user’s manual, instruction sheet, and malfunction instruction sheet can be in electronic form. This
is in accordance with the federal register titled “Regulatory Guidance Concerning Electronic Signatures and
Documents” (76 FR 411).

6.  Can an electronic logging device (ELD) be on a smartphone or other wireless device?
***************************************************************************************
Yes. An ELD can be on a smartphone or other wireless device if the device meets the ELD rule’s
technical specifications.

7.  Can a driver use a portable electronic logging device (ELD)?
****************************************************************
Yes. A driver may use a portable ELD. A portable ELD must be mounted in a fixed position during
commercial motor vehicle (CMV) operation (CMV) and visible to the driver from a normal seated driving
position. This information can be found in the ELD Rule section 395.22(g).

8.  How long must a motor carrier retain electronic logging device (ELD) record of duty status (RODS) data?
***********************************************************************************************************
A motor carrier must retain ELD record of duty status (RODS) data and back-up data for six months.
The back-up copy of ELD records must be maintained on a device separate from that where original
data are stored. Additionally, a motor carrier must retain a driver’s ELD records in a manner that protects
driver privacy.

9.  What electronically transferred data from electronic logging devices (ELDs) will be retained by Federal Motor Carrier Safety Administration (FMCSA) and other authorized safety officials?
**********************************************************************************************************************************************************************************************
FMCSA will not retain any ELD data unless there is a violation.

10 . What is the carrier’s responsibility in ensuring that they are using a registered device?
**********************************************************************************************
The motor carrier is responsible for checking that their device is registered. This includes checking both the
registration and revocation list periodically. The list of registered and revoked ELDs can be found on the
following link: https://3pdp.fmcsa.dot.gov/ELD/ELDList.aspx.
In the event that an ELD is removed from the registration list, FMCSA will make efforts to notify the public
and affected users. Motor carriers and drivers are encouraged to sign-up for ELD updates to receive
notifications on when an ELD has been listed on the Revocation List.

11 . How will the ELD display screen or printout reflect special driving categories; yard moves and personal conveyance?
************************************************************************************************************************
While not required, if the motor carrier configured the driver user account to authorize a special driving
category, then the graph-grid will overlay periods using a different style line (such as dashed, dotted line, or
shading) in accordance with section 4.8.1.3(c)(1) of the ELD Functional Specifications. The appropriate
abbreviation must also be indicated on the graph-grid.
If the motor carrier does not configure the driver user account to authorize special driving categories, then
the driver must annotate the beginning and end of the applicable special driving category.

12 . If the vehicle registration for a commercial motor vehicle reflects a model year of 2000 or newer,
but the vehicle was manufactured without an engine control module (ECM), is the carrier required
to comply with the ELD rule?
****************************************************************************************************************************************************************************************************************************************************************
Yes, a motor carrier operating a vehicle with a manufactured model year of 2000 and newer and without an
ECM is subject to the ELD rule. If the currently installed engine does not support an ECM and is unable to
obtain or estimate the required vehicle parameters, then the operator must use an ELD that does not rely
on ECM connectivity, but nevertheless meets the accuracy requirements of the final rule. See Appendix A
to Subpart B of Part 395 sections 4.2 and 4.3.1 of the ELD rule for accuracy requirements.

13 . How are motor carriers required to present records of duty status (RODS) from December 11-17, 2017? Will drivers be required to present their records of duty status on electronic logging devices (ELDs)?
*******************************************************************************************************************************************************************************************************************
No, drivers will not be required to present RODS on ELDs from December 11, 2017, to December 17, 2017.
Drivers can present their previous 7 days of RODS through any of the following:
* Paper records,
* A printout from an electronic logging device,
* A display from a device installed with logging software and electronic signature capabilities,
* Having the records available by entering them into an ELD, or
* Continued use of a grandfathered automatic on-board recording device.

14 . How must a driver reflect their record of duty status for the previous 7 days during a roadside
inspection, if he or she is employed by multiple motor carriers that are using ELDs?
******************************************************************************************************************************************************************************************************
The driver can either, (1) print out their hours-of-service from the other motor carrier, (2) if operating with
compatible devices the ELD data can be transferred between the motor carriers with the driver’s approval,
or (3) manually add the hours of service while operating for that motor carrier into the current ELD using
the editing and annotation functions of the ELD.

15 . How must a driver who is starting to work for a new motor carrier present their prior 7 days’ records of duty status to the new carrier?
*************************************************************************************************************************************************
Section 395.8(j)(2) provides that “(2) Motor carriers, when using a driver for the first time or intermittently,
shall obtain from the driver a signed statement giving the total time on duty during the immediately
preceding 7 days and the time at which the driver was last relieved from duty prior to beginning work for
the motor carriers.” In the alternative, the driver may present copies of the prior 7 days’ records of duty
status or a print-out of the prior 7 days from the prior carrier’s ELD system.

Electronic Logging Device Exemptions
------------------------------------

1.  Who is exempt from the ELD rule?
************************************
Drivers who use the timecard exception are not required to keep records of duty status (RODS) or use
ELDs. Additionally, the following drivers are not required to use ELDs; however, they are still bound by the
RODS requirements in 49 CFR 395 and must prepare logs on paper, using an Automatic On-Board
Recording Device (AOBRD), or with a logging software program when required:
* Drivers who use paper RODS for not more than 8 days out of every 30-day period.
* Drivers of vehicles manufactured before 2000.
* Drivers who are required to keep RODS not more than 8 days within any 30-day period.
* Drivers who conduct drive-away-tow-away operations, where the vehicle being driven is the
commodity being delivered, or the vehicle being transported is a motor home or a recreation
vehicle trailer with one or more sets of wheels on the surface of the roadway.
* Drivers of vehicles manufactured before the model year 2000. (As reflected on the
vehicle registration)

2.  What time periods can be used to determine the 8 days in any 30-day period?
*******************************************************************************
The 30-day period is not restricted to a single month, but applies to any 30-day period. For example,
June 15 to July 15 is considered a 30-day period.

3.  What information may be requested to support the exemption for drivers not required to use records of duty status (RODS) more than 8 days in any 30-day period?
**********************************************************************************************************************************************************************
Authorized safety officials may inspect and copy motor carrier records and request any records needed to
perform their duties.

4.  If the vehicle registration for a commercial motor vehicle reflect a model year of 2000 or newer, but the engine plate or documentation from the manufacturer indicates that the engine is older than model year 2000, is the vehicle exempt from the ELD rule?
**********************************************************************************************************************************************************************************************************************************************************************
Yes. While an ELD may voluntarily be used in vehicles that are model year 1999 or older, use of an ELD is
not required in these vehicles; likewise, vehicles with engines predating model year 2000 are to be treated
as exempt, even if the VIN number reported on the registration indicates that the CMV is a later model
year. When a vehicle is registered, the model year should follow the criteria established by the National
Highway Traffic Safety Administration (NHTSA). There may be instances where the model year reflected on
the vehicle registration is not the same as the engine model year, most commonly when a vehicle is rebuilt
using a “glider kit.” In this circumstance, an inspector/investigator should use the model year on the engine
to determine if the driver is exempt from the ELD requirements. If the engine model year is older than
2000, the driver is not subject to the ELD rule. While the driver is not required to possess documentation that confirms the vehicle engine model year, 49 CFR Part 379 Appendix A requires motor carriers to maintain all documentation on motor and engine changes at the principle place of business. If a determination cannot be made at the roadside, safety official should refer the case for further investigation.

5.  If a motor carrier’s operation is exempt from the requirements of 49 CFR Part 395.8, is the motor carrier also exempt from the ELD rule?
********************************************************************************************************************************************
Yes. Motor carriers with operations that are exempt from the requirements of 395.8 are exempt from the
ELD rule.

6.  Are Canada- and Mexico-domiciled drivers required to use electronic logging devices (ELDs) when they are operating in the United States?
********************************************************************************************************************************************
Yes. Canada- and Mexico-domiciled drivers must comply with the Federal hours of service rules
while operating in the United States. This includes using ELDs complaint with 49 CFR Part 395, unless
they qualify for one of the exceptions. A driver operating in multiple jurisdictions will be able to annotate
the driver’s record of duty status on the ELD with information on periods of operation outside the
United States.

7.  How should an ELD record a driver’s hours of service when operating in another country such as Canada?
**********************************************************************************************************
The ELD provider may tailor the device to its customers’ needs/operations to assist them in accurately
monitoring drivers’ hours of service compliance in accordance with the hours of service standards of the
country operated in, such as cross-border operations.

8.  Can drivers operate commercial motor vehicles (CMVs) equipped with electronic logging devices (ELDs), if they are not required to use them due to an exception?
*******************************************************************************************************************************************************************
Yes. Drivers can drive CMVs equipped with ELDs and still use their exception. A motor carrier may
configure an ELD to show the exception for drivers exempt from using the ELD, or use the ELD annotation
to record the status.

9.  Are motor carriers that meet the agricultural exemption defined in 395.1(k) or the covered farm vehicle 395.1(s) subject to the ELD rule?
*********************************************************************************************************************************************
The ELD rule does not change any of the current hours of service exemptions. Therefore, motor carriers
that meet the exemptions defined in 395.1 are not subject to Part 395, including the ELD rule while
they are operating under the terms of the exemption. The duty status of the driver may be noted as
either off-duty (with appropriate annotation), or “exempt.” Click here for additional information on the
agriculture exemption.

10 . Can a driver use an ELD on a commercial motor vehicle with a model year older than 2000?
*********************************************************************************************
Yes. However, the ELD must comply with the ELD rule’s technical specifications. The ELD may use
alternative sources to obtain or estimate the required vehicle parameters, in accordance with the accuracy
requirements in Section 4.3.1 of the ELD rule.

11 . Are transporters of mobile or modular homes considered Driveaway/Towaway operations under Section 395.8 (a)(1)(iii)(A)(2) or (3) and therefore exempt from the ELD rule?
*********************************************************************************************************************************************************************************
No. The transportation of mobile or modular homes does not qualify for an exception under 395.8(a)(1)(iii)
(A)(2) because the vehicle driven in transporting the mobile or modular home is not part of the shipment,
nor does the transport qualify under 395.8(a)(1)(iii)(A)(3) because the shipment is neither a motor home or
recreational vehicle trailer.

Voluntary Usage and Compliance Phases
-------------------------------------

1.  How soon can electronic logging devices (ELDs) be installed and used in commercial motor vehicles (CMVs)?
*************************************************************************************************************
Since February 16, 2016, ELD manufacturers have been able to register and self-certify their ELDs with
FMCSA, and motor carriers have been able to elect to use ELDs listed on the website. All motor carriers
and drivers subject to the requirements in the ELD rule must begin using an ELD or “grandfathered
AOBRD” on December 18, 2017, the compliance date of the ELD rule.

2.  What are the enforcement procedures for registered electronic logging devices (ELDs) installed and used in commercial motor vehicles (CMVs) prior to the compliance date of the ELD rule?
************************************************************************************************************************************************************************************************
Prior to the compliance date of December 18, 2017, ELD use is voluntary. Safety officials will review ELD
information to determine compliance with the hours of service regulations and to detect falsifications.

3.  What are the options for carriers and drivers to complete records of duty status (RODS) prior to the Electronic Logging Device (ELD) rule compliance date?
***********************************************************************************************************************************************************************
Prior to December 18, 2017 drivers and motor carriers can use:
* Automatic onboard recording device (AOBRDs),
* ELDs,
* Paper logs or
* Devices with logging software programs.

4.  What is a “grandfathered” automatic onboard recording device (AOBRD)?
*************************************************************************
A “grandfathered” AOBRD is a device that a motor carrier installed and required its drivers to use before
the electronic logging device (ELD) rule compliance date of December 18, 2017. The device must meet the
requirements of 49 CFR 395.15. A motor carrier may continue to use grandfathered AOBRDs no later than
December 16, 2019. After that, the motor carrier and its drivers must use ELDs. See Section 395.15 (a) of
the ELD final rule.

5.  When are drivers subject to the ELD rule required to start using electronic logging devices (ELDs)?
*******************************************************************************************************
Motor carriers and drivers subject to the ELD rule must start using ELDs by the compliance date of
December 18, 2017, unless they are using a grandfathered Automatic On-board Recording Device (AOBRD).

6.  What will be the enforcement procedures for “grandfathered” automatic onboard recording devices (AOBRDs) and electronic logging devices (ELD) during the two-year period following the compliance date of the ELD rule?
****************************************************************************************************************************************************************************************************************************
During the period when both “grandfathered” AOBRDs and ELDs will be used (December 18, 2017 to
December 16, 2019), authorized safety officials will enforce ELD rule requirements for ELDs and the
requirements in 49 CFR 395.15 for “grandfathered” AOBRDs. The supporting document requirements
for drivers and motor carriers using either device will take effect on the ELD rule compliance date of
December 18, 2017.

7.  According to § 395.8, if a motor carrier “installs and requires a driver to use an AOBRD…before December 18, 2017 they may continue to use the AOBRD until December 16, 2019.” Does this mean I can move an AOBRD from one vehicle to another after December 18, 2017?
****************************************************************************************************************************************************************************************************************************************************************************
If your operation uses AOBRDs before December 18, 2017, and you replace vehicles in your fleet you can
install an AOBRD that was used in the previous CMV. However, you may not purchase and install a new
AOBRD in a vehicle after December 18, 2017.

Supporting Documents
--------------------

1.  When are motor carriers and drivers required to comply with the supporting document requirements?
*****************************************************************************************************
All motor carriers and drivers must comply with the supporting documents requirements starting
December 18, 2017.

2.  How many supporting documents must be retained by motor carriers, and when must drivers submit them to the motor carrier?
********************************************************************************************************************************
Motor carriers must retain up to eight supporting documents for every 24-hour period that a driver is
on duty. Drivers must submit their records of duty status (RODS) and supporting documents to the
motor carrier no later than 13 days after receiving them. If a motor carrier retains more than 8 supporting
documents, the motor carrier must maintain the first and last document generated during the regular
course of business

3.  How long must motor carriers retain records of duty status (RODS) and supporting documents?
***********************************************************************************************
Motor carriers must retain RODS and supporting documents for six months.

4.  What are the categories of supporting documents?
****************************************************
Supporting documents required in the normal course of business are important to verify a driver’s records
of duty status (RODS). They consist of five categories, described in 49 CFR 395.11(c):
* Bills of lading, itineraries, schedules, or equivalent documents that indicate the origin and destination
of each trip;
* Dispatch records, trip records, or equivalent documents;
* Expense receipts related to any on-duty not-driving time;
* Electronic mobile communication records, reflecting communications transmitted through a fleet
management system; and
* Payroll records, settlement sheets, or equivalent documents that indicate what and how a driver
was paid.
If a driver keeps paper RODS under 49 CFR 395.8(a)(1)(iii), the carrier must also retain toll receipts.
For drivers using paper RODS, toll receipts do not count toward the eight-document cap.

5.  Are there specific categories of supporting documents that drivers can provide electronically?
**************************************************************************************************
Two categories—electronic mobile communications and payroll records—are not documents a driver would
have to physically retain. They may be part of a larger record that the carrier retains electronically or
physically at the dispatch location or principal place of business. In applying the eight-document limit, all
information in an electronic mobile communication record will be counted as one document per duty day.

6.  Can supporting documents be limited to only those acquired at the beginning and end of the workday?
*******************************************************************************************************
No. Documents acquired throughout the day are important in enforcing the 60/70-hour rule—a crucial part
of ensuring hours of service compliance. Compliance with the 60/70-hour rule is based on the cumulative
hours an individual works over a period of days. Supporting documents are critical to verify the proper duty
statuses in assessing compliance with the 60/70 hour rules.
7

7.  What information should be in the supporting documents?
***********************************************************
Supporting documents must contain the following elements:
* Driver name or carrier-assigned identification number, either on the document or on another
document enabling the carrier to link the document to the driver. The vehicle unit number can
also be used if it can be linked to the driver;
* Date;
* Location (including name of nearest city, town, or village); and
* Time.

8.  Can a document with fewer than four required elements be used as a supporting document?
*******************************************************************************************
If a driver has fewer than eight documents that include all four elements, a document that contains all of
the elements except “time” is considered a supporting document.

9.  What supporting documents should a motor carrier retain if a driver submits more than eight documents for a 24-hour period?
*********************************************************************************************************************************
If a driver submits more than eight documents, the motor carrier must retain the first and last documents
for that day and six other supporting documents. If a driver submits fewer than eight documents, the motor
carrier must keep each document.

10 . Are drivers required to show supporting documents during roadside inspections?
***********************************************************************************
Upon request, a driver must provide any supporting document in the driver’s possession for an authorized
safety official’s review.

Editing and Annotations
-----------------------

1.  What is the difference between “paper records of duty status (RODS)” and printouts of RODS from electronic logging devices (ELDs)?
*****************************************************************************************************************************************
“Paper RODS” means RODS that are not kept on an ELD or automatic onboard recording device (AOBRD),
but that are either recorded manually (in accordance with 49 CFR 395.8(f)) or on a computer not
synchronized with the vehicle or that is otherwise not qualified to be an ELD or AOBRD. Printouts of RODS
from ELDs are the reports that ELDs must be able to generate upon request from an authorized safety
official, per section 4.8.1 of the ELD rule.

2.  What is the difference between an “edit” and an “annotation”?
*****************************************************************
An edit is a change to an electronic logging device (ELD) record that does not overwrite the original record,
while an annotation is a note related to a record, update, or edit that a driver or authorized support
personnel may input to the ELD. Section 49 CFR 395.30(c)(2) requires that all edits, whether made by
a driver or the motor carrier, be annotated to document the reason for the change. For example, an edit
showing time being switched from “off duty” to “on-duty not driving” could be annotated by the carrier
to note, “Driver logged training time incorrectly as off duty.” This edit and annotation would then be sent
to the driver for approval.

3.  Can a driver annotate the electronic logging device (ELD) record?
*********************************************************************
Yes. A driver can use annotations to indicate the beginning and end of a period of authorized personal
commercial vehicle use, or yard moves, as well as other special driving categories, such as adverse driving
conditions (49 CFR 395.1(b)) or oilfield operations (49 CFR 395.1(d)).

4.  Who can edit an electronic logging device (ELD) record?
***********************************************************
Both the driver and authorized carrier staff can make limited edits to an ELD record to correct mistakes or
add missing information. All edits must include a note (annotation) to explain the reason for the edit. In
addition, the driver must confirm (certify) that any carrier edit is accurate, and resubmit the records. If the
driver chooses not to re-certify RODs, this is also reflected in the ELD record.
The ELD must keep the original, unedited record, along with the edits. Example: a carrier edits a record to
switch a period of time from “off-duty” to “on-duty not driving”, with a note that explains “Driver logged
training time incorrectly as off-duty”. The edit and annotation are sent to the driver to verify. The edit is not
accepted until the driver confirms it and resubmits the RODS.

5.  Who is responsible for the integrity of records of duty status in regards to the editing and certification rights of drivers and motor carriers?
*******************************************************************************************************************************************************
Although the ELD reflects the driver’s RODS, the driver and carrier share responsibility for the integrity of
the records. The driver certification is intended, in part, to protect drivers from unilateral changes. However,
if the driver is unavailable or unwilling to recertify the record, the carrier’s proposed edit and annotation
would remain part of the record.

6.  Are the original electronic logging device (ELD) records retained after edits are made, and accessible to drivers?
**************************************************************************************************************************
Yes. The original ELD records are retained even when allowed edits and annotations are made. If the driver
cannot independently access the records from the ELD, the motor carrier must provide access on request.
However, the right to access is limited to a six-month period, consistent with the period during which a
motor carrier must retain drivers’ records of duty status (RODS).

7.  Can a user edit or change driving time that has been recorded by an electronic logging device (ELD) to non-driving time?
***********************************************************************************************************************************
No. An ELD automatically records all of the time that a CMV is in motion as driving time that cannot be
edited or changed to non-driving time.

8.  How can a driver record their on-duty not driving status, such as working in a warehouse, on an ELD, prior to operating a commercial motor vehicle equipped with an ELD?
****************************************************************************************************************************************************************************
All of the driver’s hours of service must be accounted for when subject to the HOS rules. Prior to operating
a commercial motor vehicle equipped with an ELD, the driver can manually add any on-duty not driving
time accrued prior to.

9.  Are drivers allowed to edit their records of duty status (RODS) using the electronic logging device (ELD) back office support systems once they leave the commercial motor vehicle (CMV)?
*************************************************************************************************************************************************************************************************
Yes. Drivers may edit their RODS using ELD back office support systems. While these edits or corrections
are allowed to ensure an accurate record of the driver’s duty status, the electronic record must retain what
was originally recorded, as well as the date, time, and identity of the individual entering the corrections
or edits.

10. What procedure should be followed if multiple, compatible electronic logging devices (ELDs) are used to record a driver’s record of duty status (RODS) within a motor carrier’s operation?
****************************************************************************************************************************************************************************************************
If multiple, compatible ELDs are used to record a driver’s RODS within a motor carrier’s operation, the
ELD in the vehicle the driver is operating must be able to produce a complete ELD report for that driver,
on demand, for the current 24-hour period and the previous 7 consecutive days.

11. What procedure should be followed if multiple, incompatible electronic logging devices (ELDs) are used to record a driver’s record of duty status (RODS)?
*****************************************************************************************************************************************************************
The motor carrier and the driver are responsible for ensuring that all of the RODS information required by
the HOS rules is available for review by authorized safety officials at the roadside. If the driver uses multiple
ELDs that are not compatible (e.g., the data file from one system cannot be uploaded into the other
system), the driver must either manually enter the missing duty status information or provide a printout
from the other system(s) so that an accurate accounting of the duty status for the current and previous
seven days is available for the authorized safety official.

12 . What procedure should be followed if an electronic logging device (ELD) is replaced or reset?
**************************************************************************************************
For a reset or replaced ELD, the ELD rule requires data or documents showing the driver’s records of duty
status (RODS) history in the vehicle. This data would include the driver’s past seven days of RODS, either
loaded into the “new” ELD or in paper format to be provided at roadside.

13 . When a motor carrier discovers a driver in a team driving operation failed to log in and his or her activities were assigned to the co-driver, can the motor carrier reassign the generated data?
***************************************************************************************************************************************************************************************************************
For team drivers, the driver account associated with the driving time records may be edited and reassigned
between the team drivers, if there was a mistake resulting in a mismatch between the actual driver and the
driver recorded by the ELD, and if both team drivers were indicated in one another’s records as a co-driver.
Each co-driver must confirm the change for the corrective action to take effect.

14 . What must a driver do with unassigned driving time when he or she logs into the electronic logging device (ELD)?
***********************************************************************************************************************
A driver must review any unassigned driving time when he or she logs into the ELD. If the unassigned
records do not belong to the driver, the driver must indicate that in the ELD record. If driving time logged
under this unassigned account belongs to the driver, the driver must add that driving time to his or her
own record.

15 . What must a motor carrier do with unassigned driving records from an electronic logging device (ELD)?
**********************************************************************************************************
A motor carrier must either explain why the time is unassigned or assign the time to the appropriate driver.
The motor carrier must retain unidentified driving records for at least six months as a part of its hours of
service (HOS) ELD records and make them available to authorized safety officials.

16 . If a driver is permitted to use a Commercial Motor Vehicle (CMV) for personal reasons, how must the driving time be recorded?
***********************************************************************************************************************************
The driver must identify the driving time as personal conveyance on the device.

Harassment
----------

1.  What is the definition of harassment in the Electronic Logging Device (ELD) rule?
*************************************************************************************
FMCSA defines harassment as an action by a motor carrier toward one of its drivers that the motor carrier
knew, or should have known, would result in the driver violating hours of service (HOS) rules in 49 CFR 395
or 49 CFR 392.3. These rules prohibit carriers from requiring drivers to drive when their ability or alertness
is impaired due to fatigue, illness or other causes that compromise safety. To be considered harassment,
the action must involve information available to the motor carrier through an ELD or other technology used
in combination with an ELD. FMCSA explicitly prohibits a motor carrier from harassing a driver.

2.  How does the Electronic Logging Device (ELD) rule address harassment of drivers using ELDs?
***********************************************************************************************
The ELD rule has provisions to prevent the use of ELDs to harass drivers. FMCSA explicitly prohibits a
motor carrier from harassing a driver, and provides that a driver may file a written complaint under 49 CFR 38 6.12(b) if the driver was subject to harassment. Technical provisions that address harassment include a
mute function to ensure that a driver is not interrupted in the sleeper berth. Furthermore, the design of the
ELD allows only limited edits of an ELD record by both the driver and the motor carrier’s agents, and in
either case, the original ELD record cannot be changed. As a result, motor carriers will be limited in forcing
drivers to violate the hours of service (HOS) rules without leaving an electronic trail that would point to the
original and revised records. The driver certification is also intended, in part, to protect drivers from
unilateral changes—a factor that drivers identified as contributing to harassment.

Harassment will be considered in cases of alleged hours of service (HOS) violations; therefore, the penalty
for harassment is in addition to the underlying violation under 49 CFR 392.3 or part 395. An underlying HOS
violation must be found for a harassment penalty to be assessed.

3.  Does the Electronic Logging Device (ELD) rule require real-time tracking of commercial motor vehicle (CMVs) with ELDs?
****************************************************************************************************************************
No. Real-time tracking of CMVs is not required in the ELD rule. However, a motor carrier may use
technology to track its CMVs in real time for business purposes. A motor carrier is free to use this data
as long as it does not engage in harassment or violate the Federal Motor Carrier Safety Regulations
(FMCSRs).

4.  What are the differences between harassment and coercion?
*************************************************************
A motor carrier can only be found to have committed harassment if the driver commits a specified
underlying hours of service (HOS) violation based on the carrier’s actions and there is a connection to the
electronic logging device (ELD). Adverse action against the driver is not required, because the driver
complied with the carrier’s instructions. In contrast, coercion is much broader in terms of entities covered,
and addresses the threat to withhold work from or take adverse employment action against a driver in
order to induce the driver to violate a broader range of regulatory provisions or to take adverse action to
punish a driver for the driver’s refusal to operate a commercial motor vehicle (CMV) in violation of the
specified regulations. Unlike harassment, coercion does not have to result in the driver being in violation of
the regulations and does not have to involve the use of an ELD.

5.  What should a driver consider before filing a harassment complaint?
***********************************************************************
FMCSA encourages any driver who feels that he or she was the subject of harassment to also consider
FMCSA’s coercion rule and the Department of Labor’s whistleblower law (enacted as part of the Surface
Transportation Assistance Act (49 U.S.C. 31105)), which provides retaliation protection.

6.  How much time is allowed for a driver to file a harassment complaint?
*************************************************************************
A driver must file a written harassment complaint no later than 90 days after the event.

7.  Where should a driver file a harassment complaint?
******************************************************
The driver’s must file a written complaint with the National Consumer Complaint Database at
http://nccdb.fmcsa.dot.gov or with the FMCSA Division Administrator for the State where the driver
is employed (http://www.fmcsa.dot.gov/mission/field-offices).

8.  What information must be submitted in a harassment complaint?
*****************************************************************
The following information must be submitted in writing:

	1.  Driver’s name, address, and telephone number;

	2.  Name and address of the motor carrier allegedly harassing the driver; and

	3.  Statement of the facts to prove each allegation of harassment, including:

		a.How the electronic logging device (ELD) or other technology used with the ELD contributed
		to harassment.
		b.The date of the alleged action.
		c.How the motor carrier’s action violated either 49 CFR 392.3 or 49 CFR 395.

4.  Driver’s signature.
***********************
Any supporting evidence that will assist FMCSA in the investigation of the complaint should also be
included along with the complaint.

ELD Functions
-------------

1.  What information is automatically recorded by an electronic logging device (ELD)?
*************************************************************************************
An ELD automatically records the following data elements at certain intervals: date; time; location
information; engine hours; vehicle miles; and identification information for the driver, authenticated user,
vehicle, and motor carrier.

2.  When is location data recorded by an electronic logging device (ELD)?
*************************************************************************
Location data must be recorded by an ELD at 60-minute intervals when the vehicle is in motion, and
when the driver powers up and shuts down the engine, changes duty status, and indicates personal use
or yard moves.

3.  Will the vehicle location information identify street addresses?
********************************************************************
No. Vehicle location information is not sufficiently precise to identify street addresses. For each change in
duty status, the ELD must convert automatically captured vehicle position in latitude/longitude coordinates
into geo-location information that indicates the approximate distance and direction to an identifiable
location corresponding to the name of a nearby city, town, or village, with a State abbreviation.

4.  Is an electronic logging device (ELD) required to collect data about vehicle performance?
*********************************************************************************************
No. ELDs are not required to collect data on vehicle speed, braking action, steering function or other
vehicle performance parameters. ELDs are only required to collect data to determine compliance with
hours of service (HOS) regulations.

5.  Do the specifications in the ELD rule for electronic logging devices (ELDs) include requirements to automatically control the vehicle, such as other safety systems that may automatically reduce
acceleration or apply braking?
******************************************************************************************************************************************************************************************************************************************************************
No. The specifications for ELDs do not include requirements to control the vehicle. An ELD is a recording
device that records vehicle parameters through its synchronization to the vehicle’s engine, and allows for
entries related to a driver’s record of duty status (RODS).

6.  What is the level of accuracy for commercial motor vehicle (CMV) location information recorded by an electronic logging device (ELD)?
**************************************************************************************************************************************************
During on-duty driving periods, the location accuracy is approximately within a 1-mile radius. When a
driver operates a CMV for personal use, the position reporting accuracy would be approximately within
a 10-mile radius.

7.  What does engine synchronization mean for the purposes of electronic logging device (ELD) compliance?
*********************************************************************************************************
An ELD must be integrally synchronized with the engine of the commercial motor vehicle (CMV). Engine
synchronization means monitoring engine operation to automatically capture the engine power status,
vehicle motion status, miles driven, and engine hours.

8.  Will GPS-derived data for mileage be allowed as a substitute for data that cannot be readily obtained from a vehicle electronic control module (ECM)?
**********************************************************************************************************************************************************
No, the ELD must be able to monitor engine operation to automatically capture required data. A GPS is not
integrally synchronized with a vehicle’s engine, and cannot be a substitute for required ECM data to comply
with the ELD rule.

9.  Can an electronic logging device (ELD) have a feature to warn drivers about approaching hours of service (HOS) limits?
******************************************************************************************************************************
Yes. FMCSA allows, but does not require, warning or notification to drivers when they are nearing their
HOS limits.

10 . When will an electronic logging device (ELD) automatically start to record a driving mode or status?
*********************************************************************************************************
An ELD must automatically switch to driving mode once the commercial motor vehicle (CMV) is moving up
to a set speed threshold of five miles per hour. As a result, the in-motion state must not be configured
greater than five miles per hour. The vehicle will be considered stopped once its speed falls to zero miles
per hour and stays at zero miles per hour for three consecutive seconds.

11 . When will an electronic logging device (ELD) automatically change the duty status from driving to the default duty status of on-duty not driving?
*************************************************************************************************************************************************************
When the duty status is set to driving, and the commercial motor vehicle (CMV) has not been in motion for
five consecutive minutes, the ELD must prompt the driver to confirm a continued driving status or enter
the proper duty status. If the driver does not respond to the ELD prompt within one minute, the ELD must
automatically switch the duty status to on-duty not driving.

12 . How must a driver be able to access records of duty status (RODS) from an electronic logging device (ELD)?
***************************************************************************************************************
Since all ELD data file output will be a standard comma-delimited file, a driver may import the data output
file into Microsoft Excel, Word, notepad, or other common tools. A driver will also be able to access ELD
records through a screen display or a printout, depending on the ELD design.

13 . How does the electronic logging device reflect personal conveyance when the personal conveyance status is selected and the commercial motor vehicle (CMV) is driven?
****************************************************************************************************************************************************************************
When the personal conveyance status is selected (as allowed and configured by the motor carrier), the
CMV’s location is recorded with a lower level of precision (i.e., an approximate 10-mile radius). Personal
conveyance will be reflected on the ELD using a different style line (such as dashed or dotted line).

14 . What are the display requirements for team drivers using the same electronic logging device
(ELD) on their commercial motor vehicle (CMV)?
****************************************************************************************************************************************************************
In the event of team drivers, the ELD must display the data for both co-drivers who are logged into
the system.

15 . Can a logged-in co-driver make entries over his or her records using the electronic logging device (ELD) when he or she is not driving?
************************************************************************************************************************************************
Yes. The driver who is not operating the vehicle may make entries over his or her own records when
the vehicle is in motion. However, co-drivers cannot switch driving roles on the ELD when the vehicle
is in motion.

16 . Can an electronic logging device (ELD) record be set to record minimum duty status durations, such as 15 minutes?
**************************************************************************************************************************
No. The ELD will capture all entered duty statuses, and there is no minimum amount of time that these
statuses must or should be engaged. While longstanding industry and enforcement practices may have
relied upon minimum intervals of 15 minutes in handwritten records of duty status (RODS), an ELD
provides a more accurate accounting of drivers’ time. This should not be construed to indicate that the
activities electronically recorded as less than 15 minutes are suspect, only that the time actually required
to complete the task may be less that what had been traditionally noted in the paper RODS.


17 . As a motor carrier, how can I be sure an electronic logging device (ELD) is compliant?
*******************************************************************************************
You should only purchase an ELD that is self-certified by the manufacturer to be compliant and
that is registered and listed on the FMCSA website. The list of registered ELDs can be found at
https://3pdp.fmcsa.dot.gov/ELD/ELDList.aspx. Motor carriers should also familiarize
themselves with the ELD checklist and the ELD rule, located at
https://www.fmcsa.dot.gov/hours-service/elds/drivers-and-carriers.

18 . May an ELD device be used to track mileage for tax reporting purposes?
***************************************************************************
The device manufacturer may offer that service as part of a fleet management package but mileage
tracking for tax reporting purposes is not part of the ELD data established in Part 395.

ELD Data Transfer
-----------------

1.  What are the options for electronic logging devices (ELDs) to electronically transfer data?
***********************************************************************************************
According to the ELD rule technical specifications, an ELD must support one of two options for electronic
data transfer:

	1.  The first option is a “telematics” transfer type ELD. At a minimum, it must electronically transfer data
	to an authorized safety official on demand via wireless Web services and email.

	2.  The second option is a “local” transfer type ELD. At a minimum, it must electronically transfer data to
	an authorized safety official on demand via USB2.0 and Bluetooth®.
	To ensure that law enforcement is always able to receive the hours of service (HOS) data during a roadside
	inspection, a driver must be able to provide either the display or a printout when an authorized safety
	official requests a physical display of the information.

2. How will safety officials receive data electronically from the two different types of ELDs with options for different methods of electronic data transfer (“telematics” and “local”)?
*******************************************************************************************************************************************************************************************
Authorized safety officials who conduct roadside enforcement activities (i.e., traffic enforcement and
inspections) or compliance safety investigations will have the option of choosing a minimum of one
electronic data transfer method (wireless Web services or email) and one “local” electronic data transfer
method (USB2.0 or Bluetooth) for the electronic transfer of ELD data, depending on the type of ELD.

3. What is the process for transferring data via USB2.0?
*********************************************************
If a driver is using a “local” ELD with USB 2.0 capabilities, an authorized safety official will provide a secure
USB device to allow the driver to electronically transfer data from the ELD to the official. The driver will
return the USB device to the safety official, who will transfer the data to a computing device.

4. What is the process for transferring data via email?
********************************************************
If the driver is using a “telematics” ELD with email capabilities, the authorized safety official will request
that the electronic data transfer file be sent as an attachment to an e-mail. This e-mail address is
preprogramed in the ELD by the vendor. The safety official will provide the driver with a routing code to
reference in the email.

5. What is the process for transferring data via Bluetooth?
************************************************************
While the local Bluetooth requires the use of web services, local Bluetooth data transfer only requires
the safety official to have internet connectivity and not the ELD. The driver’s/motor carrier’s ELD will use
the safety official’s internet connection to transfer data. The internet connection between the ELD and the
safety official will be limited and can only be used for the purpose of transferring the ELD data via the web
service. During Bluetooth data transfer, the driver/motor carrier must make the ELD discoverable. Once
the ELD detects the safety official’s laptop, the safety official will provide the driver/motor carrier with a
Bluetooth code to enter into the ELD and confirm Bluetooth connectivity between the safety official’s
laptop and the ELD. Once the connection between the safety official’s laptop and the ELD has been
confirmed, the safety official will provide the driver/motor carrier with the safety official’s unique code,
and the driver/motor carrier will transfer the ELD data to web services for the safety official to retrieve.



6. What is the process for transferring data via web services?
**************************************************************
If the driver is using a “telematics” ELD with wireless Web services capabilities, the authorized safety
official will give the driver a routing code to assist the official in locating the data once transmitted, and
the driver will initiate a web transfer to an FMCSA server to be retrieved by the safety official’s software.

7.  Would an electronic logging device (ELD) be non-compliant with the ELD rule if the data cannot be sent electronically to an authorized safety official at roadside?
******************************************************************************************************************************************************************************
No. If the electronic means for transferring data is unavailable or fails, the driver can still be compliant by
showing either a printout or the actual ELD display of their RODS.

ELD Malfunctions and Data Diagnostic Events
-------------------------------------------

1.  Is an ELD required to monitor its compliance with the ELD technical requirements?
*************************************************************************************
Yes. An ELD must monitor its compliance with the ELD technical requirements and detect malfunctions
and data inconsistencies related to power, data synchronization, missing data, timing, positioning, data
recording, data transfer, and unidentified driver records requirements. The ELD output will identify these
data diagnostic and malfunction events and their status as either “detected” or “cleared.” Typically, a driver
can follow the ELD provider’s and the motor carrier’s recommendations to resolve the data inconsistencies
that generate an ELD data diagnostic event, while a motor carrier must correct a malfunction.

2.  When do electronic logging device (ELD) “power data diagnostic events” and “power compliance
malfunctions” occur?
**************************************************************************************************************************************
“Power data diagnostic events” occur when an ELD is not powered and fully functional within one minute
of the vehicle’s engine receiving power and does not remain powered for as long as the vehicle’s engine
stays powered.

“Power compliance malfunctions” occur when an ELD is not powered for an aggregated in-motion driving
time of 30 minutes or more over a 24-hour period across all driver profiles.

3.  When do electronic logging device (ELD) “engine synchronization data diagnostic events” and “engine synchronization compliance malfunctions” occur?
*****************************************************************************************************************************************************************
“Engine synchronization data diagnostic events” occur when an ELD loses ECM connectivity to any of the
required data sources (engine power status, vehicle motion status, miles driven, and engine hours) and can
no longer acquire updated values for the required ELD parameters within five seconds of the need.
“Engine synchronization compliance malfunctions” occur when ECM connectivity to any of the required
data sources (engine power status, vehicle motion status, miles driven, and engine hours) is lost for more
than 30 minutes during a 24-hour period aggregated across all driver profiles.

4.  When does an electronic logging device (ELD) “timing compliance malfunction” occur?
***************************************************************************************
A “timing compliance malfunction” occurs when the ELD can no longer meet the underlying compliance
requirement to record Coordinated Universal Time (UTC), where ELD time must be synchronized with UTC,
not to exceed an absolute deviation of 10 minutes at any time.

5.  When does an electronic logging device (ELD) “positioning compliance malfunction” occur?
********************************************************************************************
When an ELD fails to acquire a valid position measurement within 5 miles of the commercial motor vehicle
moving and 60 minutes has passed, a “position compliance malfunction” will be recorded in the data
diagnostic.

6.  When does an electronic logging device (ELD) “data recording compliance malfunction” occur?
***********************************************************************************************
A “data recording compliance malfunction” occurs when an ELD can no longer record or retain required
events or retrieve recorded logs that are not kept remotely by the motor carrier.

7.  When does an electronic logging device (ELD) “missing required data elements data diagnostic event” occur?
**************************************************************************************************************
A “missing required data elements data diagnostic event” occurs when any required data field is missing
at the time of its recording.

8.  When do electronic logging device (ELD) “data transfer data diagnostic events” and “data transfer compliance malfunctions” occur?
**************************************************************************************************************************************
A “data transfer data diagnostic event” occurs when the operation of the data transfer mechanism(s) is
not confirmed.

A “data transfer compliance” malfunction occurs when the ELD stays in the unconfirmed data transfer
mode following the next three consecutive monitoring checks.

9.  When does an electronic logging device (ELD) “unidentified driving records data diagnostic event” occur?
************************************************************************************************************
An “unidentified driving records data diagnostic event” occurs when more than 30 minutes of driving time
for an unidentified driver is recorded within a 24-hour period.

10 . What must a driver do if there is an electronic logging device (ELD) malfunction?
**************************************************************************************
If an ELD malfunctions, a driver must:

	1.  Note the malfunction of the ELD and provide written notice of the malfunction to the motor carrier within 24 hours;

	2.  Reconstruct the record of duty status (RODS) for the current 24-hour period and the previous 7 consecutive days, and record the records of duty status on graph-grid paper logs that comply with 49  CFR 395.8, unless the driver already has the records or retrieves them from the ELD; and

	3.  Continue to manually prepare RODS in accordance with 49 CFR 395.8 until the ELD is serviced and
	back in compliance. The recording of the driver’s hours of service on a paper log cannot continue for
	more than 8 days after the malfunction; a driver that continues to record his or her hours of service
	on a paper log beyond 8 days risk being placed out of service.

11 . What must a motor carrier do if there is an electronic logging device (ELD) malfunction?
*********************************************************************************************
If an ELD malfunctions, a motor carrier must:

	1.  Correct, repair, replace, or service the malfunctioning ELD within eight days of discovering the condition or a driver’s notification to the motor carrier, whichever occurs first; and

	2.  Require the driver to maintain paper record of duty status (RODS) until the ELD is back in service.

12 . When should a driver certify his or her record of duty status (RODS) on the electronic logging device (ELD) to avoid malfunction codes?
*********************************************************************************************************************************************
FMCSA recommends that drivers first certify their RODS before logging off the ELDs and then shutting
down their CMVs’ engines. If drivers don’t follow this recommendation, malfunction codes may occur, such
as indicating unaccounted odometer changes and suspicious driving activity.

13 . What types of visual indicators must be displayed by an ELD?
*****************************************************************
An ELD must display a single visual malfunction indicator on the ELD’s display or on a stand-alone indicator
for all drivers using the ELD. The visual signal must be visible to the driver, be continuously communicated
to the driver when the ELD is powered, and clearly illuminate an active malfunction.
An ELD must also display a single visual data diagnostics indicator, apart from the malfunction
indicator, for active data diagnostics events. The ELD may also provide an audible signal for the data
diagnostics indicator.

14 . In the event of a malfunction that requires a driver to reconstruct his/her previous 7 days, can a driver use a printed copy of their previous 7 days, such as a PDF copy, instead of manually recording their previous 7 days?
*****************************************************************************************************************************************************************************************************************************************
Yes. In the event that the driver experiences a malfunction that impairs the ELD ability to present the
driver’s previous 7 days, the driver may present their previous 7 days by way of any printed copy, or in
an electronic form, such as a PDF. 

15 . If an ELD malfunction corrects itself after the driver has reconstructed his or her records of duty status, must the driver present their reconstructed records of duty status during an inspection?
**********************************************************************************************************************************************************************************************************
Yes, the reconstructed records of duty status along with ELD data must be presented to a safety official
during a roadside inspection in order to satisfy the requirement to display the current day and the previous
seven days of duty status.

ELD Accounts
------------

1.  What electronic logging device (ELD) user accounts must be set up by a motor carrier?
*****************************************************************************************
ELD user accounts must be set up by a motor carrier for:

	1. Commercial motor vehicle (CMV) drivers who are employed by the motor carrier and who are required to use the ELD, and

	2. Support personnel who have been authorized by the motor carrier to create, remove, and manage user accounts; configure allowed ELD parameters; and access, review, and manage drivers’ ELD
	records on behalf of the motor carrier.

2.  Can a motor carrier create electronic logging device (ELD) accounts on individual ELDs or its support system?
*****************************************************************************************************************
Yes. ELD user accounts can be created on individual ELDs or the ELD support system.

3.  What information is required to create electronic logging device (ELD) user accounts for drivers?
*****************************************************************************************************
Each driver account must be created by entering the driver’s license number and the State of jurisdiction
that issued the driver’s license. The driver’s license information is only required to set up the driver’s
user account and verify his or her identity; it is not used as part of the daily process for entering duty
status information.

4.  How many electronic logging device (ELD) accounts can be established by a motor carrier for one of its ELD drivers?
***********************************************************************************************************************
A motor carrier must assign only one ELD driver account for each of its drivers required to use an ELD.
An ELD must not allow the creation of more than one driver account associated with a driver’s license for
a given motor carrier. The motor carrier is also responsible for establishing requirements for unique user
identifications and passwords.

5.  Can a driver’s electronic logging device (ELD) single user account be authorized for administrative functions, in addition to its driver-related functions?
****************************************************************************************************************************************************************************
No. Each driver should have one account that allows him or her to log in and perform driver-related
functions. All other administrative functions should be based on the discretion of each company or its
provider. This means a driver who is also the owner of the company would have a single account
authorizing entries as a driver, and a separate account for administrative functions. Accounts can be
created on the ELD or the ELD support system.

6.  Driver accounts must include the CDL number and state. If a driver relocates to another state and obtains a new commercial driver’s license, can the ELD allow for editing the driver profile to change the license number and state or must a new driver account be created? If so, how would the two link together to allow for recording the prior seven days?
**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************
Section 395.22(b)(2)(i) states that a motor carrier must manage ELD accounts. Therefore, the driver’s
license information must be updated in the ELD. If the data files from an individual’s old and new driver
license files cannot be merged, the driver must either manually enter the previous duty status information
or provide a printout from the older HOS to provide an accurate accounting of the duty status for the
current and previous seven days for authorized safety officials.

7.  Can a motor carrier set up a driver account as an “exempt driver” and have another account for the same driver as a regular driver account?
***********************************************************************************************************************************************
No. The ELD rule prohibits multiple driver accounts for one driver. The motor carrier must proactively
change the driver’s status to and from exempt and non-exempt.

ELD Registration and Certification
----------------------------------

1.  When will electronic logging device (ELD) system suppliers be able to start registering their ELDs with FMCSA?
******************************************************************************************************************
Since February 16, 2016, ELD system suppliers have been able to self-certify and register devices with the
FMCSA through the following link: https://www.fmcsa.dot.gov/hours-service/elds/equipment-registration.

2.  What happens if a registered device listed on FMCSA’s website is later found not to meet the technical specifications in the Electronic Logging Device (ELD) rule?
*****************************************************************************************************************************************************************************
The ELD rule includes a procedure to remove a listed registered device from the FMCSA website, to
provide additional assurance to motor carriers that ELDs on the vendor registration website are compliant.
This procedure also protects an ELD vendor’s interest in its product.

3.  What is the procedure to remove a listed certified electronic logging device (ELD) from FMCSA’s website?
************************************************************************************************************
FMCSA may initiate removal of an ELD model or version from the list in accordance with section 5.4 in
the ELD rule, by providing written notice to the ELD provider stating:
* (a) The reasons the FMCSA proposes to remove the model or version from the FMCSA list; and
* (b) Any corrective action that the ELD provider must take for the ELD model or version to remain on the list.

4.  Is the electronic logging device (ELD) vendor/manufacturer required to notify motor carriers if a device is removed from FMCSA’s ELD registration list because it was determined to be noncompliant?
******************************************************************************************************************************************************************************************************************
No, ELD vendors/manufacturers are not required to notify motor carriers if a device has been removed
from the ELD registration list. However, FMCSA will maintain on its website a list of devices that are
removed from the list, and will make every effort to ensure that industry is aware.

5.  What happens if an electronic logging device (ELD) is found to be non-compliant after it is in use?
*******************************************************************************************************
As a motor carrier, you will have 8 days from notification to replace your noncompliant device with a
compliant one. This is the same time allowed to take action on ELDs that need to be repaired, replaced, or
serviced. In the event of a widespread issue, FMCSA will work with affected motor carriers to establish a
reasonable timeframe for replacing non-compliant devices with ELDs that meet the requirements.

Differences between AOBRDs and ELDs
-----------------------------------

1.  What are the differences in the technical specifications in the 1988 automatic onboard recording device (AOBRD) Rule (49 CFR 395.15) and the Electronic Logging Device (ELD) rule?
****************************************************************************************************************************************************************************************
The table below compares the technical specifications in the AOBRD rule (49 CFR 395.15) and the
ELD rule.
Feature/Function 1988 AOBRD Rule ELD Rule Integral Synchronization Integral synchronization required, but
term not defined in the Federal Motor
Carrier Safety Regulations (FMCSRs).
Integral synchronization interfacing with the
CMV engine electronic control module
(ECM), to automatically capture engine
power status, vehicle motion status, miles
driven, engine hours. (CMVs older than
model year 2000 exempted.)
Recording Location Information of
Commercial Motor Vehicle (CMV)
Required at each change of duty status.
Manual or automated.
Requires automated entry at each change of
duty status, at 60-minute intervals while
CMV is in motion, at engine-on and engineoff
instances, and at beginning and end of
personal use and yard moves.
Graph Grid Display Not required – “time and sequence of
duty status changes.”
An ELD must be able to present a graph
grid of driver’s daily duty status changes
either on a display or on a printout.
Hours of Service (HOS) Driver Advisory
Messages
Not addressed. HOS limits notification is not required.
“Unassigned driving time/miles” warning
must be provided upon login.
Device “Default” Duty Status Not addressed. On-duty not driving status, when CMV has
not been in-motion for five consecutive
minutes, and driver has not responded to an
ELD prompt within one minute. No other
non-driver-initiated status change is allowed.
Clock Time Drift Not addressed. ELD time must be synchronized to Universal
Coordinated Time (UTC); absolute deviation
must not exceed 10 minutes at any time.
Communications Methods Not addressed – focused on interface
between AOBRD support systems and
printers.
Two Options:

1-  “Telematics”: As a minimum, the ELD
**********************************************************
must transfer data via both wireless Web
services and wireless e-mail.

2-  “Local Transfer”: As a minimum, the ELD
**********************************************************
must transfer data via both USB 2.0 and
Bluetooth.
Both types of ELDs must be capable of
displaying a standardized ELD data set to
authorized safety officials via display or
printout.
Resistance to Tampering AOBRD and support systems must be
tamperproof, to the maximum extent
practical.
An ELD must not permit alteration or
erasure of the original information collected
concerning the driver’s ELD records or
alteration of the source data streams used
to provide that information. ELD must
support data integrity check functions.
Identification of Sensor Failures and
Edited Data
AOBRD must identify sensor failures
and edited data.
An ELD must have the capability to monitor
its compliance (engine connectivity, timing,
positioning, etc.) for detectable
malfunctions and data inconsistencies. An
ELD must record these occurrences.

Differences between AOBRDs and Logging Software Programs
--------------------------------------------------------

1.  What are the differences between automatic onboard recording devices (AOBRDs) and devices using logging software programs?
******************************************************************************************************************************
A key difference between devices with logging software programs and AOBRDs relates to connectivity
with the commercial motor vehicle (CMV) operations. An AOBRD must be integrally synchronized with the
specific operations of the CMV on which it is installed. AOBRDs must also record engine use, speed, miles
driven, and date and time of day, as specified in 49 CFR 395.2. AOBRDs automatically record engine data
for driving time versus the use of Global Positioning System (GPS) data or a driver inputting his or her
driving status hours.
The display and output from devices using logging software must meet the requirements in 49 CFR 395.8.
Drivers can manually enter their hours of service (HOS) information using the application or software
program on the device, and then manually or electronically sign the RODS at the end of each 24-hour
period to certify that all required entries are true and correct. During a roadside inspection, drivers using
logging software programs can hand their device to the safety official to review their RODS. Additionally,
officers conducting inspections can request a printed copy of the driver’s log with the current and prior
seven days HOS information.

Specialty Operations
--------------------

1.  In an operation that involves a tillerman and a driver, what method should the tillerman use to record his or her hours-of-service?
***************************************************************************************************************************************
Because the ECM will not support two ELDs and only one driver can be logged into the ELD, the driver in
the front cab must log into the ELD and make an annotation that he or she is operating with a tillerman.
The tillerman has the option of manually adding their hours of service to the ELD under their ELD driver
account or keep the previous 7 days of their records of duty status in their possession for roadside
inspections. The same options apply to the motor carrier maintaining the tillerman’s records of duty status
for 6 months.

2.  Is a driver required to maintain their records of duty status on an ELD while operating a rental vehicle?
*************************************************************************************************************
The Federal Motor Carrier Safety Administration (FMCSA) issued a limited exemption to motor carriers that
operate with a rented commercial motor vehicle for 8 days or less. This limited exemption provides that all
drivers of property-carrying commercial motor vehicles rented for 8 days or less, regardless of reason, are
not required to use an ELD in the vehicle. To meet this exemption, the driver and the motor carrier must
maintain the following:

* A copy of federal register notice (FRN) 82 FR 47306 Hours of Service of Drivers: Application for Exemption; Truck Renting and Leasing Association (TRALA) or equivalent signed FMCSA exemption document. This document must be provided to the safety official upon request;
* A copy of rental agreement with parties to the agreement clearly identified, the vehicle, and the dates of the rental period; and
* A copy of the driver’s records of duty status for the current day and the prior 7 days if required on those days.
* For additional information see FRN 82 FR 47306 Hours of Service of Drivers: Application for Exemption; Truck Renting and Leasing Association (TRALA), published on October 11, 2017.

3.  Are drivers that operate in Canada and Mexico required to use an ELD when driving in the US?
************************************************************************************************
Yes, if a driver that operates in Canada or in Mexico operates and drives in the US, and does not meet one
of the ELD exemptions, said driver must use an ELD while operating in the US.

ELD Technical Specifications
----------------------------

1.  Why are technical specifications in the Electronic Logging Device (ELD) rule?
*********************************************************************************
The technical specifications in the ELD rule ensure that manufactures develop compliant devices and
systems for uniform enforcement of hours of service.

2.  Will FMCSA provide the eRODS application for vendors to test against?
*************************************************************************
No, FMCSA will not provide the RODS application for vendors to test against. Compliance test procedures
are available on the ELD website to allow vendors to test their devices’ compliance with the ELD rule
technical specifications.

3.  Is a physical connection between the ECM and ELD required in order to establish integral synchronization with the engine?
*****************************************************************************************************************************
No. The ECM and ELD may be connected by serial or Control Area Network communication protocols.
Hard wiring to the J1939 plug and Bluetooth connectivity are examples of methods of receiving the data
from the ECM or vehicle data bus.

4.  If an ELD loses connection to the ECM how will the device report a system failure?
**************************************************************************************
An ELD must use onboard sensors and data record history to identify instances when it may not have
complied with the power requirements specified in the ELD rule.

5.  Must the manufacturer self-certify and register every version or firmware update to the ELD?
************************************************************************************************
The manufacture must register each model and version and self-certify that each particular ELD is
compliant with the ELD rule. The manufacturer must decide whether a firmware update is sufficiently
significant to change the registration information. FMCSA did not specify parameters for version revisions.

6.  Are vendors required to update or register each different configuration of hardware, even though the product is the same as an application (APP) (e.g., black box and Samsung, black box and iPhone, black box and Nexus)?
*********************************************************************************************************************************************************************************************************************************
Vendors should register each device bundle if they have different operating systems (e.g., an iOS-based
bundle and an Android-based bundle would be considered two registered devices).

7.  Does the registration process require companies to exclusively use FMCSA’s test procedure? Will vendors have access to testing facilities for ELDs?.
**************************************************************************************************************************************************************
No. ELD manufacturers may use any test procedure they choose and note this in the registration. FMCSA
will not provide a third-party testing service. FMCSA will only investigate devices that are suspected of not
conforming to specifications, and will conduct testing with the FMCSA compliance test procedure during
its investigation.

8.  How will CMV environmental specifics be tested with the ELD – for example, mounting and connections to the ECM?
*********************************************************************************************************************
FMCSA does not specify testing requirements for connectivity with the vehicle. Each ELD provider is
responsible for connectivity testing.

9.  How will the ELD report on-duty not-driving status when the origin of the duty status is automatic instead of driver-input?
**********************************************************************************************************************************
The ELD will report on-duty not-driving status based on automatic detection, starting from the time the
vehicle is no longer in motion.

10 . The Event Checksum Calculation describes the individual items to be included in the calculation. Number 9 says “CMV Number.” Is this the CMV VIN or the CMV Power Unit Number?
***********************************************************************************************************************************************************************************
FMCSA showed a CMV number, as an example, in the header output file. The intent was to allow an additional vehicle identification number if an operator had numbered vehicles and chose to add companyassigned numbers to the CMV header data.

11 . Are unassigned driving reports required to be available at roadside?
*************************************************************************
Yes. Section 4.8.1.3, requires the inspected driver’s profile and the unidentified driver profile to be available as separate reports at roadside either by printout or display. If there are no unidentified driver records existing on the ELD for the current 24-hour period or for any of the previous 7 consecutive days, an ELD does not need to print or display unidentified driver records for the authorized safety official. Otherwise, both reports must be printed or displayed and provided to the authorized safety official.

12 . How should the ELD handle the dashboard odometer display not matching the odometer value
returned by the Engine Control Module (ECM)? For instance, when the engine is replaced and the
value is not synchronized. The ELD is required to obtain and display the ECM value at all times. Safety officials will use the odometer value reported on the ELD. Note that documentation of engine changes is required by 49 CFR Part 379 Appendix A to be maintained at the carrier’s place of business.

ELD Manufacturer Compliance
---------------------------

1.  As an electronic logging device (ELD) manufacturer, how can I make sure that my product is compliant?
*********************************************************************************************************
You should review the technical specifications included in the final rule, along with the FMCSA’s compliance
test procedures. You are required to self-certify your device, stating that it is in compliance with all of the
technical specifications. The compliance test procedures are designed to assist manufacturers in
determining whether their product meets the ELD rule requirements. While ELD manufacturers are not
required to use FMCSA’s compliance test procedure, they are required to ensure that their products are
compliant with the ELD rule.

Automatic On-Board Recording Device (AOBRD)
-------------------------------------------

1.  Can a motor carrier purchase automatic on-board recording devices (AOBRDs) in bulk quantities for installation after December 18, 2017?
*******************************************************************************************************************************************
No. To be grandfathered, an AOBRD must be installed in a CMV prior to December 18, 2017. If a
grandfathered AOBRD subsequently becomes inoperable, then that AOBRD must be replaced by an ELD.

2.  If a motor carrier adds an owner operator to its fleet after December 18, 2017, and the owner operator operates with a grandfathered AOBRD in their CMV, can the owner operator continue to use its grandfathered AOBRD while employed by the motor carrier?
****************************************************************************************************************************************************************************************************************************************************************
Yes, an owner operator that operates with a grandfathered AORBD and is hired by a motor carrier
after December 18, 2017 may continue to operate with its grandfathered AOBRD while employed by
the motor carrier.

3.  If an AOBRD becomes inoperable after December 18, 2017, can that AOBRD be replaced if it is under warranty?
***************************************************************************************************************
Yes, if an AOBRD is under warranty and becomes inoperable after December 18, 2017, then that AOBRD
can be replaced with another AOBRD within the terms of the manufacturer’s warranty. Motor carriers
should note that 49 CFR Part 379 Appendix A requires the motor carrier to maintain records pertaining to
replacement of equipment. If the AOBRD is not under warranty and becomes inoperable after December 18 , 2017, then that AOBRD must be replaced with an ELD. 